module.exports = {
    "mongo": {
        "users": {
            "admin": {
                "user": "<username>",
                "pass": "<password>"
            },
            "backup": {
                "user": "<username>",
                "pass": "<password>"
            },
            "read": {
                "user": "<username>",
                "pass": "<password>"
            }
        },
        "backupUrl": "<@url:port>",
        "backupDB": "<database name>"
        ,"url": "<@url:port>",
        "db": "<database name>"
    },
    "port": "",
    "secureport": "",
    "https": false,
    "sessionSecret": "turbulant 33 monkey headscarf",
    "cookieSecret": "spider coffee mug chugger 1",
    "login": {
        "ipcaptcha": 5
        // turned this off as wasted too much http request to check each call, consider a memory alternative
        ,
        "ipblock": 10,
        "sescaptcha": 3
    },
    "cache": {
        "templates": false,
        "modules": false,
        "pages": false,
        "pagecount": 10,
        "modcount": 5,
        "tempcount": 10,
        "pageforcecache": true, // this is so a page or module can have an option to always be in the cache, overruling the need for polling.
        "moduleforcecache": true // this is so a page or module can have an option to always be in the cache, overruling the need for polling.
    },
    "chatty": true
};