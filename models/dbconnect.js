//var fs = require('fs');
/*
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
module.exports.mongoose = mongoose;
module.exports.Schema = Schema;
var url = "";
var config = require('../config');
var user = "";
if (config.mongo.users.admin.user === "") {
    user = "";
}
else {
    user = config.mongo.users.admin.user + ':' + config.mongo.users.admin.pass;
}
url = 'mongodb://' + user + config.mongo.url + "/" + config.mongo.db;
var db;
//var connect = function() {
    mongoose.connect(url);
    db = mongoose.connection;
    db.on('error', function(err) {
        console.log("Connection Failure: " + err);
        mongoose.disconnect();
    });
//};
//connect();*/

var config = require("../config");
var DB = require('mongoose');

// connection

var url = "";
var user = "";

// default database connection

if (config.mongo.users.admin.user === "") {
    user = "";
}
else {
    user = config.mongo.users.admin.user + ':' + config.mongo.users.admin.pass;
}
url = 'mongodb://' + user + config.mongo.url + "/" + config.mongo.db;
    DB.connect(url);
    DB.connection.on('error', function(err) {
        console.log("Connection Failure: " + err);
        DB.disconnect();
    });

// back up connection

if (config.mongo.users.backup.user === "") {
    user = "";
}
else {
    user = config.mongo.users.backup.user + ':' + config.mongo.users.backup.pass;
}
url = 'mongodb://' + user + config.mongo.backupUrl + "/" + config.mongo.backupDB;

var backUpDB = DB.createConnection(url);

module.exports.DB = DB;
module.exports.backUpDB = backUpDB;