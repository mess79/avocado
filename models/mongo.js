// schemas and models

// connect to db

var db = require('./dbconnect');

var DB = db.DB;
var backUpDB = db.backUpDB;
//var argon = require('argon2');

//var passportLocalMongoose = require('passport-local-mongoose');
//var argon = require("argon2");
//console.log(argon);
/*
var config = require("../config");
var DB = require('mongoose');
var passportLocalMongoose = require('passport-local-mongoose');

// connection

var url = "";
var user = "";

// default database connection

if (config.mongo.users.admin.user === "") {
    user = "";
}
else {
    user = config.mongo.users.admin.user + ':' + config.mongo.users.admin.pass;
}
url = 'mongodb://' + user + config.mongo.url + "/" + config.mongo.db;
    DB.connect(url);
    DB.connection.on('error', function(err) {
        console.log("Connection Failure: " + err);
        DB.disconnect();
    });

// back up connection

if (config.mongo.users.backup.user === "") {
    user = "";
}
else {
    user = config.mongo.users.backup.user + ':' + config.mongo.users.backup.pass;
}
url = 'mongodb://' + user + config.mongo.backupUrl + "/" + config.mongo.backupDB;
var backUpDB = DB.createConnection(url);
//console.log(backUpDB);
*/



// object to hold the schemas

var objects = {};
var backUp = {};

//module.exports.objects = objects;



// segment names and id's

objects.segments = {
    name: {
        type: String,
        unique: true
    }
};

var segmentsSchema = new DB.Schema(objects.segments, {
    collection: 'segments'
});

module.exports.segments = DB.model('segments', segmentsSchema);
backUp.segments = backUpDB.model('segments', segmentsSchema);

// users
/*
objects.user = {
    email: {
        type: String,
        required: true,
        unique: true
    },
    type: {
        type: String,
        default: "admin"
    },
    name: {
        type: String,
        default: "unknown user",
        required: true
    },
    salt: String,
    hash: String,
    username: String
};

var userSchema = new DB.Schema(objects.user, {
    collection: 'user'
});
*/

objects.user = {
    access: {
        type: Number,
        default: "0"
    },
    local: {
        email: String,
        password: {
            options: String,
            salt: String,
            hash: String
        }
    },
    facebook: {
        id: String,
        token: String,
        email: String,
        name: String
    },
    twitter: {
        id: String,
        token: String,
        displayName: String,
        username: String
    },
    google: {
        id: String,
        token: String,
        email: String,
        name: String
    },
    details: {
        firstName: String,
        middleNames: [String],
        secondName: String
    }
};

var userSchema = new DB.Schema(objects.user, {
    collection: 'user'
});

/*
userTestSchema.methods.generateHash = function(password) {
    argon.generateSalt(32).then(salt => {
    //console.log(salt);
    argon.hash('password', salt).then(hash => {
        // ... 
        //console.log(hash);
        return hash;
    }).catch(err => {
        console.log(err);
        // ... 

    });
});
    //return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userTestSchema.methods.validPassword = function(password) {
    //return bcrypt.compareSync(password, this.local.password);
};

*/
//module.exports.userTest = DB.model('user', userSchema);

/*
passportLocalMongoose.argon = {
    function(){
        console.log('ssdfsdfdssdfsdfsdfsdfs');
    }
};

console.log(passportLocalMongoose);*/

//userSchema.plugin(passportLocalMongoose);

module.exports.user = DB.model('user', userSchema);
backUp.user = backUpDB.model('user', userSchema);

// objects to be reused in schemans, not subdocuments, there is no need, subdocuments will add unecessary id's and indexes

objects.scripts = new DB.Schema({
    mime: String,
    name: {
        type: String
            //, index : { unique: true, sparse: true }
    },
    text: String,
    min: String
});

objects.script = {
    _id: false,
    path: String,
    static: String,
    cdn: String,
    text: String,
    min: String //minified as above
        ,
    draft: String,
    onload: String,
    requires: {
        text: String
    },
    browser: [objects.scripts],
    require: [objects.scripts],
    server: [objects.scripts],
    notes: String,
    updated: {
        type: Date,
        default: Date.now
    },
    mime: String
};


objects.htmldata =
    new DB.Schema({
        _id: false,
        templateid: {
            type: DB.Schema.ObjectId,
            ref: 'segments'
        },
        moduleid: {
            type: DB.Schema.ObjectId,
            ref: 'module'
        },
        tag: String,
        nested: Number,
        classAttr: [{
            name: String
        }],
        id: [{
            name: String
        }],
        attr: [{
            name: String,
            value: String
        }],
        text: String,
        mime: String,
    });
// add recursive functionality
objects.htmldata.add({
    data: [objects.htmldata]
});

objects.html = {
    _id: false,
    templateid: {
        type: DB.Schema.ObjectId,
        ref: 'segments'
    },
    mime: String,
    position: Number,
    data: [objects.htmldata]
        /*data: [{
            templateid: {
                type: db.DB.Schema.ObjectId,
                ref: 'segments'
            },
            moduleid: {
                type: db.DB.Schema.ObjectId,
                ref: 'module'
            },
            tag: String,
            nested: Number,
            classAttr: [{
                name: String
            }],
            id: [{
                name: String
            }],
            attr: [{
                name: String,
                value: String
            }],
            text: String,
            mime: String,
            code : [objects.htmldata]
        }]*/
};

objects.pagedata = {
    _id: false,
    requires: {
        text: String
    },
    head: {
        html: [objects.html],
        //html:[],
        constants: {
            html: String,
            doctype: String,
            title: String,
            meta: {
                name: {
                    description: String,
                    applicationName: String,
                    author: String,
                    generator: String,
                    keywords: String
                },
                charset: String
            }
        }
    },
    resources: {
        scripts: objects.script,
        css: [{
            _id: false,
            query: String,
            name: String,
            text: String,
            min: String //minified as above
                ,
            notes: String,
            updated: {
                type: Date,
                default: Date.now
            },
            mime: String
        }]
    },
    html: [objects.html]
        //html:[]
};

objects.data = {
    slaves: [{
        _id: false,
        type: DB.Schema.ObjectId,
        ref: 'module'
    }],
    master: {
        type: DB.Schema.ObjectId,
        ref: 'module'
    },
    name: {
        type: String,
        required: true,
        unique: true
    },
    hash: {
        js: String,
        css: String
    },
    type: String,
    template: {
        type: DB.Schema.ObjectId,
        ref: 'template'
    },
    tags: String,
    cat: String,
    freq: String,
    priority: String,
    robots: Boolean,
    notes: {
        type: String,
        default: ""
    },
    active: {
        type: Boolean,
        default : true
    },
    secure: {
        type: Boolean,
        default: false
    },
    access: String,
    position: Number,
    draft: objects.pagedata,
    data: objects.pagedata,
    published: {
        type: Boolean,
        default: false
    },
    date: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date,
        default: Date.now
    },
    images: [{
        _id: false,
        type: DB.Schema.ObjectId,
        ref: 'images'
    }],
    assets: [{
        _id: false,
        type: DB.Schema.ObjectId,
        ref: 'assets'
    }],
    modules: [{
        _id: false,
        type: DB.Schema.ObjectId,
        ref: 'module'
    }]
};



/*----------------------------------------------------------------------------*/

//var dataSchema = new db.Schema(objects.data, {collection:'data'});
//module.exports.data = db.DB.model('data', dataSchema);

var pageSchema = new DB.Schema(objects.data, {
    collection: 'page'
});
module.exports.page = DB.model('page', pageSchema);
backUp.page = backUpDB.model('page', pageSchema);

var templateSchema = new DB.Schema(objects.data, {
    collection: 'template'
});
module.exports.template = DB.model('template', templateSchema);
backUp.template = backUpDB.model('template', templateSchema);

var moduleSchema = new DB.Schema(objects.data, {
    collection: 'module'
});
module.exports.module = DB.model('module', moduleSchema);
backUp.module = backUpDB.model('module', moduleSchema);

objects.trash = objects.data;

objects.trash.original = {
    id: {
        type: String
    },
    deleteDate: {
        type: Date,
        default: Date.now
    },
    details: String
};

objects.trash.name = {
    type: String,
    required: false,
    unique: false
};

var trashSchema = new DB.Schema(objects.trash, {
    collection: 'trash'
});

//trashSchema.set('versionKey', false);

module.exports.trash = DB.model('trash', trashSchema);
backUp.trash = backUpDB.model('trash', trashSchema);


/*----------------------------------------------------------------------------*/

// pages
/*
objects.page = {
    name : { type: String, required: true, unique: true }
    , template : {type : db.DB.Schema.ObjectId, ref : 'template'}
    , title : String
    , desc : String
    , tags : String
    , cat : String
    , freq : String
    , priority : String
    , robots : Boolean
    , notes : String
    , secure : { type: Boolean, default:false }
    , accesslevel : String
    , html : [
            {templateid : {type : db.DB.Schema.ObjectId, ref : 'segments'}
            , mime: String
            , position : Number
            , data :
                [{ 
                moduleId: {type : db.DB.Schema.ObjectId, ref : 'module'}
                , tag: String
                , nested: Number 
                , class: [{
                  name: String   
                }]
                , id: [{ 
                  name:String
                }]
                , text: String
                }]
            }
        ]
        , draft : [
            {templateid : {type : db.DB.Schema.ObjectId, ref : 'segments'}
            , mime: String
            , data :
                [{ 
                  moduleId: {type : db.DB.Schema.ObjectId, ref : 'module'}
                , tag: String
                , nested: Number 
                , class: [{
                  name: String   
                }]
                , id: [{ 
                  name:String
                }]
                , text: String
                }]
            }
        ]
    , published : {type: Boolean, default:false }
    , date: { type: Date, default: Date.now }
    , updated: { type: Date, default: Date.now }
    , images: [{type : db.DB.Schema.ObjectId, ref : 'images'}]
    , assets: [{type : db.DB.Schema.ObjectId, ref : 'assets'}]
    , modules : [{type : db.DB.Schema.ObjectId, ref : 'module'}]
};

objects.adminpage = objects.page;

var pageSchema = new db.Schema(objects.page, {collection:'page'});
var adminpageSchema = new db.Schema(objects.page, {collection:'adminpage'});

module.exports.page = db.DB.model('page', pageSchema);
module.exports.adminpage = db.DB.model('adminpage', adminpageSchema);

//templates

objects.template = {
    name : { type: String, required: true, unique: true }
    , notes : String
    , html : [
            {templateid : {type : db.DB.Schema.ObjectId, ref : 'segments'}
            , module : Boolean
            , position : Number
            , mime: String
            , data :
                [{ 
                insert : {type : Boolean, default:false}
                , moduleid: {type : db.DB.Schema.ObjectId, ref : 'module'}
                , tag: String
                , nested: Number 
                , class: [{
                  name: String   
                }]
                , id: [{ 
                  name:String
                }]
                , text: String
                }]
            }
        ]
        , draft : [
            {templateid : {type : db.DB.Schema.ObjectId, ref : 'segments'}
            , mime: String
            , data :
                [{ 
                  moduleid: {type : db.DB.Schema.ObjectId, ref : 'module'}
                , tag: String
                , nested: Number 
                , class: [{
                  name: String   
                }]
                , id: [{ 
                  name:String
                }]
                , text: String
                }]
            }
        ]
    , date: { type: Date, default: Date.now }
    , updated: { type: Date, default: Date.now }
    , images: [{type : db.DB.Schema.ObjectId, ref : 'images'}]
    , assets: [{type : db.DB.Schema.ObjectId, ref : 'assets'}]
    , modules : [{type : db.DB.Schema.ObjectId, ref : 'module'}]
};

var templateSchema = new db.Schema(objects.template, {collection:'template'});

module.exports.template = db.DB.model('template', templateSchema);

// modules

objects.module = {
      name : { type: String, required: true, unique: true }
    , notes : {type : String, default: false }
    , active : Boolean
    , position : Number
    , secure: { type: Boolean, default:false }
    , data : [
        {
          tag: String
        , nested: Number 
        , class: [{
          name: String   
        }]
        , id: [{ 
          name:String
        }]
        , text: String
        , mime: String
        }
    ]
    , draft : [
        {
          tag: String
        , nested: Number 
        , class: [{
          name: String   
        }]
        , id: [{ 
          name:String
        }]
        , text: String
        , mime: String
        }
    ]
    , published : {type: Boolean, default:false }
    , date: { type: Date, default: Date.now }
    , updated: { type: Date, default: Date.now }
    , images: [{type : db.DB.Schema.ObjectId, ref : 'images'}]
    , assets: [{type : db.DB.Schema.ObjectId, ref : 'assets'}]
    , scripts: {
        name : String
        , text : String
        , min : String //minified as above
        , draft : String
        , onload : String
        , notes : String
        , updated: { type: Date, default: Date.now }
        , mime: String
    }
    , css :{
        name : String
        , text : String
        , min : String //minified as above
        , draft : String
        , notes : String
        , updated: { type: Date, default: Date.now }
        , mime: String
    }
    , page : [{type : db.DB.Schema.ObjectId, ref : 'pages'}]
    , adminpage : [{type : db.DB.Schema.ObjectId, ref : 'adminpages'}]
    , template : [{type : db.DB.Schema.ObjectId, ref : 'template'}]
};

var moduleSchema = new db.Schema(objects.module, {collection:'module'});
module.exports.module = db.DB.model('module', moduleSchema);
*/
// media queries

objects.media = {
    query: String,
    columns: Number,
    name: String
};

var mediaSchema = new DB.Schema(objects.media, {
    collection: 'media'
});
module.exports.media = DB.model('media', mediaSchema);

backUp.media = backUpDB.model('media', mediaSchema);

module.exports.backUp = backUp;


objects.assets = {
    name: {
        type: String,
        required: true,
        unique: true
    }
    , type: String
};

var assetsSchema = new DB.Schema(objects.assets, {
    collection: 'assets'
});
module.exports.assets = DB.model('assets', assetsSchema);
/*
// assets

objects.assets = {
    name: String
    
    pages: [{
        name: String
    }],
    adminpages: [{
        name: String
    }]
};

var assetsSchema = new DB.Schema(objects.assets, {
    collection: 'assets'
});
module.exports.assets = DB.model('assets', assetsSchema);

//console.log(backUpDB);
// menu
/*
objects.menu = {
    name: String,
    originalId: String // original page id
        ,
    nested: Number
};

var menuSchema = new db.Schema(objects.menu, {
    collection: 'menu'
});
module.exports.menu = db.DB.model('menu', menuSchema);

// images

objects.images = {
    name: String,
    width: Number,
    height: Number,
    pages: [{
        name: String
    }],
    adminpages: [{
        name: String
    }]
};

var imagesSchema = new db.Schema(objects.images, {
    collection: 'images'
});
module.exports.images = db.DB.model('images', imagesSchema);

// assets

objects.assets = {
    name: String,
    pages: [{
        name: String
    }],
    adminpages: [{
        name: String
    }]
};

var assetsSchema = new db.Schema(objects.assets, {
    collection: 'assets'
});
module.exports.assets = db.DB.model('assets', assetsSchema);

// defaults 
/*
objects.defaults = {
      name: String
    , active: Boolean
    , template: {
        page: {type : db.DB.Schema.ObjectId, ref : 'template'}
        ,   adminpage :  {type : db.DB.Schema.ObjectId, ref : 'template'}
    }
    ,  fieldrelations : {
        date: [String]
        ,   textarea: [String]
        ,   number : [String]
        ,   boolean : [String]
        ,   disabled : [String]
        ,   array : [String]
    }
    ,   admincollections : [{
        name : String
        ,   level : String
    }]
    , segments: [{
    name : {type : String, unique : true}
}]
};

var defaultsSchema = new db.Schema (objects.defaults, {collection:'defaults'});
module.exports.defaults = db.DB.model('defaults', defaultsSchema);
*/
