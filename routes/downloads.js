var mongo = require('../models/mongo');
var path = require('path');
var fs = require('fs');
//var sharp = require('sharp');
//var _ = require("lodash");


exports.display = function(req, res, next) {
    var dec = decodeURI(req.path);
    var pathArr = dec.split("/");
    if (pathArr[0] === "") {
        pathArr.shift();
    }
    pathArr.shift();
    //console.log(pathArr);
    mongo.assets.findOne({
        "name": pathArr[0]
    }, function(err, data) {
        if (err) {
            console.log(err);
            res.status(404).send('error');
        }
        else {
            //console.log(data);
            if (data) {
                var pathToFile = path.join(__dirname, '../assets/downloads/') + data._id;
                fs.stat(pathToFile, function(err, stats) {
                    if (err) {
                        console.log(err);
                        res.status(404).send('cannot find file');
                    }
                    else {
                        res.sendFile(pathToFile);
                    }
                });
            }
            else {
                res.status(404).send('no file found');
            }
        }
    });
};