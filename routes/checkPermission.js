var checkPermission = function(user, levelRequired, res) {
    if (user && user.access > levelRequired) {
        return true;
    }
    else {
        res.status(401).send({
            error: 'You do not have permission to perform this task.'
        });
    }
};

module.exports = checkPermission;