var formidable = require('formidable');
var fs = require('fs');
var sharp = require('sharp');
var mongo = require('../models/mongo');
var checkPermission = require('./checkPermission');
var folder = "./assets";

var _ = require('lodash');
var fn = require('../lib/fn');
//var pdfToPng = require('pdf-to-png');

exports.upload = function(req, res, next) {
    if (checkPermission(req.user, 2, res)) {
        var form = new formidable.IncomingForm();
        form.uploadDir = folder + "/tmp";
        form.parse(req, function(err, fields, files) {
            if (err) {
                res.send(err);
            }
            else {
                var file = files['uploads[]'];
                var trySave = function(newDoc) {
                    newDoc.save(function(err, data) {
                        if (err) {
                            if (err.code === 11000) {
                                newDoc.name = fn.util.duplicates(newDoc.name);
                                trySave(newDoc);
                            }
                            else {
                                res.status(500).send({
                                    error: 'Server error, check admin log for more details'
                                });
                            }
                        }
                        else {
                            var count = [];
                            var counter = function(counted) {
                                if (!count || count.length === 3) {
                                    fs.unlink(file.path, function(err) {
                                        if (err) {
                                            console.log(err);
                                        }
                                        else {}
                                    });
                                }
                            };
                            var copyFile = function(source, target, cb) {
                                var cbCalled = false;
                                var done = function(err) {
                                    if (!cbCalled) {
                                        cb(err);
                                        cbCalled = true;
                                    }
                                };
                                var rd = fs.createReadStream(source);
                                rd.on("error", function(err) {
                                    done(err);
                                });
                                var wr = fs.createWriteStream(target);
                                wr.on("error", function(err) {
                                    done(err);
                                });
                                wr.on("close", function(ex) {
                                    done();
                                });
                                rd.pipe(wr);
                            };
                            var resizes = function(width, height, destination) {
                                sharp(file.path)
                                    .resize(width, height)
                                    .withoutEnlargement(false)
                                    .max()
                                    .toFile(folder + "/images/" + destination + data._id, function(err) {
                                        if (err) {
                                            console.log(err);
                                        }
                                        else {
                                            count.push(destination);
                                            counter(count);
                                        }
                                    });
                            };
                            switch (file.type) {
                                case "image/jpeg":
                                case "image/png":
                                    resizes(100, 100, "thumbnail/");
                                    resizes(500, 500, "small/");
                                    copyFile(file.path, folder + "/images/original/" + data._id, function(err) {
                                        if (err) {
                                            console.log(err);
                                        }
                                        else {
                                            count.push("original");
                                            counter(count);
                                        }
                                    });
                                    break;
                                default:
                                    /*pdfToPng({
                                        input: file.path,
                                        output: folder + "/downloads/thumbnails/" + data._id
                                    }, function(err) {
                                        console.log('here');
                                        console.log(err);
                                    });*/
                                    copyFile(file.path, folder + "/downloads/" + data._id, function(err) {
                                        if (err) {
                                            console.log(err);
                                        }
                                        else {
                                            count=null;
                                            counter(count);
                                        }
                                    });
                            }

                            console.log("Asset created " + file.name);
                            res.send(data);
                        }
                    });
                };
                var newDoc = new mongo.assets({
                    "name": file.name,
                    "type": file.type
                });
                trySave(newDoc);
            }
        });
    }
};

exports.delete = function(req, res, next) {
    var pathArr = req.path.split("/");
    if (pathArr[0] === "") {
        pathArr.shift();
    }
    var id = pathArr[1];
    mongo.assets.findByIdAndRemove(id, function(err, data) {
        if (err) {
            console.log(err);
            res.status(500).send();
        }
        else {
            var deleteLoop = function(folderArr, id) {
                _.each(folderArr, function(i) {
                    var path = "./assets/images/" + i + "/" + id;
                    fs.stat(path, function(err) {
                        if (err) {
                            console.log(err);
                            res.status(500).send();
                        }
                        else {
                            fs.unlink(path, function(err) {
                                if (err) {
                                    console.log(err);
                                }
                            });
                        }
                    });
                });
            };
            deleteLoop(["original", "thumbnail", "small"], id),
                console.log("deleted: " + id);
            res.send(data);
        }
    });

};

exports.update = function(req, res) {
    var pathArr = req.path.split("/");
    if (pathArr[0] === "") {
        pathArr.shift();
    }
    var id = pathArr[1];
    mongo.assets.findById(id, function(err, data) {
        if (err) {
            res.status(500).send();
        }
        else {
            data.name = req.body.name;
            data.type = req.body.type;
            var trySave = function(data) {
                data.save(function(err, response) {
                    if (err) {
                        if (err.code === 11000) {
                            data.name = fn.util.duplicates(data.name);
                            trySave(data);
                        }
                        else {
                            res.status(500).send({
                                error: 'Server error, check admin log for more details'
                            });
                        }
                    }
                    else {
                        console.log("Assest name change: " + response.name);

                        res.status(201).send(response);
                    }
                });
            };
            trySave(data);
        }
    });
};
