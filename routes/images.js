var mongo = require('../models/mongo');
var path = require('path');
var fs = require('fs');
var sharp = require('sharp');
var _ = require("lodash");


exports.display = function(req, res, next) {
    var dec = decodeURI(req.path);
    var pathArr = dec.split("/");
    if (pathArr[0] === "") {
        pathArr.shift();
    }
    pathArr.shift();
    mongo.assets.findOne({
        "name": pathArr[1]
    }, function(err, data) {
        if (err) {
            console.log(err);
            res.status(404).send('error');
        }
        else {
            if (data) {
                var pathToFile = path.join(__dirname, '../assets/images/') + pathArr[0] + "/" + data._id;
                fs.stat(pathToFile, function(err, stats) {
                    if (err) {
                        console.log(err);
                        res.status(404).send('cannot find file');
                    }
                    else {
                        res.sendFile(pathToFile);
                    }
                });
            }
            else {
                res.status(404).send('no file found');
            }
        }
    });
};


exports.rotate = function(req, res) {
    var pathArr = req.path.split("/");
    if (pathArr[0] === "") {
        pathArr.shift();
    }
    var id = pathArr[2];
    var angle = pathArr[3];
    if (!angle) {
        angle = parseInt(angle, 10);
        angle = 90;
    }
    else {
        angle = parseInt(angle, 10);
        angle += 90;
    }
    if (angle === 360) {
        angle = 0;
    }
    var count = [];
    var processed = true;
    var end = function(count, processed) {
        if (!processed) {
            res.status(500).send();
        }
        else if (count.length === folders.length) {
            res.send("rotated");
        }
    };
    var folders = ["original", "small", "thumbnail"];
    _.each(folders, function(folder) {
        var pathToFile = path.join(__dirname, '../assets/images/') + folder + "/" + id;
        fs.stat(pathToFile, function(err, stats) {
            if (err) {
                processed = false;
                console.log(err);
                end(count, processed);
            }
            else {
                var tmpFile = path.join(__dirname, '../assets/tmp/') + "/" + id + folder;

                var copyFile = function(source, target, cb) {
                    var cbCalled = false;
                    var done = function(err) {
                        fs.unlink(source, function(err2) {
                            if (err2) {
                                cb(err);
                                cbCalled = true;
                            }
                        });
                        if (!cbCalled) {
                            cb(err);
                            cbCalled = true;
                        }
                    };
                    var rd = fs.createReadStream(source);
                    rd.on("error", function(err) {
                        done(err);
                    });
                    var wr = fs.createWriteStream(target);
                    wr.on("error", function(err) {
                        done(err);
                    });
                    wr.on("close", function(ex) {
                        done();
                    });
                    rd.pipe(wr);
                };

                sharp(pathToFile)
                    .rotate(angle)
                    .toFile(tmpFile, function(err) {
                        if (err) {
                            console.log(err);
                        }
                        else {
                            copyFile(tmpFile, pathToFile, function() {
                                count.push(folder);
                                //console.log(folder);
                                end(count, processed);
                            });
                        }
                    });
            }
        });
    });
};
