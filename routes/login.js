//var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var svgCaptcha = require('svg-captcha');
var fn = require('../lib/fn');
var _ = require('lodash');
var log = fn.log;
var config = require('../config');
//var mongo = require('../models/mongo');

//var user = require('./../models/mongo').user;
//log(user);

//passport.use(new LocalStrategy(user.authenticate()));
//passport.serializeUser(user.serializeUser());
//passport.deserializeUser(user.deserializeUser());


/*passport.use('local-login', new LocalStrategy(
    function(email, password, done) {
        log(email);
        log(password);
        log('ssd');
        user.findOne({
            email: email
        }, function(err, user) {
            if (err) {
                return done(err);
            }
            if (!user) {
                return done(null, false, {
                    message: 'Incorrect email.'
                });
            }
            if (!user.validPassword(password)) {
                return done(null, false, {
                    message: 'Incorrect password.'
                });
            }
            return done(null, user);
        });
    }
));

passport.serializeUser(function(user, cb) {
    cb(null, user.id);
});

passport.deserializeUser(function(id, cb) {
    user.findById(id, function(err, user) {
        if (err) {
            return cb(err);
        }
        cb(null, user);
    });
});*/




module.exports = function(app, passport) {

    return function(req, res, next) {
        if (!req.session.fails) {
            req.session.fails = 0;
        }
        if (!req.session.captcha) {
            req.session.captcha = false;
            req.body.captcha = false;
        }
        if (!req.body.captcha) {
            req.body.captcha = false;
        }

        switch (req.params.method) {
            case "login":
                /*var captcha = svgCaptcha.create({
                    size: 4,
                    noise: 2,
                    color: true
                });*/
                passport.authenticate('local-login', function(err, user, info) {
                        if (err) {
                            log(err);
                            res.status(501).send();
                        }
                        if (info) {
                            log("\nLogin FAILED, " + info.message + "\n");
                        }
                        if (!user) {
                            req.session.fails++;
                            if (req.session.fails <= config.login.sescaptcha) {
                                req.session.login = [2, "Incorrect password or username"];
                                res.send(req.session.login);
                            }
                            else if (req.session.fails <= config.login.ipblock) {
                                req.session.captcha = true;
                                req.session.login = [3, "Incorrect password or username"];
                                res.send(req.session.login);
                            }
                            else {
                                req.session.login = [1, "blocked"];
                                res.send(req.session.login);
                            }
                        }
                        else {
                            req.logIn(user, function() {
                                //log(req.user);
                                req.session.login = [4, "logged in as " + req.user.local.email, req.user.details.firstName];
                                req.session.captcha = false;
                                req.session.fails = 0;
                                res.send(req.session.login);
                            });
                        }
                    })
                    (req, res, next);
                break;
            case "logout":
                req.session.login = [0, ""];
                req.logout();
                res.send(req.session.login);
                break;
            case "register":
                //force a log out
                req.session.login = [0, ""];
                req.logout();

                passport.authenticate('local-signup', function(err, user, info) {
                    if (err) {
                        log(err);
                        res.status(501).send();
                    }
                    else {
                        req.logIn(user, function() {
                            //log(req.user);
                            if(!user){
                                res.status(500);
                                res.send("register error");
                            } else {
                            req.session.login = [4, "logged in as " + req.user.local.email, req.user.details.firstName];
                            req.session.captcha = "";
                            req.session.fails = 0;
                            res.send(req.session.login);
                            }
                        });
                    }
                })(req, res, next);
                break;
            default:
                res.status(500).send();
                break;
        }
    };
};
