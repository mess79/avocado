//CRUD API
module.exports = function(app, passport) {
var authenticate = require('./login');
var main = require('./crud');
var assets = require('./assets');
var images = require('./images');
var downloads = require('./downloads');

app.post("/api/*", main.api.create);
app.get("/api/*", main.api.read);
app.put("/api/*", main.api.update);
app.delete("/api/*", main.api.delete);
app.get('/test', main.test);
app.post('/beautify/:method', main.beautify);
app.get('/images/icons/*', main.icon);
//app.get('/auth/:method', main.login);
app.get('/auth/:method', authenticate(app, passport));
app.post('/auth/:method', authenticate(app, passport));
//app.post('/auth/:method', main.login);
app.get('/captcha', main.captcha);
app.get('/captcha/*', main.captcha);
app.get('/sitemap.xml', main.sitemap);
app.get('/robots.txt', main.robots);
app.get('/scripts/js/*', main.script);
app.get('/scripts/css/*', main.script);
app.post('/images/rotate/*', images.rotate);
app.get('/images/*', images.display);
app.get('/downloads/*', downloads.display);
app.post('/assets/*', assets.upload);
app.put('/assets/*', assets.update);
app.delete('/assets/*', assets.delete);
app.get('/*', main.file);
};