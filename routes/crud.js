// output routes

var mongo = require('../models/mongo');
//var passport = require('passport');
//var ccap = require('ccap');
var fn = require('../lib/fn');
var _ = require('lodash');
var log = fn.log;
//var fs = require('fs');
var mongoose = require('mongoose');
var beautify = require('js-beautify');
var UglifyJS = require("uglify-js");
//var CleanCSS = require('clean-css');
var svgCaptcha = require('svg-captcha');
var checkPermission = require('./checkPermission');

/*var LocalStrategy = require('passport-local').Strategy;
var user = require('./../models/mongo').user;
passport.use(new LocalStrategy(user.authenticate()));
passport.serializeUser(user.serializeUser());
passport.deserializeUser(user.deserializeUser());*/
//var argon = require('argon2');
//console.log(argon);

//var uglifyCss = require('../node_modules/uglifycss/uglifycss-lib');
var config = require('../config');
if (config.port == "80") {
    config.port = "";
}
if (config.secureport == "443") {
    config.secureport = "";
}
/*var port = (config.port);
if (config.port !== "") {
    port = ":" + config.port;
}
var secureport = (config.secureport);
if (config.secureport !== "") {
    secureport = ":" + config.secureport;
}*/

// sitemap

exports.sitemap = function(req, res) {
    fn.render.sitemap(req.protocol, function(data) {
        res.send(data);
    });
};

exports.robots = function(req, res) {
    fn.render.robots(function(data) {
        res.send(data);
    });
};

exports.script = function(req, res, next) {
    var path = req.path.split("/");
    if (path[0] === "") {
        path.shift();
    }
    path.splice(2, 0, "output");
    path = path.join("/");
    var options = {
        root: './lib/',
        dotfiles: 'deny',
        headers: {
            "Cache-Control": "public, max-age=31536000"
        }
    };

    res.sendFile(path, options, function(err) {
        if (err) {
            if (err.code === "ECONNABORT" && res.statusCode == 304) {
                console.log('Cache used for ' + path);
            }
            else {
                console.error("SendFile error:", err, " (status: " + err.status + ")");
                if (err.status) {
                    res.status(err.status).end();
                }
            }
        }
    });
};

/*exports.css = function(req, res, next) {
    var path = req.path.split("/");
    if (path[0] === "") {
        path.shift();
    }
    path.splice(2, 0, "output");
    path = path.join("/");
    var options = {
        root: './lib/',
        dotfiles: 'deny',
        headers: {
            "Cache-Control":"public, max-age=31536000"
        }
    };

    res.sendFile(path, options, function(err) {
        if (err) {
            if (err.code === "ECONNABORT" && res.statusCode == 304) {
                console.log('Cache used for ' + path);
            }
            else {
                console.error("SendFile error:", err, " (status: " + err.status + ")");
                if (err.status) {
                    res.status(err.status).end();
                }
            }
        }
    });
};*/

// authentication (passportjs);

// register a new user
/*
exports.register = function(req, res) {
    res.render('register', {});
};
var user = mongo.user;
exports.registerpost = function(req, res) {
    user.register(new user({
        username: req.body.username,
        email: req.body.email
    }), req.body.password, function(err, account) {
        if (err) {
            console.log(err);
            return res.render('register', {
                account: account
            });
        }
        res.redirect('/');
    });
};*/

// login
/*
exports.login = function(req, res, next) {
    if (!req.session.fails) {
        req.session.fails = 0;
    }
    if (!req.session.captcha) {
        req.session.captcha = "";
        req.body.captcha = "";
    }
    if (!req.body.captcha) {
        req.body.captcha = "";
    }

    switch (req.params.method) {
        case "login":
            var captcha = svgCaptcha.create({
                size: 4,
                noise: 2,
                color: true
            });
            if (req.body.captcha === req.session.captcha) {
                passport.authenticate('local', function(err, user, info) {
                        if (err) {
                            log(err);
                        }
                        if (info) {
                            \nLogin FAILED, " + info.message + "\n");
                        }
                        if (!user) {
                            req.session.fails++;
                            if (req.session.fails <= config.login.sescaptcha) {
                                req.session.login = [2, "Incorrect password or username"];
                                res.send(req.session.login);
                            }
                            else if (req.session.fails <= config.login.ipblock) {
                                req.session.captcha = captcha.text;
                                req.session.login = [3, "Incorrect password or username"];
                                res.send(req.session.login);
                            }
                            else {
                                req.session.login = [1, "blocked"];
                                res.send(req.session.login);
                            }
                        }
                        else {
                            req.logIn(user, function() {
                                req.session.login = [4, "logged in as " + req.user.username];
                                req.session.captcha = "";
                                req.session.fails = 0;
                                res.send(req.session.login);
                            });
                        }
                    })
                    (req, res, next);
            }
            else {
                req.session.fails++;
                req.session.captcha = captcha.text;
                req.session.login = [3, "Captcha Failed"];
                res.send(req.session.login);
            }
            break;
        case "logout":
            req.session.login = [0, ""];
            req.logout();
            res.send(req.session.login);
            break;
        case "register":/*

            //var salt = argon.generateSalt(function(){
            //  log('hfghfg');
            //});
            
            var user = mongo.user;
            if(!req.body.username){
                req.body.username = "test";            }
            user.register(new user({
                username: req.body.username,
                email: req.body.email
            }), req.body.password, function(err, account) {
                if (err) {
                    console.log(err);
                    //return res.render('register', {
                      //  account: account
                    //});
                }
                res.send(req.body);
                //res.redirect('/');
            });
            //res.send(req.body);
            break;
        default:
            res.status(500).send();
            break;
    }
};*/


//generate captcha for login

exports.captcha = function(req, res) {

    var captcha = svgCaptcha.create({
        size: 4,
        noise: 2,
        color: true
    });
    req.session.captcha = captcha.text;

    res.set('Content-Type', 'image/svg+xml');
    res.status(200).send(captcha.data);

    //initiate captcha plugin
    /*var captcha = ccap({
        width: 500,
        height: 100,
        offset: 80,
        quality: 15,
        fontsize: 80,
        generate: function() {
            // get captcha text from session
            if (req.session.captcha) {
                return req.session.captcha;
            }
            else {
                return "captcha";
            }
        }
    });
    var ary = captcha.get();
    var image = ary[1];
    // stop cache of pic
    res.setHeader('Cache-Control', 'no-store');
    console.log(image);
    res.send(image);
    */
    //res.send('ssdd');
};

exports.file = function(req, res) {
    var userAccess = 0;
    if (req.user) {
        userAccess = req.user.access;
    }
    var patharr = req.path.split("/");
    if (_.first(patharr) === "") {
        patharr.shift();
    }
    if (_.last(patharr) === "") {
        patharr.pop();
    }
    // check query string, put into object variables and paths and work out if the url has index as the default page or not (any other page will have an odd number of )
    var queryObj = {};
    var clonepath = _.clone(patharr);
    if (!clonepath || (clonepath.length % 2) == 0) {
        queryObj.path = "index";
    }
    else(queryObj.path = clonepath.shift());
    for (var a = 0; clonepath.length > a; a++) {
        if (!clonepath[a]) {
            break;
        }
        queryObj[clonepath[a]] = clonepath[a + 1];
        a++;
    }
    // query for the html

    //log(req.path);

    switch (queryObj.path) {
        default: log("\nInitiated loading HTML\n");
        fn.query.data({
            title: "HTML",
            start: "page",
            paths: queryObj,
            access: userAccess,
            patharr: patharr,
            fields: {
                page: "template data modules hash",
                temp: "data.html data.head modules",
                mod: "data.head data.html"
            }
        }, function(result, pathsObj) {
            if (!result) {
                res.status(500);
                res.send("Internal Server Error");
            }
            else {
                if (!req.session.login) {
                    req.session.login = [0, ""];
                }
                fn.render.api.json(pathsObj, result, req.session.login, function(data) {
                    fn.render.api.html(data, function(data) {

                        switch (req.session.login[0]) {
                            case 0:
                            case 2:
                            case 3:
                                req.session.login[1] = "";
                        }
                        log("page http or https: " + req.protocol);

                        if (pathsObj.status) {
                            res.status(pathsObj.status);
                        }
                        res.send(data);
                    });
                });
            }
        });
        break;
        case "template":
                log("\nInitiated loading Template HTML\n");
            //log(req.session);
            fn.query.data({
                title: "Template",
                start: "template",
                access: userAccess,
                paths: queryObj,
                fields: {
                    temp: "data.html data.head modules",
                    mod: "data.head data.html"
                }
            }, function(result) {
                res.send(result);
                log("Template data sent");
            });
            break;
            /*case "scripts":
                    var str = "";
                if (queryObj.requires) {
                    str = queryObj.requires.split(".");
                    str = str[0];
                    log("\nInitiated loading Requirejs Scripts\n");
                    fn.query.data({
                        title: "REQUIRE SCRIPT",
                        start: "script",
                        paths: {
                            path: str
                        },
                        fields: {
                            page: "template modules",
                            temp: "modules",
                            mod: "name data.resources.scripts"
                        }
                    }, function(json, queryObj) {
                        fn.script.reqrender(json, function(data) {
                            if (!data) {
                                res.status(500);
                                res.send("/*\nError loading Requirejs handling page\n*/
            /*");
                                    }
                                    else {
                                        res.type("js");
                                        res.send(data);
                                        log("\nInitiated loading page Scripts\n");
                                    }
                                });
                            });
                        }
                        else if (queryObj.css) {
                            log("\nInitiated loading CSS Scripts\n");
                            fn.query.data({
                                title: "CSS",
                                start: "css",
                                paths: queryObj,
                                fields: {
                                    temp: "data.resources.css data.head modules",
                                    mod: "name position data.resources.css"
                                }
                            }, function(json, queryObj) {

                                if (queryObj.path === "error") {
                                    res.status(500);
                                    res.send("/*\nError in DB CSS Query\n*/
            /*");
                                    log("ERROR in CSS DB query");
                                }
                                else {
                                    fn.css.render(json, function(result) {
                                        res.type('css');
                                        res.send(result);
                                        log("CSS sent");
                                    });
                                }
                            });
                        }
                        else if (queryObj.js) {
                            str = queryObj.js.split(".");
                            str = str[0];
                            fn.script.render(str, function(result) {
                                if (result) {
                                    res.type("js");
                                    res.send(result);
                                }
                                else {
                                    res.status(500);
                                    res.send("/*\nError loading " + str + " Script\n*/
            /*");
                                    log("ERROR db loading " + str + " script");
                                }
                            });
                        }
                        else {
                            res.send('failed');
                            log('failed');
                        }
                        break;*/
    }
};

exports.icon = function(req, res) {
    var patharr = req.path.split("/");
    patharr.splice(0, 3);
    patharr.pop();
    //var width = (patharr.length)*128;
    var width = 512;
    var x = 0;
    var output = "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" width=\"" + width + "px\" height=\"2304px\">";
    _.each(patharr, function(i, n) {
        var color = "#" + i;
        output += "<g transform=\"translate(" + x + " 0)\">";
        output += "<svg x=\"0px\" y=\"0px\" width=\"128px\" height=\"128px\" viewBox=\"0 0 512 512\" enable-background=\"new 0 0 512 512\" xml:space=\"preserve\">";
        output += "<path fill=\"" + color + "\" id=\"sort-icon\" d=\"M256,50l132.034,176H123.966L256,50z M388.034,286H123.966L256,462L388.034,286z\"/>";
        output += "</svg>";
        output += "<svg x=\"0px\" y=\"128px\" width=\"128px\" height=\"128px\" viewBox=\"0 0 512 512\" enable-background=\"new 0 0 512 512\" xml:space=\"preserve\">";
        output += "	<path fill=\"" + color + "\" id=\"checkbox-9-icon\" d=\"M391,121v270H121V121H391z M431,81H81v350h350V81z\"/>";
        output += "</svg>";
        output += "<svg x=\"0px\" y=\"256px\" width=\"128px\" height=\"128px\" viewBox=\"0 0 512 512\" enable-background=\"new 0 0 512 512\" xml:space=\"preserve\">";
        output += "<path fill=\"" + color + "\" id=\"checkbox-4-icon\" d=\"M391,121v270H121V121H391z M431,81H81v350h350V81z M335.388,167.606L227.381,275.611l-46.783-46.794l-34.69,34.697l81.476,81.498L370.094,202.29L335.388,167.606z\"/>";
        output += "</svg>";
        output += "<svg x=\"0px\" y=\"384px\" width=\"128px\" height=\"128px\" viewBox=\"0 0 512 512\" enable-background=\"new 0 0 512 512\" xml:space=\"preserve\">";
        output += "	<path fill=\"" + color + "\" id=\"checkbox-19-icon\" d=\"M256,111c38.73,0,75.144,15.083,102.53,42.47S401,217.27,401,256s-15.083,75.144-42.47,102.53 S294.73,401,256,401s-75.144-15.083-102.53-42.47S111,294.73,111,256s15.083-75.144,42.47-102.53S217.27,111,256,111z M256,71 C153.827,71,71,153.828,71,256s82.827,185,185,185c102.172,0,185-82.828,185-185S358.172,71,256,71z\"/>";
        output += "	</svg>";
        output += "	<svg x=\"0px\" y=\"512px\" width=\"128px\" height=\"128px\" viewBox=\"0 0 512 512\" enable-background=\"new 0 0 512 512\" xml:space=\"preserve\">";
        output += "	<path fill=\"" + color + "\" id=\"checkbox-17-icon\" d=\"M256,111c38.73,0,75.144,15.083,102.53,42.47S401,217.27,401,256s-15.083,75.144-42.47,102.53 S294.73,401,256,401s-75.144-15.083-102.53-42.47S111,294.73,111,256s15.083-75.144,42.47-102.53S217.27,111,256,111z M256,71 C153.827,71,71,153.828,71,256s82.827,185,185,185c102.172,0,185-82.828,185-185S358.172,71,256,71z M256,161 c-52.467,0-95,42.533-95,95s42.533,95,95,95s95-42.533,95-95S308.467,161,256,161z\"/>";
        output += "</svg>";
        output += "<svg x=\"0px\" y=\"640px\" width=\"128px\" height=\"128px\" viewBox=\"0 0 512 512\" enable-background=\"new 0 0 512 512\" xml:space=\"preserve\">";
        output += "	<path fill=\"" + color + "\" id=\"lock-11-icon\" d=\"M160.667,100c-33.451,0-60.666,27.215-60.666,60.667v62.666h-50v-62.666 C50.001,99.645,99.646,50,160.667,50s110.666,49.645,110.666,110.667v62.666h-50v-62.666C221.333,127.215,194.118,100,160.667,100z M461.999,253.333V462h-296V253.333H461.999z M340.999,341c0-14.912-12.088-27-27-27s-27,12.088-27,27 c0,7.811,3.318,14.844,8.619,19.773c4.385,4.075,6.881,9.8,6.881,15.785V399.5h23v-22.941c0-5.989,2.494-11.708,6.881-15.785 C337.683,355.844,340.999,348.811,340.999,341z\"/>";
        output += "</svg>";
        output += "<svg x=\"0px\" y=\"768px\" width=\"128px\" height=\"128px\" viewBox=\"0 0 512 512\" enable-background=\"new 0 0 512 512\" xml:space=\"preserve\">";
        output += "	<path fill=\"" + color + "\" id=\"lock-3-icon\" d=\"M195.334,223.333h-50v-62.666C145.334,99.645,194.979,50,256,50c61.022,0,110.667,49.645,110.667,110.667 v62.666h-50v-62.666C316.667,127.215,289.452,100,256,100c-33.451,0-60.666,27.215-60.666,60.667V223.333z M404,253.333V462H108 V253.333H404z M283,341c0-14.912-12.088-27-27-27s-27,12.088-27,27c0,7.811,3.317,14.844,8.619,19.773 c4.385,4.075,6.881,9.8,6.881,15.785V399.5h23v-22.941c0-5.989,2.494-11.708,6.881-15.785C279.683,355.844,283,348.811,283,341z\"/>";
        output += "</svg>";
        output += "<svg x=\"0px\" y=\"896px\" width=\"128px\" height=\"128px\" viewBox=\"0 0 512 512\" enable-background=\"new 0 0 512 512\" xml:space=\"preserve\">";
        output += "	<path fill=\"" + color + "\" id=\"redo-3-icon\" d=\"M114.646,132.251c86.064-86.063,225.516-86.189,311.743-0.389l35.499-35.499L462,227.977l-131.612-0.112 l33.573-33.573c-51.748-51.324-135.301-51.197-186.886,0.389c-51.717,51.717-51.717,135.567,0,187.284l-62.429,62.429 C28.451,358.197,28.451,218.446,114.646,132.251z\"/>";
        output += "</svg>";
        output += "<svg x=\"0px\" y=\"1024px\" width=\"128px\" height=\"128px\" viewBox=\"0 0 512 512\" enable-background=\"new 0 0 512 512\" xml:space=\"preserve\">";
        output += "	<path fill=\"" + color + "\" id=\"undo-3-icon\" d=\"M397.354,132.251c-86.064-86.063-225.516-86.189-311.743-0.389l-35.499-35.5L50,227.977l131.612-0.113 l-33.573-33.573c51.748-51.324,135.301-51.198,186.886,0.388c51.717,51.717,51.717,135.568,0,187.285l62.429,62.429 C483.549,358.197,483.549,218.446,397.354,132.251z\"/>";
        output += "</svg>";
        output += "<svg x=\"0px\" y=\"1152px\" width=\"128px\" height=\"128px\" viewBox=\"0 0 512 512\" enable-background=\"new 0 0 512 512\" xml:space=\"preserve\">";
        output += "	<path fill=\"" + color + "\" id=\"save-icon\" d=\"M285.666,102.333h43V180h-43V102.333z M456,123v339H56V50h327L456,123z M150.5,202H354V80H150.5V202z M398.333,269.666H113.667v159.666h284.666V269.666z\"/>";
        output += "</svg>";

        output += "<svg x=\"0px\" y=\"1280px\" width=\"128px\" height=\"128px\" viewBox=\"0 0 512 512\" enable-background=\"new 0 0 512 512\" xml:space=\"preserve\">";
        output += "<path fill=\"" + color + "\" id=\"sort-6-icon\" d=\"M256,393.298L50,118.702h412L256,393.298z\"/>";
        output += "</svg>";
        output += "<svg x=\"0px\" y=\"1408px\" width=\"128px\" height=\"128px\" viewBox=\"0 0 512 512\" enable-background=\"new 0 0 512 512\" xml:space=\"preserve\">";
        output += "<polygon fill=\"" + color + "\" id=\"arrow-37-icon\" points=\"418.999,256.001 121.001,462 121.001,50 \"/>";
        output += "</svg>";

        output += "<svg x=\"0px\" y=\"1536px\" width=\"128px\" height=\"128px\" viewBox=\"0 0 512 512\" enable-background=\"new 0 0 512 512\" xml:space=\"preserve\">";
        output += "<path fill=\"" + color + "\" id=\"refresh-3-icon\" d=\"M373.223,142.573l-37.252,37.253c-20.225-20.224-48.162-32.731-79.021-32.731 c-61.719,0-111.752,50.056-111.752,111.776c0,0.016,0-0.016,0,0h43.412l-69.342,69.315L50,258.871h42.514c0-0.008,0,0.006,0,0 c0-90.816,73.621-164.46,164.436-164.46C302.357,94.411,343.467,112.816,373.223,142.573z M462,253.129l-69.268-69.316 l-69.342,69.316h43.412c0,0.016,0-0.017,0,0c0,61.72-50.033,111.776-111.752,111.776c-30.859,0-58.797-12.508-79.021-32.731 l-37.252,37.253c29.758,29.757,70.867,48.162,116.273,48.162c90.814,0,164.436-73.644,164.436-164.459c0-0.007,0,0.008,0,0H462z\"/>";
        output += "</svg>";
        output += "<svg x=\"0px\" y=\"1664px\" width=\"128px\" height=\"128px\" viewBox=\"0 0 512 512\" enable-background=\"new 0 0 512 512\" xml:space=\"preserve\">";
        output += "<path fill=\"" + color + "\" id=\"plus-icon\" d=\"M256,40C136.715,40,40,136.715,40,256s96.715,216,216,216s216-96.715,216-216S375.285,40,256,40z M382,292h-90v90h-72v-90h-90v-72h90v-90h72v90h90V292z\"/>";
        output += "	</svg>";

        output += "<svg x=\"0px\" y=\"1792px\" width=\"128px\" height=\"128px\" viewBox=\"0 0 512 512\" enable-background=\"new 0 0 512 512\" xml:space=\"preserve\">";
        output += "<path fill=\"" + color + "\" id=\"minus-icon\" d=\"M256,40C136.715,40,40,136.715,40,256s96.715,216,216,216s216-96.715,216-216S375.285,40,256,40z M382,292H130v-72h252V292z\"/>";
        output += "	</svg>";

        output += "<svg x=\"0px\" y=\"1920px\" width=\"128px\" height=\"128px\" viewBox=\"0 0 512 512\" enable-background=\"new 0 0 512 512\" xml:space=\"preserve\">";
        output += "<path fill=\"" + color + "\" id=\"close-icon\" d=\"M256,40C136.715,40,40,136.715,40,256s96.715,216,216,216s216-96.715,216-216S375.285,40,256,40zM330.707,362.98l-74.566-73.846L182.04,364l-32.994-32.994l73.872-74.83L148,182.031l32.994-32.994l74.786,73.846L329.688,148l33.293,33.275l-73.811,74.549L364,329.688L330.707,362.98z\"/>";
        output += "	</svg>";

        output += "<svg x=\"0px\" y=\"2048px\" width=\"128px\" height=\"128px\" viewBox=\"0 0 512 512\" enable-background=\"new 0 0 512 512\" xml:space=\"preserve\">";
        output += "<path fill=\"" + color + "\" id=\"Trash-icon\" d=\"M93.996,148v324h324.008V148H93.996z M184.004,400c0,9.931-8.084,18-18.009,18c-9.931,0-17.999-8.069-17.999-18V220c0-9.932,8.068-18,17.999-18c9.925,0,18.009,8.068,18.009,18V400z M274.004,400c0,9.931-8.084,18-17.999,18c-9.95,0-18-8.069-18-18V220c0-9.932,8.05-18,18-18c9.915,0,17.999,8.068,17.999,18V400z M364.004,400c0,9.931-8.084,18-17.998,18c-9.949,0-18-8.069-18-18V220c0-9.932,8.051-18,18-18c9.914,0,17.998,8.068,17.998,18V400z M436.004,76v36H75.996V76h102.806c16.189,0,29.355-19.775,29.355-36h95.662c0,16.225,13.148,36,29.354,36H436.004z\"/>";
        output += "	</svg>";

        output += "<svg x=\"0px\" y=\"2176px\" width=\"128px\" height=\"128px\" viewBox=\"0 0 512 512\" enable-background=\"new 0 0 512 512\" xml:space=\"preserve\">";
        output += "<path fill=\"" + color + "\" id=\"info-icon\" d=\"M256,472c119.285,0,216-96.715,216-216S375.285,40,256,40S40,136.715,40,256S136.715,472,256,472zM273.999,364h-36V220h36V364z M256,143.5c12.42,0,22.501,10.073,22.501,22.501c0,12.427-10.081,22.5-22.501,22.5c-12.419,0-22.5-10.072-22.5-22.5C233.5,153.572,243.581,143.5,256,143.5z\"/>";
        output += "	</svg>";

        output += "</g>";
        x = x + 128;
    });
    output += "</svg>";
    res.type('svg');
    res.send(output);
};

var api = {

    delete: function(req, res, next) {
        if (checkPermission(req.user, 2, res)) {
            var patharr = req.path.split("/");
            if (_.first(patharr) === "") {
                patharr.shift();
            }
            if (_.last(patharr) === "") {
                patharr.pop();
            }
            var collection = patharr[1];
            var document = patharr[2];
            if (patharr[3] === "sub") {
                var sub;
                switch (patharr[4]) {
                    case "browserScripts":
                        sub = "browser";
                        break;
                    case "requireScripts":
                        sub = "require";
                        break;
                    case "serverScripts":
                        sub = "server";
                        break;
                }
                mongo[patharr[1]].findOne({
                    _id: patharr[2]
                }, function(err, data) {
                    if (err) {
                        res.status(500).send({
                            error: 'Server error, check admin log for more details'
                        });
                        log(err);
                    }
                    else {
                        var match = _.find(data.data.resources.scripts[sub], {
                            id: patharr[5]
                        });
                        if (match) {
                            try {
                                data.data.resources.scripts[sub] = _.without(data.data.resources.scripts[sub], match);
                                data.save(function(err, data) {
                                    if (err) {
                                        res.status(500).send({
                                            error: 'Server error, check admin log for more details'
                                        });
                                        log(err);
                                    }
                                    else {
                                        if (sub === "browser" || sub === "require") {
                                            if (patharr[4] === "requireScripts") {
                                                patharr[4] = "require";
                                            }
                                            fn.util.localScripts({
                                                type: patharr[4],
                                                data: data,
                                                name: data.name,
                                                delete: true,
                                                path: patharr[1],
                                                subName: match.name,
                                                active: true,
                                                callback: function(inputProcessed) {
                                                    res.status(202).json({
                                                        "id": patharr[2],
                                                        "subID": patharr[5]
                                                    }).send();
                                                }
                                            });
                                        }
                                        else {
                                            res.status(202).json({
                                                "id": patharr[2],
                                                "subID": patharr[5]
                                            }).send();
                                        }
                                    }
                                });
                            }
                            catch (err) {
                                res.status(500).send({
                                    error: 'Server error, check admin log for more details'
                                });
                            }
                        }
                        else {
                            res.status(500).send({
                                error: 'Server error, check admin log for more details'
                            });
                        }
                    }
                });
            }
            else {
                fn.trash.delete(collection, document, function(data) {
                    res.send({
                        "collection": collection,
                        "id": document
                    });
                });
            }
        }
    },

    read: function(req, res) {
        if (checkPermission(req.user, 2, res)) {
            var patharr = req.path.split("/");
            if (_.first(patharr) === "") {
                patharr.shift();
            }
            if (_.last(patharr) === "") {
                patharr.pop();
            }
            switch (patharr[1]) {
                case "count":
                    mongo[patharr[2]].count({}, function(err, total){
                        if(err){
                            log(err);
                        } else {
                        res.send({"total" : total}); 
                        }
                    });
                    break;
                case "documentlist":
                   
                    var sort = null;
                    var skip = null;
                    var limit = null;
                    
                     var fields = 'name';
                    if (patharr[2] === "media") {
                        fields = 'name query columns';
                    }
                    
                    if (patharr[2]=== "trash") {
                        fields = "name original type";
                        sort = { _id: -1 };
                    }
                    
                     if (patharr[2]=== "user") {
                        fields = "";
                    }
                    
                    if (patharr[2]=== "assets") {
                        fields = "name type";
                        sort = {"name": 1};
                    }
                    
                    if(patharr[3]){
                        skip = parseInt(patharr[3], 10) -1;
                    }
                    
                    if(patharr[4]){
                        limit = parseInt(patharr[4], 10);
                    }
                    
                    mongo[patharr[2]].find({}, fields, function(err, list) {
                        if (err) {
                            res.status(500).send({
                                error: 'Server error, check admin log for more details'
                            });
                            log(err);
                        }
                        else {
                            res.setHeader('Cache-Control', 'no-cache');
                            res.send(list);
                        }
                    }).skip(skip).limit(limit).sort(sort);
                    break;
                default:
                    mongo[patharr[1]].findOne({
                        '_id': patharr[2]
                    }).lean().exec(function(err, json) {
                        if (err) {
                            res.status(500).send({
                                error: 'Server error, check admin log for more details'
                            });
                            log(err);
                        }
                        else {
                            res.setHeader('Cache-Control', 'no-cache');
                            res.send((json));
                        }
                    });
                    break;
            }
        }
    },

    create: function(req, res, next) {
        if (checkPermission(req.user, 2, res)) {
            var patharr = req.path.split("/");
            if (_.first(patharr) === "") {
                patharr.shift();
            }
            if (_.last(patharr) === "") {
                patharr.pop();
            }
            var trySave = function(newDoc, method) {
                
                newDoc.save(function(err) {
                    if (err) {
                        if (err.code === 11000) {
                            newDoc.name = fn.util.duplicates(newDoc.name);
                            trySave(newDoc, method);
                        }
                        else {
                            res.status(500).send({
                                error: 'Server error, check admin log for more details'
                            });
                        }
                    }
                    else {
                        log("New document " + method + ": " + newDoc.name + " in collection: " + patharr[3]);
                        
                        if(method === "DUPLICATED"){
                            fn.newSubs(newDoc, patharr[3]);
                        }
                        res.status(201).send(newDoc);
                    }
                });
            };
            if (patharr[1] === "duplicate") {
                mongo[patharr[3]].findOne({
                    '_id': patharr[2]
                }, function(err, data) {
                    if (err) {
                        res.status(500).send({
                            error: 'Server error, check admin log for more details'
                        });
                        log(err);
                    } else {
                    data._id = mongoose.Types.ObjectId();
                    data.isNew = true;
                    trySave(data, "DUPLICATED");
                    }
                });
            }
            else if (patharr[1] === "create") {
                var newDoc = new mongo[patharr[3]]({
                    "name": patharr[2],
                    "type": patharr[3]
                });
                trySave(newDoc, "CREATED");
            }
            else if (patharr[1] === "backup") {
                fn.databaseTools.backup(function(data) {
                    res.status(201).send(data);
                });
            }
        }
    },

    update: function(req, res, next) {
    
        //log(req.body);
        if (checkPermission(req.user, 2, res)) {
            var patharr = req.path.split("/");
            if (_.first(patharr) === "") {
                patharr.shift();
            }
            if (_.last(patharr) === "") {
                patharr.pop();
            }
            // check to see if updating whole model:

            if (patharr[3] === "section" && patharr[4] === "mongodb") {
                delete req.body.id;
                delete req.body.__v;
                mongo[patharr[1]].findByIdAndUpdate(req.body._id, req.body, function(err) {
                    if (err) {
                        res.status(500).send({
                            error: 'Server error, check admin log for more details'
                        });
                        log(err);
                    }
                    else {
                        res.status(201).json({
                            save: true
                        }).send();
                    }
                });
            }
            else {
                mongo[patharr[1]].findOne({
                    '_id': patharr[2]
                }, function(err, data) {
                    if (err || !data) {
                        res.status(500).send({
                            error: 'Server error, check admin log for more details'
                        });
                        log(err);
                    }
                    var pos;
                    var inputProcessed = false;
                    var returnJson = {
                        save: true
                    };

                    var end = function() {
                        if (inputProcessed) {
                            data.save(function(err, data) {
                                if (err) {
                                    res.status(500).send({
                                        error: 'Server error, check admin log for more details'
                                    });
                                    log(err);
                                }
                                else {
                                    res.status(201).json(returnJson).send();
                                }
                            });
                        }
                        else {
                            data = {
                                save: false
                            };
                            res.status(500).json(data).send();
                        }
                    };
                    switch (patharr[3]) {
                        case "section":
                            switch (patharr[4]) {
                                //case "mongodb":

                                //  break;
                                case "js":
                                    data.data.resources.scripts.text = req.body.patch;
                                    fn.util.localScripts({
                                        type: "js",
                                        data: data,
                                        name: data.name,
                                        path: patharr[1],
                                        active: true,
                                        callback: function(resultProcessed, resultData) {
                                            inputProcessed = resultProcessed;
                                            if (resultData) {
                                                data = resultData;
                                            }
                                            end();
                                        }
                                    });
                                    /*try {
                                        data.data.resources.scripts.min = UglifyJS.minify(req.body.patch, {
                                            fromString: true
                                        }).code;
                                        inputProcessed = true;
                                        end();
                                    }
                                    catch (err) {
                                        log(err);
                                        inputProcessed = false;
                                        end();
                                    }*/
                                    break;
                                case "jsonload":
                                    data.data.resources.scripts.onload = req.body.patch;
                                    inputProcessed = true;
                                    end();
                                    break;
                                case "reqs":
                                    data.data.resources.scripts.requires.text = req.body.patch;
                                    fn.util.localScripts({
                                        type: "reqs",
                                        data: data,
                                        name: data.name,
                                        path: patharr[1],
                                        active: true,
                                        callback: function(resultProcessed, resultData) {
                                            inputProcessed = resultProcessed;
                                            if (resultData) {
                                                data = resultData;
                                            }
                                            end();
                                        }
                                    });
                                    break;
                                case "pug":
                                    data.data.html[0].data[0] = req.body.patch;
                                    //data.data.html[0].data[0].text = req.body.patch.text;
                                    inputProcessed = true;
                                    end();
                                    break;
                                case "body":
                                    data.data.html = req.body.patch;
                                    returnJson = data.data.html;
                                    inputProcessed = true;
                                    end();
                                    break;
                                case "head":
                                    data.data.head = req.body.patch;
                                    returnJson = data.data.head;
                                    inputProcessed = true;
                                    end();
                                    break;
                                case "cssEditor":
                                    pos = _.findIndex(data.data.resources.css, {
                                        query: patharr[5]
                                    });
                                    if (pos >= 0) {
                                        data.data.resources.css[pos].text = req.body.patch.text;
                                    }
                                    else {
                                        data.data.resources.css.push({
                                            "query": patharr[5],
                                            "text": req.body.patch.text
                                        });
                                    }
                                    fn.util.localScripts({
                                        type: "css",
                                        data: data,
                                        name: data.name,
                                        path: patharr[1],
                                        active: true,
                                        callback: function(resultProcessed, resultData) {
                                            inputProcessed = resultProcessed;
                                            if (resultData) {
                                                data = resultData;
                                            }
                                            end();
                                        }
                                    });
                                    break;
                                case "browserScripts":
                                    pos = _.findIndex(data.data.resources.scripts.browser, {
                                        id: patharr[5]
                                    });
                                    if (pos >= 0) {
                                        data.data.resources.scripts.browser[pos] = req.body.patch;
                                        try {
                                            data.data.resources.scripts.browser[pos].min = UglifyJS.minify(req.body.patch.text, {
                                                fromString: true
                                            }).code;
                                            inputProcessed = true;
                                        }
                                        catch (err) {
                                            log(err);
                                            inputProcessed = false;
                                        }
                                    }
                                    else {
                                        var checkName = function(name) {
                                            pos = _.findIndex(data.data.resources.scripts.browser, {
                                                name: name
                                            });
                                            if (pos >= 0) {
                                                name = fn.util.duplicates(name);
                                                checkName(name);
                                            }
                                            else {
                                                data.data.resources.scripts.browser.push({
                                                    "name": name
                                                });
                                                returnJson = data.data.resources.scripts.browser[data.data.resources.scripts.browser.length - 1];
                                                inputProcessed = true;
                                            }
                                        };
                                        try {
                                            checkName(req.body.patch.name);
                                        }
                                        catch (err) {
                                            log(err);
                                        }
                                    }
                                    fn.util.localScripts({
                                        type: "browserScripts",
                                        data: data,
                                        name: data.name,
                                        path: patharr[1],
                                        subName: req.body.patch.name,
                                        active: true,
                                        callback: function(resultProcessed, resultData) {
                                            inputProcessed = resultProcessed;
                                            if (resultData) {
                                                data = resultData;
                                            }
                                            end();
                                        }
                                    });
                                    break;
                                case "requireScripts":
                                    pos = _.findIndex(data.data.resources.scripts.require, {
                                        id: patharr[5]
                                    });
                                    if (pos >= 0) {
                                        data.data.resources.scripts.require[pos] = req.body.patch;
                                        inputProcessed = true;
                                    }
                                    else {
                                        var checkReqName = function(name) {
                                            pos = _.findIndex(data.data.resources.scripts.require, {
                                                name: name
                                            });
                                            if (pos >= 0) {
                                                name = fn.util.duplicates(name);
                                                checkReqName(name);
                                            }
                                            else {
                                                data.data.resources.scripts.require.push({
                                                    "name": name
                                                });
                                                returnJson = data.data.resources.scripts.require[data.data.resources.scripts.require.length - 1];
                                                inputProcessed = true;
                                            }
                                        };
                                        try {
                                            checkReqName(req.body.patch.name);
                                        }
                                        catch (err) {
                                            log(err);
                                        }
                                    }
                                    fn.util.localScripts({
                                        type: "require",
                                        data: data,
                                        name: data.name,
                                        path: patharr[1],
                                        subName: req.body.patch.name,
                                        active: true,
                                        callback: function(resultProcessed, resultData) {
                                            inputProcessed = resultProcessed;
                                            if (resultData) {
                                                data = resultData;
                                            }
                                            end();
                                        }
                                    });
                                    break;
                                case "serverScripts":
                                    pos = _.findIndex(data.data.resources.scripts.server, {
                                        id: patharr[5]
                                    });
                                    if (pos >= 0) {
                                        data.data.resources.scripts.server[pos] = req.body.patch;
                                        try {
                                            data.data.resources.scripts.server[pos].min = UglifyJS.minify(req.body.patch.text, {
                                                fromString: true
                                            }).code;
                                            inputProcessed = true;
                                        }
                                        catch (err) {
                                            log(err);
                                            inputProcessed = false;
                                        }
                                    }
                                    else {
                                        var checkServerName = function(name) {
                                            pos = _.findIndex(data.data.resources.scripts.server, {
                                                name: name
                                            });
                                            if (pos >= 0) {
                                                name = fn.util.duplicates(name);
                                                checkServerName(name);
                                            }
                                            else {
                                                data.data.resources.scripts.server.push({
                                                    "name": name
                                                });
                                                returnJson = data.data.resources.scripts.server[data.data.resources.scripts.server.length - 1];
                                                inputProcessed = true;
                                            }
                                        };
                                        try {
                                            checkServerName(req.body.patch.name);
                                        }
                                        catch (err) {
                                            log(err);
                                        }
                                    }
                                    end();
                                    break;
                            }
                            break;
                        case "info":
                            var text = req.body.patch;
                            if (text.name !== undefined) {
                                data.name = text.name;
                            }
                            if (text.type !== undefined) {
                                data.type = text.type;
                            }
                            if (text.template !== undefined) {
                                data.template = text.template;
                            }
                            if (text.freq !== undefined) {
                                data.freq = text.freq;
                            }
                            if (text.priority !== undefined) {
                                data.priority = text.priority;
                            }
                            if (text.robots !== undefined) {
                                data.robots = text.robots;
                            }
                            if (text.active !== undefined) {
                                data.active = text.active;
                            }
                            if (text.secure !== undefined) {
                                data.secure = text.secure;
                            }
                            if (text.access !== undefined) {
                                data.access = text.access;
                            }
                            if (text.notes !== undefined) {
                                data.notes = text.notes;
                            }
                            inputProcessed = true;
                            end();
                            break;
                        case "modules":
                            data.modules = req.body.patch;
                            inputProcessed = true;
                            end();
                            break;
                    }
                });
            }
        }
    }

    /*update: function(req, res, next) {
            var error = null;
            var patharr = req.path.split("/");
            if (_.first(patharr) === "") {
                patharr.shift();
            }
            if (_.last(patharr) === "") {
                patharr.pop();
            }

            mongo[patharr[1]].findOne({
                '_id': patharr[2]
            }, function(err, data) {

                if (err) {
                    res.status(500).send({
                        error: 'Server error, check admin log for more details'
                    });
                    log(err);
                }

                switch (patharr[3]) {
                    case "js":
                        //var min = UglifyJS.minify(req.body.text, {fromString: true});
                        //log(min.code);

                        data.data.resources.scripts.text = req.body.text;
                        try {
                            data.data.resources.scripts.min = UglifyJS.minify(req.body.text, {
                                fromString: true
                            }).code;
                        }
                        catch (err) {
                            error = err;
                        }
                        break;
                    case "css":
                        var pos = _.findIndex(data.data.resources.css, {
                            query: req.body.id
                        });
                        try {
                            var minified = new CleanCSS().minify(req.body.text).styles;
                            if (pos >= 0) {
                                data.data.resources.css[pos].text = req.body.text;
                                data.data.resources.css[pos].min = minified;
                            }
                            else {
                                data.data.resources.css.push({
                                    "query": req.body.id,
                                    "text": req.body.text,
                                    "min": minified
                                });
                            }
                        }
                        catch (err) {
                            error = err;
                        }
                        break;
                    case "onload":
                        data.data.resources.scripts.onload = req.body.text;
                        break;
                    case "require":
                        data.data.resources.scripts.requires.text = req.body.text;
                        break;
                    case "pug":
                    case "jade":
                        data.data.html[0].data[0].text = req.body.text;
                        break;
                    case "modules":
                        //log(data);
                        data.modules = JSON.parse(req.body.text);
                        break;
                    case "info":
                        var text = JSON.parse(req.body.text);
                        //log(text);
                        if (text.name !== undefined) {
                            data.name = text.name;
                        }
                        if (text.type !== undefined) {
                            data.type = text.type;
                        }
                        if (text.template !== undefined) {
                            data.template = text.template;
                        }
                        if (text.freq !== undefined) {
                            data.freq = text.freq;
                        }
                        if (text.priority !== undefined) {
                            data.priority = text.priority;
                        }
                        if (text.robots !== undefined) {
                            data.robots = text.robots;
                        }
                        if (text.active !== undefined) {
                            data.active = text.active;
                        }
                        if (text.secure !== undefined) {
                            data.secure = text.secure;
                        }
                        if (text.access !== undefined) {
                            data.access = text.access;
                        }
                        if (text.notes !== undefined) {
                            data.notes = text.notes;
                        }
                        break;
                }
                if (!error) {
                    data.save(function(err) {
                        if (err) {
                            res.status(500).send({
                                error: 'Server error, check admin log for more details'
                            });
                            log(err);
                        }
                        else {
                            //res.status(500).send({ error: 'Server error, check admin log for more details' });

                            res.status(201).send(patharr[3] + " updated");
                        }
                    });
                }
                else {
                    res.status(500).send({
                        error: 'Server error, check admin log for more details'
                    });
                    log(error);
                }
            });
        }
        //}*/
};

exports.api = api;

exports.beautify = function(req, res, next) {
    var output = "";
    //console.log(req.body.text);
    switch (req.params.method) {
        case "js":
            try {
                output = beautify.js_beautify(req.body.text, {
                    "jslint_happy": true,
                    "comma_first": true
                });

            }
            catch (err) {
                res.status(500);
                output(err);
            }
            break;
        case "css":
            try {
                output = beautify.css_beautify(req.body.text, {});


            }
            catch (err) {
                res.status(500);
                output(err);
            }


    }
    res.send(output);
};



////////////////////////////////////////////////////////////////////////////////////////////////////

/* 

                                Experiments
                                
                                
*/

////////////////////////////////////////////////////////////////////////////////////////////////////


exports.streamtest = function(req, res, next) {
    var output = '';
    var stream = require('stream');

    var writestream = new stream.Stream();
    writestream.writable = true;
    writestream.write = function(data) {
        log(data);
        log("new line");
    };
    writestream.end = function(data, next) {
        log(data);
        log('here');
        res.send('sds');
    };
    mongo.template.find({
        'active': true
    }).lean().stream().pipe(writestream, function(data) {
        res.send('data');
    });
};


exports.test = function(req, res) {

    /* var obj = new mongo.page({
         "name": "NEW TEST PAGE",
         "modules": [],
         "assets": [],
         "images": [],
         "updated": {
             "$date": "2015-06-23T17:24:44.990Z"
         },
         "date": {
             "$date": "2015-06-23T17:24:44.990Z"
         },
         "published": false,
         "draft": {
             "html": [],
             "resources": {
                 "css": {
                     "updated": {
                         "$date": "2015-06-23T17:24:44.990Z"
                     }
                 },
                 "scripts": {
                     "updated": {
                         "$date": "2015-06-23T17:24:44.990Z"
                     }
                 }
             },
             "head": {
                 "html": []
             }
         },
         "data": {
             "html": [],
             "resources": {
                 "css": {
                     "updated": {
                         "$date": "2015-06-23T17:24:44.989Z"
                     }
                 },
                 "scripts": {
                     "updated": {
                         "$date": "2015-06-23T17:24:44.989Z"
                     }
                 }
             },
             "head": {
                 "html": []
             }
         },
         "secure": false,
         "notes": "false",
     });*/
    //obj.save(function(err){
    ///i/f (err) return console.error(err);
    //});

    var query = {
        name: 'NEW TEST PAGE'
    };
    //mongo.page.update();

    var update = {
        published: false,
        updated: new Date(),
        data: {
            html: [{
                data: [{
                    tag: "section"
                }]
            }, {
                data: [{
                    data: [{
                        tag: "section"
                    }]
                }, {
                    tag: "div",
                    data: [{
                        data: [{
                            tag: "div",
                            data: [{
                                tag: "div",
                                data: [{
                                    data: [{
                                        tag: "div",
                                        data: []
                                    }]
                                }]
                            }, {
                                data: [{
                                    tag: "section"
                                }]
                            }]
                        }]
                    }]
                }]
            }]
        }
    };

    mongo.page.update(query, update, function(err) {
        return console.error(err);
    });

    //mongo.page.findOne(query, function(err, json) {

    //json.validate();

    //log(err);
    // });

    res.send('testpage');

};

/*
exports.adhoc = function(req, res) {
    
    mongo.data.find({ type: "module" }, function (err, data) {
    if (err) return console.error(err);
    //console.log(data);
    
    for (var a = 0; a < data.length; a++){
    var page = new mongo.module(data[a]);
    page.save(function (err, fluffy) {
    if (err) return console.error(err);
    //fluffy.speak();
    });
    };
});
    res.send("hello");
};*/

/*
exports.update = function(req, res) {
    console.log(req.body);
    var json = JSON.stringify(req.body.input);
    var saver = new mongo.module({
        name: "test",
        scripts: {
            text: json
        }
    });
    saver.save();
    res.send('done');
};*/
