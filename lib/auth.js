// load all the things we need
var argon = require('argon2');
var LocalStrategy = require('passport-local').Strategy;
var config = require('./../config');
var cryptoJS = require("crypto-js");
var _ = require('lodash');

//console.log(options);
//var pepper = new Buffer(config.hash.pepper);
//console.log(pepper);
//console.log(config);
//var FacebookStrategy = require('passport-facebook').Strategy;
//var TwitterStrategy  = require('passport-twitter').Strategy;
//var GoogleStrategy   = require('passport-google-oauth').OAuth2Strategy;

// load up the user model
var User = require('./../models/mongo').user;

var options = {
    timeCost: config.hash.timeCost,
    memoryCost: config.hash.memoryCost,
    parallelism: config.hash.parallelism,
    type: argon[config.hash.type],
    hashLength: config.hash.hashLength
};
var aes = cryptoJS.AES;

//
//console.log('sdsdsd');

// load the auth variables
//var configAuth = require('./auth'); // use this one for testing

module.exports = function(passport) {

    //var LocalStrategy = require('passport-local').Strategy;
    //var user = require('./../models/mongo').user;
    //passport.use(new LocalStrategy(user.authenticate()));
    //passport.serializeUser(user.serializeUser());
    //passport.deserializeUser(user.deserializeUser());


    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    passport.use('local-login', new LocalStrategy({
            
            // by default, local strategy uses username and password, we will override with email
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
        },
        function(req, email, password, done) {
            
            if (email)
                email = email.toLowerCase(); // Use lower-case e-mails to avoid case-sensitive e-mail matching
            // asynchronous
            process.nextTick(function() {
                User.findOne({
                    'local.email': email
                }, function(err, user) {
                    
                    // if there are any errors, return the error
                    if (err)
                        return done(err);
                    // if no user is found, return the message
                    if (!user) {
                        console.log('No User found.');
                        return done();
                    }
                    // all is well, return user
                    else {
                        
                        var hash = user.local.password;
                        try {
                            var hashObjDecrypt = {
                                options: aes.decrypt(user.local.password.options, config.hash.optionKey).toString(cryptoJS.enc.Utf8),
                                salt: aes.decrypt(user.local.password.salt, config.hash.saltKey).toString(cryptoJS.enc.Utf8),
                                hash: aes.decrypt(user.local.password.hash, config.hash.hashKey).toString(cryptoJS.enc.Utf8)
                            };

                            hash = hashObjDecrypt.options + "$" + hashObjDecrypt.salt + "$" + hashObjDecrypt.hash;
                        }
                        catch (err) {
                            console.log(err);
                        }
                        argon.verify(hash, password).then(match => {
                            if (match) {
                                // password match 
                                return done(null, user);
                            }
                            else {
                                // password did not match 
                                console.log('Oops! Wrong password.');
                                return done();
                            }
                        }).catch(err => {
                            console.log(err);
                            return done(err);
                            // internal failure 
                        });
                        //console.log('fffssss');
                    }
                });
            });
        }));

    // =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================
    passport.use('local-signup', new LocalStrategy({
            // by default, local strategy uses username and password, we will override with email
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
        },
        function(req, email, password, done) {

            if (email) {
                email = email.toLowerCase();
                // Use lower-case e-mails to avoid case-sensitive e-mail matching
            }
            // asynchronous
            process.nextTick(function() {
                // if the user is not already logged in:
                if (!req.user) {
                    User.findOne({
                        'local.email': email
                    }, function(err, user) {
                        // if there are any errors, return the error
                        if (err)
                            return done(err);
                        // check to see if theres already a user with that email
                        if (user) {
                            console.log('That email is already taken.');
                            return done();
                        }
                        else {
                            var newUser = new User();
                            newUser.local.email = email;
                            argon.generateSalt(config.hash.saltLength).then(salt => {
                                argon.hash(password, salt, options).then(hash => {
                                    // breakdown and encrypt the hashed output
                                    var argonArray = hash.split(/,|\$/);
                                    var options = "$" + argonArray[1] + "$" + argonArray[2] + "$" + argonArray[3] + "," + argonArray[4] + "," + argonArray[5];
                                    var hashObj = {
                                        options: aes.encrypt(options, config.hash.optionKey).toString(),
                                        salt: aes.encrypt(argonArray[6], config.hash.saltKey).toString(),
                                        hash: aes.encrypt(argonArray[7], config.hash.hashKey).toString()
                                    };
                                    newUser.local.password = hashObj;
                                    newUser.details.firstName = req.body.firstName;
                                    newUser.details.secondName = req.body.secondName;
                                    //console.log(req.body);
                                    var middleName = req.body.middleNames.split(" ");
                                    //console.log(middleName);
                                    var middleNames = [];
                                    _.each(middleName, function(i){
                                        if(i.length>0){
                                            middleNames.push(i);
                                        }
                                    });
                                    //log
                                    newUser.details.middleNames = middleNames;
                                    newUser.access = 1; // make sure that higher access cannot be inserted to the input data.
                                    newUser.save(function(err) {
                                        if (err) {
                                            console.log(err);
                                            return done(err);
                                        } else {
                                            console.log(newUser.details.middleNames);
                                        return done(null, newUser);
                                        }
                                    });
                                }).catch(err => {
                                    console.log(err);
                                });
                            });
                        }
                    });
                    // if the user is logged in but has no local account...
                }
                else if (!req.user.local.email) {
                    // ...presumably they're trying to connect a local account
                    // BUT let's check if the email used to connect a local account is being used by another user
                    User.findOne({
                        'local.email': email
                    }, function(err, user) {
                        if (err)
                            return done(err);

                        if (user) {
                            return done(null, false, req.flash('loginMessage', 'That email is already taken.'));
                            // Using 'loginMessage instead of signupMessage because it's used by /connect/local'
                        }
                        else {
                            user = req.user;
                            user.local.email = email;
                            user.local.password = user.generateHash(password);
                            user.save(function(err) {
                                if (err)
                                    return done(err);

                                return done(null, user);
                            });
                        }
                    });
                }
                else {
                    // user is logged in and already has a local account. Ignore signup. (You should log out before trying to create a new account, user!)
                    return done(null, req.user);
                }

            });

        }));
    /*
        // =========================================================================
        // FACEBOOK ================================================================
        // =========================================================================
        var fbStrategy = configAuth.facebookAuth;
        fbStrategy.passReqToCallback = true;  // allows us to pass in the req from our route (lets us check if a user is logged in or not)
        passport.use(new FacebookStrategy(fbStrategy,
        function(req, token, refreshToken, profile, done) {

            // asynchronous
            process.nextTick(function() {

                // check if the user is already logged in
                if (!req.user) {

                    User.findOne({ 'facebook.id' : profile.id }, function(err, user) {
                        if (err)
                            return done(err);

                        if (user) {

                            // if there is a user id already but no token (user was linked at one point and then removed)
                            if (!user.facebook.token) {
                                user.facebook.token = token;
                                user.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName;
                                user.facebook.email = (profile.emails[0].value || '').toLowerCase();

                                user.save(function(err) {
                                    if (err)
                                        return done(err);
                                        
                                    return done(null, user);
                                });
                            }

                            return done(null, user); // user found, return that user
                        } else {
                            // if there is no user, create them
                            var newUser            = new User();

                            newUser.facebook.id    = profile.id;
                            newUser.facebook.token = token;
                            newUser.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName;
                            newUser.facebook.email = (profile.emails[0].value || '').toLowerCase();

                            newUser.save(function(err) {
                                if (err)
                                    return done(err);
                                    
                                return done(null, newUser);
                            });
                        }
                    });

                } else {
                    // user already exists and is logged in, we have to link accounts
                    var user            = req.user; // pull the user out of the session

                    user.facebook.id    = profile.id;
                    user.facebook.token = token;
                    user.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName;
                    user.facebook.email = (profile.emails[0].value || '').toLowerCase();

                    user.save(function(err) {
                        if (err)
                            return done(err);
                            
                        return done(null, user);
                    });

                }
            });

        }));

        // =========================================================================
        // TWITTER =================================================================
        // =========================================================================
        passport.use(new TwitterStrategy({

            consumerKey     : configAuth.twitterAuth.consumerKey,
            consumerSecret  : configAuth.twitterAuth.consumerSecret,
            callbackURL     : configAuth.twitterAuth.callbackURL,
            passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)

        },
        function(req, token, tokenSecret, profile, done) {

            // asynchronous
            process.nextTick(function() {

                // check if the user is already logged in
                if (!req.user) {

                    User.findOne({ 'twitter.id' : profile.id }, function(err, user) {
                        if (err)
                            return done(err);

                        if (user) {
                            // if there is a user id already but no token (user was linked at one point and then removed)
                            if (!user.twitter.token) {
                                user.twitter.token       = token;
                                user.twitter.username    = profile.username;
                                user.twitter.displayName = profile.displayName;

                                user.save(function(err) {
                                    if (err)
                                        return done(err);
                                        
                                    return done(null, user);
                                });
                            }

                            return done(null, user); // user found, return that user
                        } else {
                            // if there is no user, create them
                            var newUser                 = new User();

                            newUser.twitter.id          = profile.id;
                            newUser.twitter.token       = token;
                            newUser.twitter.username    = profile.username;
                            newUser.twitter.displayName = profile.displayName;

                            newUser.save(function(err) {
                                if (err)
                                    return done(err);
                                    
                                return done(null, newUser);
                            });
                        }
                    });

                } else {
                    // user already exists and is logged in, we have to link accounts
                    var user                 = req.user; // pull the user out of the session

                    user.twitter.id          = profile.id;
                    user.twitter.token       = token;
                    user.twitter.username    = profile.username;
                    user.twitter.displayName = profile.displayName;

                    user.save(function(err) {
                        if (err)
                            return done(err);
                            
                        return done(null, user);
                    });
                }

            });

        }));

        // =========================================================================
        // GOOGLE ==================================================================
        // =========================================================================
        passport.use(new GoogleStrategy({

            clientID        : configAuth.googleAuth.clientID,
            clientSecret    : configAuth.googleAuth.clientSecret,
            callbackURL     : configAuth.googleAuth.callbackURL,
            passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)

        },
        function(req, token, refreshToken, profile, done) {

            // asynchronous
            process.nextTick(function() {

                // check if the user is already logged in
                if (!req.user) {

                    User.findOne({ 'google.id' : profile.id }, function(err, user) {
                        if (err)
                            return done(err);

                        if (user) {

                            // if there is a user id already but no token (user was linked at one point and then removed)
                            if (!user.google.token) {
                                user.google.token = token;
                                user.google.name  = profile.displayName;
                                user.google.email = (profile.emails[0].value || '').toLowerCase(); // pull the first email

                                user.save(function(err) {
                                    if (err)
                                        return done(err);
                                        
                                    return done(null, user);
                                });
                            }

                            return done(null, user);
                        } else {
                            var newUser          = new User();

                            newUser.google.id    = profile.id;
                            newUser.google.token = token;
                            newUser.google.name  = profile.displayName;
                            newUser.google.email = (profile.emails[0].value || '').toLowerCase(); // pull the first email

                            newUser.save(function(err) {
                                if (err)
                                    return done(err);
                                    
                                return done(null, newUser);
                            });
                        }
                    });

                } else {
                    // user already exists and is logged in, we have to link accounts
                    var user               = req.user; // pull the user out of the session

                    user.google.id    = profile.id;
                    user.google.token = token;
                    user.google.name  = profile.displayName;
                    user.google.email = (profile.emails[0].value || '').toLowerCase(); // pull the first email

                    user.save(function(err) {
                        if (err)
                            return done(err);
                            
                        return done(null, user);
                    });

                }

            });

        }));
    */
};
