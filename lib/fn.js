var mongo = require('../models/mongo');
var config = require("../config");
var pug = require("pug");
var clone = require("clone");
var reqs = {
    "module": {},
    "page": {},
    "template": {}
};
var _ = require("lodash");
var mongoose = require('mongoose');
var fs = require("fs");
var bundle = require('./scripts/bundle');
//bundle("test");

/* Utilities */

var util = {
    // sort array by property
    dynamicSort: function(property) {
        var sortOrder = 1;
        if (property[0] === "-") {
            sortOrder = -1;
            property = property.substr(1);
        }
        return function(a, b) {
            var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
            return result * sortOrder;
        };
    },
    // sort array by more than one proprty    
    SortMultiple: function() {
        var props = arguments;
        return function(obj1, obj2) {
            var i = 0,
                result = 0,
                numberOfProperties = props.length;
            while (result === 0 && i < numberOfProperties) {
                result = util.dynamicSort(props[i])(obj1, obj2);
                i++;
            }
            return result;
        };
    },
    //check array for variable
    isInArray: function(value, array) {
        return array.indexOf(value) > -1;
    },
    //check object for match and return object
    objectMatch: function(needle, key, haystack) {
        if (!haystack) {
            return "false";
        }
        for (var i = 0; i < haystack.length; i++) {
            if (String(haystack[i][key]) === String(needle)) {
                return haystack[i];
            }
        }
        return "false";
    },
    //sort object by key
    sortByKey: function(array, key) {
        return array.sort(function(a, b) {
            var x = a[key];
            var y = b[key];
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        });
    },
    reqStr: function(filename, coll, callback) {

        if (!reqs[coll][filename]) {
            try {
                reqs[coll][filename] = require("./reqs/" + coll + "/" + filename);
                if (callback) {
                    callback();
                }

            }
            catch (err) {
                log(err);
            }
        }
    },
    duplicates: function(name) {
        var output = "";
        var ext = name.split(".");
        var patt = new RegExp(/\(([^)]+)\)/);
        var matches = patt.exec(name);
        if (matches) {
            var string = name.split("(", matches.index);
            string = string[0];
            var duplicates = parseInt(matches[1], 10);
            duplicates++;
            output = string + "(" + duplicates + ")";
        }
        else {
            output = name + "(1)";
        }
        if(ext.length>0){
            ext = ext.pop();
            //log(ext);
            output = output + "." + ext;
        }
        return output;
    },
    folderDelete: function(path) {
        fs.readdir(path, function(err, files) {
            if (err) {
                log(err);
            }
            else {
                if (!files.length) {
                    fs.rmdir(path, function(err) {
                        if (err) {
                            log(err);
                        }
                    });
                }
            }
        });
    },
    localScripts: function(obj) {
        var inputProcessed = false;
        var text;
        var stream;
        var path = null;
        var folder = null;
        var fileWrite = function() {
            if (path) {
                try {
                    if (text && text.length > 0) {
                        fs.stat(path, function(err, stats) {
                            if (err) {
                                if (err.errno !== -2) {
                                    log(err);
                                }
                                else {
                                    write(function() {
                                        if (obj.path === 'page') {
                                            bundle(obj.name);
                                        }
                                    });
                                }
                            }
                            else {
                                write();
                            }
                        });
                        var write = function(cb) {
                            stream = fs.createWriteStream(path);
                            stream.once('open', function(fd) {
                                stream.write(text);
                                stream.end();
                                inputProcessed = true;
                                obj.callback(inputProcessed);
                                if (_.isFunction(cb)) {
                                    cb();
                                }
                            });
                        };
                    }
                    else {
                        fs.stat(path, function(err, stat) {
                            if (err) {
                                if (err.errno !== -2) {
                                    log(err);
                                }
                                else {
                                    inputProcessed = true;
                                }
                                obj.callback(inputProcessed);
                            }
                            else {
                                //log(path);
                                fs.unlink(path, function(err) {
                                    if (err) {
                                        log(err);
                                        obj.data = {
                                            save: false
                                        };
                                        obj.callback(inputProcessed, obj.data);
                                    }
                                    else {

                                        if (obj.path === "page" && obj.type === "js") {
                                            var loopDelete = function(name) {
                                                fs.unlink("./lib/scripts/js/output/entry/" + name, function(err) {
                                                    if (err) {
                                                        log(err);
                                                    }
                                                    else {
                                                        inputProcessed = true;
                                                        obj.callback(inputProcessed);
                                                        log("DELETED " + name);
                                                        if (folder) {
                                                            fs.readdir(folder, function(err, files) {
                                                                if (err) {
                                                                    log(err);
                                                                }
                                                                else {
                                                                    if (!files.length) {
                                                                        fs.rmdir(folder, function(err) {
                                                                            if (err) {
                                                                                log(err);
                                                                            }
                                                                        });
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    }
                                                });
                                            };
                                            fs.readdir("./lib/scripts/js/output/entry/", function(err, files) {
                                                if (err) {
                                                    log(err);
                                                }
                                                else {
                                                    files.forEach(file => {
                                                        var fileSplit = file.split(".");
                                                        if (fileSplit[0] === obj.name) {
                                                            loopDelete(file);
                                                        }
                                                    });
                                                    inputProcessed = true;
                                                    obj.callback(inputProcessed);
                                                }
                                            });
                                        }
                                        else {
                                            inputProcessed = true;
                                            obj.callback(inputProcessed);
                                        }
                                    }
                                });
                                switch (obj.type) {
                                    case "browserScripts":
                                        util.folderDelete("./lib/scripts/js/subjs/" + obj.path + "/" + obj.name);
                                        break;
                                    case "require":
                                        util.folderDelete("./lib/reqs/" + obj.path + "/" + obj.name);
                                        break;
                                }
                            }
                        });
                    }
                }
                catch (err) {
                    log(err);
                    obj.data = {
                        save: false
                    };
                    inputProcessed = false;
                    obj.callback(inputProcessed, obj.data);
                }
            }
            else {
                obj.data = {
                    save: false
                };
                inputProcessed = false;
                obj.callback(inputProcessed, obj.data);
            }
        };
        switch (obj.type) {
            case 'reqs':
                path = "./lib/reqs/" + obj.path + "/" + obj.name + ".js";
                text = obj.data.data.resources.scripts.requires.text;
                fileWrite();
                break;
            case "require":
                path = "./lib/reqs/" + obj.path + "/" + obj.name + "/" + obj.subName + ".js";
                folder = "./lib/reqs/" + obj.path + "/" + obj.name;
                if (obj.delete) {
                    text = "";
                }
                else {
                    text = _.find(obj.data.data.resources.scripts.require, {
                        name: obj.subName
                    }).text;
                }
                fs.stat(folder, function(err, stats) {
                    if (err) {
                        if (err.errno === -2) {
                            fs.mkdirSync(folder);
                            fileWrite();
                        }
                        else {
                            fileWrite();
                            log(err);
                        }
                    }
                    else {
                        fileWrite();
                    }
                });
                break;
            case "js":
                path = "./lib/scripts/js/" + obj.path + "/" + obj.name + ".js";
                text = obj.data.data.resources.scripts.text;
                fileWrite();
                break;
            case "browserScripts":
                path = "./lib/scripts/js/subjs/" + obj.path + "/" + obj.name + "/" + obj.subName + ".js";
                folder = "./lib/scripts/js/subjs/" + obj.path + "/" + obj.name;
                if (obj.delete) {
                    text = "";
                }
                else {
                    text = _.find(obj.data.data.resources.scripts.browser, {
                        name: obj.subName
                    }).text;
                }
                fs.stat(folder, function(err, stats) {
                    if (err) {
                        if (err.errno === -2) {
                            fs.mkdirSync(folder);
                            fileWrite();
                        }
                        else {
                            fileWrite();
                            log(err);
                        }
                    }
                    else {
                        fileWrite();
                    }
                });
                break;
            case "css":
                path = "./lib/scripts/css/" + obj.path + "/" + obj.name + ".css";
                text = "";
                var mq = util.sortByKey(globals.media, "columns");
                var cssSrc = obj.data.data.resources.css;
                _.each(mq, function(i, n) {
                    var tempText = _.find(cssSrc, ['query', String(i._id)]);
                    if (tempText && tempText.text) {
                        if (i.query === "null") {
                            text += tempText.text;
                        }
                        else {
                            text += "\n" + i.query + "{\n\n" + tempText.text + "\n\n}\n";
                        }
                    }
                });
                fileWrite();
                break;
        }
    }
};

// extend underscore

_.mixin({
    defined: function(obj, arr) {
        var testobj = _.clone(obj);
        arr = arr.split(".");
        var testfind = _.find(arr, function(a) {
            if (testobj[a]) {
                testobj = testobj[a];
                return false;
            }
            else {
                return true;
            }
        });
        return _.isUndefined(testfind);
    }
});

exports.util = util;

/* captcha */

/*var captcha = {
    randomStr: function() {
        var str_ary = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
        var str_num = 6;
        var r_num = str_ary.length;
        var text = '';
        for (var i = 0; i < str_num; i++) {
            var pos = Math.floor(Math.random() * r_num);
            text += str_ary[pos];
        }
        return text;
    }
};

exports.captcha = captcha;*/

// function for console logging, allows config to enable or disable console.log()

var log = function(a, b) {
    if (!b) {
        if (config.chatty) {
            console.log(a);
        }
    }
    else {
        switch (a) {
            case "timeSt":
                console.time(b);
                break;
            case "timeEd":
                console.timeEnd(b);
        }
    }
};

exports.log = log;

// set up globals

var globals = {
    temp: [],
    mod: [],
    page: [],
    media: []
};
exports.globals = globals;

// set up cache controling functions

var cache = {
    counter: 0,
    end: function(callback) {
        cache.counter++;
        if (cache.counter === 5) {
            callback();
        }
    },
    startup: function(callback) {
        cache.requires(callback);
        cache.update(callback);
        cache.mediaQuery(callback);
    },
    mediaQuery: function(callback) {
        mongo.media.find(null, 'query columns name', {
            sort: 'columns'
        }).lean().exec(function(err, mediaquery) {
            if (err) {
                log("error in media queries: " + err.message);
            }
            //log(mediaquery);
            _.each(mediaquery, function(i) {
                globals.media.push({
                    "query": i.query,
                    "col": i.columns,
                    "name": i.name,
                    "_id": i._id
                });
            });
            log("Media Queries Cached");
            cache.end(callback);
        });
    },
    update: function(callback) {
        // simple function for querying the DB
        var cycle = function(type, count) {
            cache.query(type, count, function() {
                log(type + "s loaded, maximum of " + count);
                cache.end(callback);
            });
        };
        // check each config.cache and load if needed.
        if (config.cache.modules) {
            cycle('module', config.cache.modcount);
        }
        else {
            log('Modules not cached');
            cache.end(callback);
        }
        if (config.cache.pages) {
            cycle('page', config.cache.pagecount);
        }
        else {
            log('Pages not cached');
            cache.end(callback);
        }
        if (config.cache.templates) {
            cycle('template', config.cache.tempcount);
        }
        else {
            log('Templates not cached');
            cache.end(callback);
        }
    },
    // basic query for use for all cache DB querys
    query: function(type, count, callback) {
        mongo[type].find({
                'type': type
            })
            .lean()
            .limit(count)
            .exec(function(err, data) {
                if (err) {
                    log("error in cache for " + type + ": " + err.message);
                }
                switch (type) {
                    case 'template':
                        globals.temp = data;
                        break;
                    case 'module':
                        globals.mod = data;
                        break;
                    case 'page':
                        globals.page = data;
                        break;
                }
                callback();
            });
    },

    requires: function(callback, data) {
        var load = function(coll, cb) {
            mongo[coll].find({
                'active': true,
                'data.resources.scripts.requires': {
                    $exists: true
                }
            }, "_id data.resources.scripts.requires name").lean().exec(function(err, json) {
                if (err) {
                    log("error in requires: " + err.message);
                }
                _.each(json, function(i) {
                    var obj = JSON.parse(JSON.stringify(i));
                    if (obj.data.resources.scripts.requires.text.length > 0) {
                        util.reqStr(obj.name, coll, function() {
                            try {
                                if (reqs[coll][obj.name].run) {
                                    reqs[coll][obj.name].run(function(message) {
                                        log("Required Module Loaded: " + obj.name + " Returned Message: " + message);
                                    });
                                }
                                else {
                                    log("Required Module Loaded: " + obj.name);
                                }
                            }
                            catch (err) {
                                log(err);
                            }
                        });
                    }
                });
                cb();
            });
        };
        load('module', function() {
            load('template', function() {
                load('page', function() {
                    cache.end(callback);
                });
            });
        });
    }
};
exports.cache = cache;

/*var script = {
    render: function(path, callback) {
        log("timeSt", "SCRIPT \"" + path + "\" Load");
        var clonedGlobal = clone(globals);
        var globalchk = util.objectMatch(path, "name", clonedGlobal.mod);
        var end = function(json, cb) {
            if (!json) {
                cb();
            }
            else {
                if (!_.defined(json, "data.resources.scripts.min")) {
                    json = json.data.resources.scripts.text;
                }
                else {
                    json = json.data.resources.scripts.min;
                }
                log("timeEd", "SCRIPT \"" + path + "\" Load");

                // if removed all stringified entries can remove this failsafe

                if (json[0] === "\"") {
                    cb(JSON.parse(json));
                }
                else {
                    // end of block                    
                    cb(json);
                    // and this     
                }
                //
            }
        };
        if (globalchk === "false") {

            mongo.module.findOne({
                'name': path,
            }, "data.resources.scripts", function(err, json) {
                if (err) {
                    log("error in templates: " + err.message);
                }
                json = JSON.parse(JSON.stringify(json));
                end(json, callback);
            });
        }
        else {
            var json = globalchk;
            end(json, callback);
        }
    },
    reqrender: function(json, callback) {
        if (!json) {
            callback(false);
            log("ERROR in requirejs file, error message sent");
        }
        else {
            var output = json.mod;
            var configPaths = [];
            var onloads = [];
            var text = "requirejs.config({\n\twaitSeconds: 60,\n\tbaseUrl: \"/scripts/js\",\n\tpaths: {";
            _.each(output, function(a) {
                var name = a.name;
                var script = a.data.resources.scripts;
                if (script.onload) {
                    var path = name;
                    if (script.static) {
                        path = script.static;
                    }
                    onloads.push([script.onload, name, path]);
                }

                if (script.cdn) {
                    configPaths.push("\n\t\t\"" + name + "\": [\"" + script.cdn + "\", \"" + path + "\"]");
                }
                else if (script.static) {
                    configPaths.push("\n\t\t\"" + name + "\": \"" + path + "\"");
                }

            });
            _.each(configPaths, function(i, n) {
                if (n > 0) {
                    text += ",";
                }
                text += i;
            });
            text += "\n\t\t}\n\t});\n\n";
            _.each(onloads, function(i) {
                text += "requirejs([\"" + i[1] + "\"], function(" + i[1] + "){" + i[0] + "})\n\n";
            });

            //text += "function render(id) {recaptchaClientId = grecaptcha.render(id, {    'sitekey': '"+config.recaptchaSiteKey+"',   'size': 'normal', 'theme': 'default'  });}window.renderRecaptcha = render;var onloadCallback = function() {if (!document.getElementById('g-recaptcha')) {return;}window.renderRecaptcha('g-recaptcha');};";

            log("REQUIRE Config page sent");
            callback(text);
        }
    }
};

exports.script = script;

var css = {
    render: function(json, callback) {
        var cssarr = {};

        var mq = util.sortByKey(globals.media, "columns");
        _.each(mq, function(i) {
            //log(i);
            cssarr[i._id] = new Array;
        });

        _.each(json.mod, function(i) {
            if (!i.position) {
                i.position = json.mod.length + 1;
            }
        });

        json.mod = util.sortByKey(json.mod, "position");

        _.each(json.mod, function(i) {
            //log(i.position);
            if (_.defined(i, "data.resources.css")) {
                _.each(i.data.resources.css, function(j) {
                    cssarr[j.query].push(j.min);
                });
            }
        });
        if (_.defined(json, "temp.data.resources.css")) {
            _.each(json.temp.data.resources.css, function(j) {
                cssarr[j.query].push(j.min);
            });
        }
        var output = "";

        _.each(_.keys(cssarr), function(i) {
            var query = util.objectMatch(i, "_id", mq);
            if (cssarr[i].length > 0) {
                if (query.query !== "null") {
                    output += query.query + " {\n";
                }
                _.each(cssarr[i], function(j) {
                    output += j + "\n";
                });

                if (query.query !== "null") {
                    output += "\n}\n";
                }
            }
        });
        callback(output);
    }
};

exports.css = css;
*/
var query = {

    /*
        usage:
  
        query.start(
                obj: 
                    {
                    title: <console.log title for all load times>,
                    start: <page, template etc... i.e. going in at template level and not page>,
                    patharr :<array of url, path[0] being the paage>, 
                    fields: { <fields to return for page, templates and modules>
                    httpstatus: <404, 500 etc... leave blank unless needed>
                        page: 
                        temp:
                        mod:
                    }},
                callback(data)
                )
    */

    // functions

    // final function
    endfunc: function(obj, callback) {
        callback(obj.output, obj.paths);
    },
    // check modules
    chkMod: function(obj, clonedGlobal, querycallback) {
        log("timeSt", obj.title + " modules Load");
        var globalchk = "";
        var mods = [];
        if (_.defined(obj, "output.temp.modules")) {
            _.each(obj.output.temp.modules, function(i) {
                if (!util.isInArray(String(i), mods)) {
                    mods.push(String(i));
                }
            });
        }
        if (_.defined(obj, "output.page.modules")) {

            _.each(obj.output.page.modules, function(i) {
                if (!util.isInArray(String(i), mods)) {
                    mods.push(String(i));
                }
            });
        }
        //log(mods);
        // mods contains the id's of all the used modules
        // now check the cache, removing from mods any id's that are cached
        _.each(mods, function(i, a) {
            globalchk = util.objectMatch(i, "_id", clonedGlobal.mod);
            if (globalchk !== "false") {
                if (obj.start === "script") {
                    if (_.defined(globalchk, "data.resources.scripts")) {
                        if (globalchk.data.resources.scripts.min ||
                            globalchk.data.resources.scripts.text) {
                            obj.output.mod.push(globalchk);
                            delete mods[a];
                        }
                    }
                }
                else {
                    obj.output.mod.push(globalchk);
                    delete mods[a];
                }
            }
        });
        if (obj.output.mod.length !== 0) {
            query.objselect("mod", obj.output.mod, obj.fields.mod, function(outobj) {
                obj.output.mod = outobj;
            });
        }
        _.each(mods, function(i, a) {
            if (i === undefined) {
                mods.splice(a, 1);
            }
        });
        mods = _.compact(mods);
        // check mongo for the rest
        var queryStr = {
            '_id': {
                $in: mods
            },
            'active': true
        };
        /*if (obj.start === "script") {
            queryStr = {
                '_id': {
                    $in: mods
                },
                $or: [{
                    'data.resources.scripts.min': {
                        $exists: true
                    }
                }, {
                    'data.resources.scripts.text': {
                        $exists: true
                    }
                }],
                'active': true
            };
        }*/
        if (mods.length > 0) {
            mongo.module.find(
                queryStr, obj.fields.mod, {
                    sort: 'position'
                }).lean().exec(
                function(err, json) {
                    if (err) {
                        log("error in modules: " + err.message);
                    }
                    obj.output.mod = obj.output.mod.concat(json);
                    log("timeEd", obj.title + " modules Load");
                    querycallback(obj);
                });
        }
        else {
            log("timeEd", obj.title + " modules Load");
            querycallback(obj);
        }
    },
    // check templates
    chkTemp: function(obj, clonedGlobal, querycallback) {
        log("timeSt", obj.title + " template Load");
        var globalchk = "";
        var tempId = obj.output.page.template;
        if (obj.start === "template" //|| obj.start === "css"
        ) {
            /*if (_.defined(obj, "paths.css")) {
                tempId = obj.paths.css;
            }
            else {*/
            tempId = obj.paths.template;
            //}
        }
        globalchk = util.objectMatch(tempId, "_id", clonedGlobal.temp);
        if (globalchk === "false") {
            mongo.template.findOne({
                '_id': tempId,
                'active': true
            }, obj.fields.temp).lean().exec(function(err, json) {
                if (err) {
                    log("error in templates: " + err.message);
                }
                obj.output.temp = json;
                log("timeEd", obj.title + " template Load");
                querycallback(obj);
            });
        }
        else {
            query.objselect("temp", globalchk, obj.fields.temp, function(outobj) {
                obj.output.temp = outobj;
                log("timeEd", obj.title + " template Load");
                querycallback(obj);
            });
        }
    },
    // check to see if page is in the globals and then send to check templates
    pageChk: function(obj, clonedGlobal, querycallback) {
        if (obj.paths.path === "error" && !obj.paths.status) {
            obj.paths.status = "404";
        }
        log("timeSt", obj.title + " page Load");
        var globalchk = "";
        globalchk = util.objectMatch(obj.paths.path, "name", clonedGlobal.page);
        if (globalchk === "false") {
            var endfunc = function(json) {
                obj.output.page = json;
                log("timeEd", obj.title + " page Load");
                querycallback(obj);
            };
            var querypage = function(name, cb) {
                mongo.page.findOne({
                    'name': name,
                    'active': true
                }, obj.fields.page, function(err, json) {
                    if (err) {
                        log("error in pages: " + err.message);
                    }
                    cb(json);
                });
            };
            querypage(obj.paths.path, function(json) {
                if (json) {
                    endfunc(json);
                }
                else {
                    querypage("error", function(json) {
                        obj.paths = {
                            path: "error",
                            status: "404"
                        };
                        endfunc(json);
                    });
                }
            });
        }
        else {
            query.objselect("page", globalchk, obj.fields.page, function(outobj) {
                obj.output.page = outobj;
                log("timeEd", obj.title + " page Load");
                querycallback(obj);
            });
        }
    },
    // query for sorting the correct fields from the cached objects if needed
    objselect: function(type, json, fields, callback) {
        if (!json.length) {
            json = [json];
        }
        var outarr = [];
        var fieldarr = fields.split(" ");
        fieldarr.push("_id");
        _.each(fieldarr, function(d, e) {
            fieldarr[e] = d.split(".");
        });
        _.each(json, function(i) {
            var outobj = {};
            _.each(fieldarr, function(a) {
                var temp = i;
                _.each(a, function(b) {
                    if (temp[b]) {
                        temp = temp[b];
                    }
                });
                // cannot sub for _.each() as c = a.length
                for (var c = a.length; c > 0; c--) {
                    var tempobj = temp;
                    var objname = a[c - 1];
                    temp = {};
                    if (c == 1) {
                        if (!outobj[objname]) {
                            outobj[objname] = tempobj;
                        }
                        else {
                            for (var key in tempobj) {
                                if (tempobj.hasOwnProperty(key)) {
                                    outobj[objname][key] = tempobj[key];
                                }
                            }
                        }
                    }
                    else {
                        temp[objname] = tempobj;
                    }
                }
            });
            outarr.push(outobj);
        });
        if (outarr.length == 1) {
            outarr = outarr[0];
        }
        callback(outarr);
    },
    // function for page queries, i.e. the main function.
    data: function(obj, callback) {
        var pagefields = String(obj.fields.page);
        pagefields += " access";
        var tempfields = String(obj.fields.temp);
        tempfields += " access";
        var modsfields = String(obj.fields.mod);
        modsfields += " access";
        var fields = {
            page: pagefields,
            temp: tempfields,
            mod: modsfields
        };
        obj.fields = fields;
        obj.output = {
            page: {},
            temp: {},
            mod: []
        };
        var serverError = function(obj, text, callback) {
            log("ERROR:" + text);
            obj = {
                paths: {
                    path: "error",
                    status: "404"
                },
                output: obj
            };
            query.endfunc(obj, callback);
        };
        log("timeSt", obj.title + " globals cloned");
        var clonedGlobal = clone(globals);
        log("timeEd", obj.title + " globals cloned");

        //switch (obj.start) {
        //  case "page":

        var queryCycle = function() {

            var access = {
                pass: [],
                fail: []
            };
            var secure = {
                pass: [],
                fail: []
            };
            //log(obj);
            query.pageChk(obj, clonedGlobal, function(obj) {
                if (obj.output.page && obj.output.page.access) {
                    var accesstest = (obj.output.page.access <= obj.access);
                    if (accesstest) {
                        access.pass.push("page")
                    }
                    else {
                        access.fail.push("page")
                    }
                }
                else {
                    access.pass.push("Page");
                }

                if (obj.output.page) {
                    query.chkTemp(obj, clonedGlobal, function(obj) {
                        if (obj.output.temp && obj.output.temp.access) {
                            var accesstest = (obj.output.temp.access <= obj.access);
                            if (accesstest) {
                                access.pass.push("temp")
                            }
                            else {
                                access.fail.push("temp")
                            }
                        }
                        else {
                            access.pass.push("temp");
                        }
                        if (obj.output.temp) {
                            query.chkMod(obj, clonedGlobal, function(obj) {
                                if (obj.output.mod && obj.output.mod.length > 0) {
                                    _.each(obj.output.mod, function(i, n) {
                                        var moduleNo = "module_" + n;
                                        if (i.access) {
                                            var accesstest = (i.access <= obj.access);
                                            if (accesstest) {
                                                access.pass.push(moduleNo)
                                            }
                                            else {
                                                access.fail.push(moduleNo)
                                            }

                                        }
                                        else {
                                            access.pass.push(moduleNo);
                                        }
                                    });
                                }
                                if (obj.output.mod) {
                                    //log(access);
                                    if (access.fail.length > 0) {


                                        obj.paths = {
                                            path: "error",
                                            status: "401"
                                        };

                                        obj.output = {
                                            "page": {},
                                            "temp": {},
                                            "mod": []
                                        };

                                        queryCycle();
                                    }
                                    else {
                                        //log(obj);
                                        query.endfunc(obj, callback);
                                    }
                                }
                                else {
                                    serverError(false, "Module query returned no values", callback);
                                }
                            });
                        }
                        else {
                            serverError(false, "Template query returned no values", callback);
                        }
                    });
                }
                else {
                    serverError(false, "Page query returned no values", callback);
                }
            });
            // break;
            /*case "template":
            case "css":
                query.chkTemp(obj, clonedGlobal, function(obj) {
                    if (obj.output.temp) {
                        query.chkMod(obj, clonedGlobal, function(obj) {
                            //log(obj);
                            query.endfunc(obj, callback);
                        });
                    }
                    else {
                        log("ERROR, DB read error, reverting to error page");
                        obj = {
                            output: "Error loading CSS link",
                            paths: {
                                path: "error",
                                status: "404"
                            },
                        };
                        //log(obj);
                        query.endfunc(obj, callback);
                    }
                });
                break;
            case "script":
                //log(obj);
                query.pageChk(obj, clonedGlobal, function(obj) {
                    if (obj.output.page) {
                        query.chkTemp(obj, clonedGlobal, function(obj) {
                            if (obj.output.temp) {
                                query.chkMod(obj, clonedGlobal, function(obj) {
                                    query.endfunc(obj, callback);
                                });
                            }
                        });
                    }
                    else {
                        log("ERROR, DB read error, reverting to error page");
                        obj = {
                            output: "Error loading Script link",
                            paths: {
                                path: "error",
                                status: "404"
                            },
                        };
                        query.endfunc(obj, callback);
                    }
                });
                break;*/
            //}
        };
        queryCycle();
    }
};

exports.query = query;

var render = {
    output: {
        head: {},
        body: {},
        vars: {}
    },
    // go through templates finding the pages and mods to insert.
    recursiveBodyInsert: function(json, input, designation) {
        // set up vars
        var forced = false;
        if (designation === "html") {
            forced = true;
        }
        //loop through objects
        _.each(json, function(i, n, list) {
            var watchForChange = false;
            var jsonToUpdate = {};
            var tempdata = {};
            var pagehaystack;
            // check for temp
            if (i.templateid) {
                if (!i.tag && forced) {
                    i.tag = "div";
                }
                pagehaystack = input.page.data[designation];
                if (designation === "head") {
                    pagehaystack = input.page.data[designation].html;
                }
                tempdata = util.objectMatch(i.templateid, "templateid", pagehaystack);
                watchForChange = true;
                jsonToUpdate = tempdata.data;
                //log(jsonToUpdate);
            }
            // check for mod
            else if (i.moduleid) {
                if (!i.tag && forced) {
                    i.tag = "div";
                }
                tempdata = util.objectMatch(i.moduleid, "_id", input.mod);
                if (designation === "html") {
                    if (!tempdata.data) {
                        pagehaystack = [{
                            "tag": "div",
                            "mime": "html",
                            "text": "Module not found ID: " + i.moduleid
                        }];
                    }
                    else {
                        try {
                            pagehaystack = tempdata.data[designation];
                            //log(pagehaystack[0].data);
                        }
                        catch (err) {
                            log(err);
                        }
                    }
                }
                else
                if (designation === "head") {
                    //log(tempdata.data[designation].html);
                    try {
                        pagehaystack = tempdata.data[designation].html[0].data;
                    }
                    catch (err) {
                        log(err);
                    }
                }
                watchForChange = true;
                jsonToUpdate = pagehaystack;
            }
            //update output JSON
            if (watchForChange) {
                i.data = jsonToUpdate;
                //log(i);
            }
            //decide to recursive
            if (_.has(i, "data") && i.data !== "false") {
                if (i.data) {
                    render.recursiveBodyInsert(i.data, input, designation);
                }
            }
        });
    },
    api: {
        json: function(paths, input, loginMessage, next) {
            //log(input);
            //merge the meta together
            if (_.defined(input, "page.data.head.constants.meta.name")) {
                for (var attrname in input.page.data.head.constants.meta.name) {
                    if (input.page.data.head.constants.meta.name[attrname]) {
                        input.temp.data.head.constants.meta.name[attrname] = input.page.data.head.constants.meta.name[attrname];
                    }
                }
            }
            if (_.defined(input, "page.data.head.constants.meta.charset")) {
                input.temp.data.head.constants.meta.charset = input.page.data.head.constants.meta.charset;
            }
            if (_.defined(input, "page.data.head.constants.title")) {
                input.temp.data.head.constants.title = input.page.data.head.constants.title;
            }
            // create objects
            //log(config.baseurl);
            paths.hash = input.page.hash;
            var output = {};
            output.head = input.temp.data.head;
            output.body = input.temp.data.html;
            output.vars = {
                baseurl: "http://" + config.baseurl,
                message: loginMessage,
                paths: paths,
                insert: [],
                head: {
                    constants: output.head.constants,
                    css: input.page.data.resources.css,
                    js: input.page.data.resources.js
                },
                template: {
                    id: input.temp._id,
                    modules: input.temp.modules
                }
            };
            //log();
            _.each(output.body, function(a) {
                render.recursiveBodyInsert(a.data, input, "html");
            });
            _.each(output.head.html, function(a) {
                render.recursiveBodyInsert(a.data, input, "head");
            });
            next(output);
        },
        html: function(input, next) {

            //log(input.body);
            var output = "";
            var pugArray = [];
            var text = "";
            var insertData = [];
            var recursiveJsonTraverse = function(segment, indent) {
                if (!indent || indent < 0) {
                    indent = 0;
                }
                _.each(segment, function(i) {
                    var pugObjectHoldingInsertObject = {};
                    var insertDataLength;
                    var textToInsertInJadeArray = function(text, indent) {
                        insertData.push(text);
                        insertDataLength = insertData.length - 1;
                        pugObjectHoldingInsertObject = {
                            data: {
                                tag: "|",
                                text: "\" !{insert[" + insertDataLength + "]}\""
                            }
                        };
                        pugArray.push(render.jsonToJade(pugObjectHoldingInsertObject.data, indent));
                    };
                    switch (i.mime) {
                        default:
                            case "raw":
                            if (i.tag) {
                                pugArray.push(render.jsonToJade(i, indent));
                                if (i.data && i.data.length >= 1) {
                                    indent++;
                                    recursiveJsonTraverse(i.data, indent);
                                    if (indent <= (segment.length + 1)) {
                                        indent--;
                                    }
                                }
                            }
                            else {
                                if (i.data) {
                                    recursiveJsonTraverse(i.data, indent);
                                }
                                //log(indent+" "+(segment.length-1)+" "+n);
                            }
                        break;
                        case "pug":
                            // for the parsed data, remove when fixed
                                if (i.text[0] === "\"") {
                                    i.text = JSON.parse(i.text);
                                }
                                //
                            text = pug.render(i.text, {
                                pretty: true,
                                vars: input.vars
                            });
                            textToInsertInJadeArray(text, indent);
                            //log(indent+" "+(segment.length-1)+" "+n);
                            break;
                        case "html":
                                text = i.text;
                            textToInsertInJadeArray(text, indent);
                            //log(indent+" "+(segment.length-1)+" "+n);
                            break;
                        case "node":
                                var evalSandbox = new Function("reqs", "vars", "cb", i.text);
                            try {
                                evalSandbox(reqs, input.vars, function(text) {
                                    textToInsertInJadeArray(text, indent);
                                    //log(indent+" "+(segment.length-1)+" "+n);
                                });
                            }
                            catch (err) {
                                textToInsertInJadeArray("Error check logs", indent);
                                log(err);
                            }
                            break;
                    }
                });
            };
            pugArray.push("doctype " + input.vars.head.constants.doctype);
            pugArray.push("html");
            if (_.defined(input.vars, "head.constants.title")) {
                pugArray.push("    " + "title " + input.vars.head.constants.title);
            }
            if (_.defined(input.vars, "head.constants.meta.charset")) {
                pugArray.push("    " + "meta(charset='" + input.vars.head.constants.meta.charset + "')");
            }
            if (_.defined(input.vars, "head.constants.meta.name.applicationName")) {
                pugArray.push("    " + "meta(name='application-name' content='" + input.vars.head.constants.meta.name.applicationName + "')");
            }
            if (_.defined(input.vars, "head.constants.meta.name.author")) {
                pugArray.push("    " + "meta(name='author' content='" + input.vars.head.constants.meta.name.author + "')");
            }
            if (_.defined(input.vars, "head.constants.meta.name.generator")) {
                pugArray.push("    " + "meta(name='generator' content='" + input.vars.head.constants.meta.name.generator + "')");
            }
            if (_.defined(input.vars, "head.constants.meta.name.keywords")) {
                pugArray.push("    " + "meta(name='keywords' content='" + input.vars.head.constants.meta.name.keywords + "')");
            }
            if (_.defined(input.vars, "head.constants.meta.name.description")) {
                pugArray.push("    " + "meta(name='description' content='" + input.vars.head.constants.meta.name.description + "')");
            }
            recursiveJsonTraverse(input.head.html, 1);
            pugArray.push("    body");
            recursiveJsonTraverse(input.body, 2);
            _.each(pugArray, function(i) {
                output += i + "\n";
            });
            var end = pug.render(output, {
                pretty: true,
                vars: input.vars,
                insert: insertData
            });
            next(end);
        }
    },
    jsonToJade: function(input, nested) {
        var output = "";
        for (var a = 0; a < nested; a++) {
            output += "    ";
        }
        if (!input.tag) {
            log(input);
            input.tag = "div";
        }
        output += input.tag;
        var addDataAttr = function(attr) {
            if (input[attr]) {
                if (!input.attr) {
                    input.attr = []
                }
                input.attr.push({
                    "name": "data-" + attr,
                    "value": input[attr]

                });
            }
        };
        addDataAttr("moduleid");
        addDataAttr("templateid");
        if (input.attr) {
            _.each(input.attr, function(a, b) {
                if (b === 0) {
                    output += "(";
                }
                if (b !== 0) {
                    output += ", ";
                }
                output += a.name + "=\"" + a.value + "\"";
                if (b + 1 === input.attr.length) {
                    output += ")";
                }
            });
        }
        if (input.id) {
            _.each(input.id, function(b) {
                output += "#" + b.name;
            });
        }
        if (input.classAttr) {
            _.each(input.classAttr, function(c) {
                output += "." + c.name;
            });
        }
        if (!input.insert) {
            if (input.text) {
                if (input.text[0] === "\"") {
                    input.text = JSON.parse(input.text);
                }
                output += " " + input.text;
            }
        }
        return output;
    },

    jsonToHtml: function() {

    },
    jsonToNode: function() {

    },
    pugToJade: function() {

    },
    sitemap: function(prot, next) {
        mongo.page.find({
            'robots': false
        }, 'name freq priority date', function(err, xml) {
            if (err) {
                log(err);
                next('error loading sitemap.xml');
            }
            else {
                var output = "<?xml version='1.0' encoding='UTF-8'?>\n\n<urlset xmlns='http://www.sitemaps.org/schemas/sitemap/0.9'>\n\n";
                for (var i = 0; i < xml.length; i++) {
                    var day = xml[i].date.getDate();
                    var month = parseInt(xml[i].date.getMonth(), 10) + 1;
                    var year = xml[i].date.getFullYear();
                    var date = year + "-" + month + "-" + day;
                    output += "\t<url>\n\n\t\t<loc>" + prot + "://" + config.baseurl + "/" + xml[i].name + "</loc>\n\n\t\t<lastmod>" + date + "</lastmod>\n\n\t\t<changefreq>" + xml[i].freq + "</changefreq>\n\n\t\t<priority>" + xml[i].priority + "</priority>\n\n\t</url>\n\n";
                }
                output += "</urlset>";
                next(output);
            }
        });
    },
    robots: function(next) {
        mongo.page.find({
            'robots': true
        }, 'name', function(err, robots) {
            if (err) {
                log(err);
                next('error loading robots.txt');
            }
            else {
                var output = "User-agent: *\n";
                for (var i = 0; i < robots.length; i++) {
                    output += "Disallow: " + "/" + robots[i].name + "/\n";
                }
                next(output);
            }
        });
    }
};
exports.render = render;

var newSubs = function(data, collection) {
    //log(data.name + " " + collection);
    var end = function() {
        log('done');
    };

    var cycle = function(type, sub) {
        //log(type);

        var options = {
            type: type,
            data: data,
            name: data.name,
            path: collection,
            callback: function(resultProcessed, resultData) {
                end();
            }
        };

        if (sub) {
            options.subName = sub;
        }

        util.localScripts(options);
    };

    if (data && data.data && data.data.resources) {
        if (data.data.resources.scripts && data.data.resources.scripts.text) {
            cycle("js");
        }
        if (data.data.resources.scripts && data.data.resources.scripts.requires && data.data.resources.scripts.requires.text) {
            cycle("reqs");
        }
        if (data.data.resources.css && data.data.resources.css.length > 0) {
            cycle("css");
        }
        if (data.data.resources.scripts && data.data.resources.scripts.browser && data.data.resources.scripts.browser.length>0) {
            _.each(data.data.resources.scripts.browser, function(i){
                cycle("browserScripts", i.name);
            });
        }
        if (data.data.resources.scripts && data.data.resources.scripts.require && data.data.resources.scripts.require.length>0) {
            _.each(data.data.resources.scripts.require, function(i){
                cycle("require", i.name);
            });
            
        }
    }
};

exports.newSubs = newSubs;

var trash = {
    delete: function(collection, document, callback) {
        mongo[collection].findById(document).exec(
            function(err, data) {
                if (err) {
                    log(err);
                }
                else {
                    if (!data) {
                        callback("Error deleting, no data found");
                    }
                    else {
                        var localScripts = data.data.resources;
                        var checkAndDeleteFiles = function(type) {
                            var folder = "";
                            var path = "";
                            var deleteLoop = function(path, folder) {
                                if (path) {
                                    fs.stat(path, function(err, stat) {
                                        if (err) {
                                            if (err.errno !== -2) {
                                                log(err);
                                            }
                                        }
                                        else {
                                            var stream = fs.createWriteStream(path);
                                            stream.once('open', function(fd) {
                                                stream.write("");
                                                stream.end();
                                                fs.unlink(path, function(err) {
                                                    if (err) {
                                                        log(err);
                                                    }
                                                    else {
                                                        log("File deleted: " + path);
                                                    }
                                                });
                                            });
                                        }

                                    });
                                }
                                if (folder) {
                                    fs.readdir(folder, function(err, files) {
                                        if (err) {
                                            if (err.errno !== -2) {
                                                log(err);
                                            }
                                        }
                                        else {
                                            var removeFolder = function() {
                                                fs.rmdir(folder, function(err) {
                                                    if (err) {
                                                        if (err.errno !== -2) {
                                                            log(err);
                                                        }
                                                    }
                                                    else {
                                                        log("Folder deleted: " + folder);
                                                    }
                                                });
                                            };
                                            if (files.length) {
                                                _.each(files, function(i) {
                                                    var stream = fs.createWriteStream(folder + "/" + i);
                                                    stream.once('open', function(fd) {
                                                        stream.write("");
                                                        stream.end();
                                                        fs.unlink(folder + "/" + i, function(err) {
                                                            if (err) {
                                                                log(err);
                                                            }
                                                            else {
                                                                log("File deleted: " + folder + "/" + i);
                                                            }
                                                        });
                                                        _.pull(files, i);
                                                        if (!files.length) {
                                                            removeFolder();
                                                        }
                                                    });
                                                });
                                            }
                                            else {
                                                if (!files.length) {
                                                    removeFolder();
                                                }
                                            }
                                        }
                                    });
                                }
                            };
                            switch (type) {
                                case "js":
                                    path = "./lib/scripts/js/" + collection + "/" + data.name + ".js";
                                    deleteLoop(path, null);
                                    break;
                                case "require":
                                    path = "./lib/reqs/" + collection + "/" + data.name + ".js";
                                    deleteLoop(path, null);
                                    break;
                                case "browserScripts":
                                    folder = "./lib/scripts/js/subjs/" + collection + "/" + data.name;
                                    deleteLoop(null, folder);
                                    break;
                                case "requireScripts":
                                    folder = "./lib/reqs/" + collection + "/" + data.name;
                                    deleteLoop(null, folder);
                                    break;
                                case "css":
                                    path = "./lib/scripts/css/" + collection + "/" + data.name + ".css";
                                    deleteLoop(path, null);
                                    break;
                            }
                        };
                        //JS
                        if (localScripts.scripts.text) {
                            checkAndDeleteFiles("js");
                        }
                        // req
                        if (localScripts.scripts.requires && localScripts.scripts.requires.text) {
                            checkAndDeleteFiles("require");
                        }
                        //subJS
                        if (localScripts.scripts.browser) {
                            checkAndDeleteFiles("browserScripts");
                        }
                        //sub Req
                        if (localScripts.scripts.require) {
                            checkAndDeleteFiles("requireScripts");
                        }
                        //css
                        if (localScripts.css) {
                            checkAndDeleteFiles("css");
                        }
                        delete data.__v;
                        var trashed = new mongo.trash(data);
                        trashed.original.id = trashed._id;
                        trashed._id = mongoose.Types.ObjectId();
                        trashed.original.details = "Deleted";
                        trashed.isNew = true;
                        mongo[collection].remove({
                            _id: document
                        }, function(err) {
                            if (err) {
                                log(err);
                            }
                            trashed.save(function(err) {
                                if (err) {
                                    log(err);
                                }
                                callback("Collection:" + collection + " Document:" + document + " Deleted");
                            });
                        });
                    }
                }
            }
        );
    }
};

exports.trash = trash;

var databaseTools = {
    backup: function(callback) {
        this.toBackup("segments");
        this.toBackup("media");
        this.toBackup("module");
        this.toBackup("page");
        this.toBackup("template");
        this.toBackup("trash");
        this.toBackup("user");
        callback("Back up complete");
    },
    toBackup: function(collection) {
        mongo[collection].find().exec(
            function(err, data) {
                if (err) {
                    log(err);
                }
                mongo.backUp[collection].remove({}, function(err) {
                    if (err) {
                        log(err);
                    }
                    log("Backing Up: " + collection);
                    _.each(data, function(i) {
                        i = JSON.parse(JSON.stringify(i));
                        delete i.__v;
                        var copy = new mongo.backUp[collection](i);
                        copy.save(function(err) {
                            if (err) {
                                log(err);
                            }
                            log(copy.name + " backed up");
                        });
                    });
                });
            });
    }

};

exports.databaseTools = databaseTools;
