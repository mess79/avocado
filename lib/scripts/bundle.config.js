//var webpack = require("webpack");
var fs = require('fs');
var WebpackOnBuildPlugin = require('on-build-webpack');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var mongo = require("../../models/mongo");
var glob = require("glob");
var _ = require('lodash');
var path = require('path');
//const originFolder = './lib/scripts/js/page/';

exports.files = function() {
    var result = glob.sync("./lib/scripts/js/page/*.js");
    var output = {};
    _.each(result, function(i) {
        i = i.split(".");
        i.pop();
        i = i.join("");
        var name = "entry/" + i.split('/')[5];
        output[name] = "." + i + ".js";
    });
    return output;
};

exports.config = {
    entry: {},
    output: {
        filename: "[name].[chunkHash:20].js",
        publicPath: "/scripts/js/",
        chunkFilename: "chunk/[id].[chunkHash:20].js",
        path: "./lib/scripts/js/output/"
    },
    resolve: {
        alias: {
            //modules: path.resolve(__dirname, 'src/utilities/'),
            //page: path.resolve(__dirname, ""),
            //templates: path.resolve(__dirname, 'src/templates/')

            module: path.resolve(__dirname, 'js/module'),
            page: path.resolve(__dirname, 'js/page'),
            template: path.resolve(__dirname, 'js/template'),
            cssModule: path.resolve(__dirname, 'css/module'),
            cssPage: path.resolve(__dirname, 'css/page'),
            cssTemplate: path.resolve(__dirname, 'css/template'),
            sub: path.resolve(__dirname, 'js/subjs')

        }
    },
    //devtool: 'source-map',
    module:
    /*{
            rules: [{
               test: /\.css$/,
               use: ExtractTextPlugin.extract({
                   fallback: 'style-loader',
                   use: 'css-loader'
              })
            }]
        },*/
    {
        loaders: [{
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: {
                        loader: "css-loader",
                        //options: {
                        //	sourceMap: true
                        //}
                    },
                    publicPath: "../"
                })
            }
            //,{ test: /\.png$/, loader: "file-loader" }
        ]
    },
    plugins: [
        /*new webpack.optimize.CommonsChunkPlugin({
            name: "common",
            filename: "/commons/[name].js"
                //, async: true
        }),*/
        new ExtractTextPlugin({
            filename: "../../css/output/[name].[contenthash:20].css",
            disable: false,
            allChunks: true
        }),
        //new ExtractTextPlugin('style.css'),
        /*new webpack.optimize.UglifyJsPlugin({
           sourceMap: true
        }),*/
        new WebpackOnBuildPlugin(function(stats) {
            //console.log('on build start');
            // tidy up build folders and update hashes in DB
            var updateHash = function(name, jshash, csshash) {
                //console.log('updateHash');
                mongo.page.findOne({
                    "name": name
                }, function(err, data) {
                    if (err) {
                        console.log(err);
                    }
                    else {
                        //console.log(name);
                        if(data){
                        if (!data.hash) {
                            data.hash = {
                                js: null,
                                css: null
                            };
                        }
                        //else {}
                        data.hash.js = jshash;
                        data.hash.css = csshash;
                        data.save(function(err) {
                            if (err) {
                                console.log(err);
                            }
                            else {
                                console.log("webpack hash updated: " + name + ":" + jshash);
                            }
                        });
                        }
                    }
                });
            };
            var newlyCreatedAssets = stats.compilation.assets;
            var chunks = stats.compilation.namedChunks;
            //console.log(stats.compilation.records["extract-text-webpack-plugin"]);
            for (var key in chunks) {
                //console.log("key in chunks");
                var file = key.split("/");
                if (file.length === 2) {
                    file.shift();
                }
                file = file.join();
                var obj = chunks[key];
                //console.log(obj.files);
                //console.log('before if 1');
                //console.log(obj);
                //if (obj.rendered) {
                    if (file !== "common") {
                        //console.log('before if 2');
                        var jshash;
                        var csshash;
                        _.each(obj.files, function(i) {
                            var hash = i.split(".");
                            var arrLength = hash.length;
                            var hashArrLength = arrLength - 2;
                            var mimeArrLength = arrLength - 1;
                            if (hash[mimeArrLength] === "css") {
                                csshash = hash[hashArrLength];
                            }
                            else if (hash[mimeArrLength] === "js") {
                                jshash = hash[hashArrLength];
                            }
                        });
                        //console.log(file, jshash, csshash);
                        updateHash(file, jshash, csshash);
                        jshash = null;
                        csshash = null;

                    }
                //}
            }
            var unlinked = [];
            var folderLoop = function(folder, buildDir) {
                fs.readdir(path.resolve(buildDir + folder + "/"), (err, files) => {
                    if (err) {
                        console.log(err);
                    }
                    else {
                        files.forEach(file => {
                            var pre = "";
                            if (buildDir === __dirname + "/css/output/") {
                                pre = "../../css/output/";
                            }
                            var testDir = newlyCreatedAssets[pre + folder + "/" + file];
                            if (!testDir) {
                                fs.unlink(path.resolve(buildDir + folder + "/" + file));
                                unlinked.push(file);
                            }
                        });
                        if (unlinked.length > 0) {
                            console.log('Removed old assets: ', unlinked);
                            unlinked = [];
                        }
                    }
                });
            };
            var buildDir = __dirname + "/js/output/";
            folderLoop("chunk", buildDir);
            folderLoop("entry", buildDir);
            folderLoop("commons", buildDir);
            buildDir = __dirname + "/css/output/";
            folderLoop("entry", buildDir);
        })
    ]
};
