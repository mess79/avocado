var webpack = require("webpack");
var config = require('./bundle.config.js').config;
var files = require('./bundle.config.js').files;
var watching;
var compiler;

var webpackRun = function() {

    config.entry = files();
    if (compiler && compiler.outputFileSystem) {
        try {
            watching.close();
        }
        catch (err) {
            console.log(err);
        }
    }

    compiler = webpack(config);
    watching = compiler.watch({
        aggregateTimeout: 300,
    }, function(err, stats) {
        if (err) {
            console.log(err);
        }
        else {
            console.log("Webpack updated files");
        }
    });

};
webpackRun();

module.exports = webpackRun;
