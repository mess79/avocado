console.log('subloaded editorConrol');

var validate = require("module/webpack_validation");
var $ = require('jquery');
var alertBox = require("module/ux_holder").modal.modal;
var errorAlert = require("module/ux_holder").modal.error;

module.exports = function (Backbone, editor, currentID) {

    var editorUndo = Backbone.View.extend({
        initialize: function (editor) {
            this.editor = editor;
        }
        , el: "#undo"
        , events: {
            "click": "clicked"
        }
        , clicked: function (e) {
            e.preventDefault();
            this.editor.undo(true);
        }
    });

    var editorRedo = Backbone.View.extend({
        initialize: function (editor) {
            this.editor = editor;
        }
        , el: "#redo"
        , events: {
            "click": "clicked"
        }
        , clicked: function (e) {
            e.preventDefault();
            this.editor.redo(true);
        }
    });

    var editorToolsUp = Backbone.View.extend({
        initialize: function (editor) {
            this.editor = editor;
        }
        , el: ".tools-up"
        , events: {
            "click": "clicked"
        }
        , clicked: function (e) {
            e.preventDefault();
            var pos = this.editor.getCursorPosition();
            pos.row--;
            if (this.$el.closest(".dropdownInnerContent").find(".tools-select").parent().css("display") !== "none") {
                this.editor.navigateTo(pos.row, pos.column);
            } else if (this.$el.closest(".dropdownInnerContent").find(".tools-copy").parent().css("display") !== "none") {
                //log('fddsf');
                this.editor.moveCursorToPosition(pos);
            }
        }
    });

    var editorToolsDown = Backbone.View.extend({
        initialize: function (editor) {
            this.editor = editor;
        }
        , el: ".tools-down"
        , events: {
            "click": "clicked"
        }
        , clicked: function (e) {
            e.preventDefault();
            var pos = this.editor.getCursorPosition();
            pos.row++;
            if (this.$el.closest(".dropdownInnerContent").find(".tools-select").parent().css("display") !== "none") {
                this.editor.navigateTo(pos.row, pos.column);
            } else if (this.$el.closest(".dropdownInnerContent").find(".tools-copy").parent().css("display") !== "none") {
                this.editor.moveCursorToPosition(pos);
            }
        }
    });

    var editorToolsLeft = Backbone.View.extend({
        initialize: function (editor) {
            this.editor = editor;
        }
        , el: ".tools-left"
        , events: {
            "click": "clicked"
        }
        , clicked: function (e) {
            e.preventDefault();
            var pos = this.editor.getCursorPosition();
            pos.column--;
            if (this.$el.closest(".dropdownInnerContent").find(".tools-select").parent().css("display") !== "none") {
                this.editor.navigateTo(pos.row, pos.column);
            } else if (this.$el.closest(".dropdownInnerContent").find(".tools-copy").parent().css("display") !== "none") {
                this.editor.moveCursorToPosition(pos);
            }
        }
    });

    var editorToolsRight = Backbone.View.extend({
        initialize: function (editor) {
            this.editor = editor;
        }
        , el: ".tools-right"
        , events: {
            "click": "clicked"
        }
        , clicked: function (e) {
            e.preventDefault();
            var pos = this.editor.getCursorPosition();
            pos.column++;
            if (this.$el.closest(".dropdownInnerContent").find(".tools-select").parent().css("display") !== "none") {
                this.editor.navigateTo(pos.row, pos.column);
            } else if (this.$el.closest(".dropdownInnerContent").find(".tools-copy").parent().css("display") !== "none") {
                this.editor.moveCursorToPosition(pos);
            }
        }
    });

    var editorToolsTab = Backbone.View.extend({
        initialize: function (editor) {
            this.editor = editor;
        }
        , el: ".tools-tab"
        , events: {
            "click": "clicked"
        }
        , clicked: function (e) {
            e.preventDefault();
            editor.indent();
        }
    });

    var editorToolsTabRemove = Backbone.View.extend({
        initialize: function (editor) {
            this.editor = editor;
        }
        , el: ".tools-tabRemove"
        , events: {
            "click": "clicked"
        }
        , clicked: function (e) {
            e.preventDefault();
            editor.blockOutdent();
        }
    });


    var editorToolsComment = Backbone.View.extend({
        initialize: function (editor) {
            this.editor = editor;
        }
        , el: ".tools-comment"
        , events: {
            "click": "clicked"
        }
        , clicked: function (e) {
            e.preventDefault();
            var text = this.editor.getCopyText();
            this.editor.insert("/* " + text + " */");
        }
    });

    var editorToolsClear = Backbone.View.extend({
        initialize: function (editor) {
            this.editor = editor;
        }
        , el: ".tools-clear"
        , events: {
            "click": "clicked"
        }
        , clicked: function (e) {
            e.preventDefault();
            this.editor.insert("");
        }
    });

    var editorToolsSelect = Backbone.View.extend({
        initialize: function (editor) {
            this.editor = editor;
        }
        , el: ".tools-select"
        , events: {
            "click": "clicked"
        }
        , clicked: function (e) {
            e.preventDefault();
            this.$el.parent().addClass("hd");
            this.$el.closest(".dropdownInnerContent").find(".tools-copy").parent().removeClass("hd");
        }
    });

    var clipboard = "";

    var editorClipboard = Backbone.View.extend({
        initialize: function (editor) {
            this.editor = editor;
            //this.clipboard = "";
            var editorToolsCopyApp = new editorToolsCopy(this.editor);
            var editorToolsPasteApp = new editorToolsPaste(this.editor);


        }
    });

    var editorToolsCopy = Backbone.View.extend({
        initialize: function (editor) {
            this.editor = editor;
            //this.clipboard = clipboard;
        }
        , el: ".tools-copy"
        , events: {
            "click": "clicked"
        }
        , clicked: function (e) {
            e.preventDefault();
            clipboard = editor.getCopyText();
            this.$el.parent().addClass("hd");
            this.$el.closest(".dropdownInnerContent").find(".tools-select").parent().removeClass("hd");
        }
    });

    var editorToolsPaste = Backbone.View.extend({
        initialize: function (editor) {
            this.editor = editor;
            //this.clipboard = clipboard;
        }
        , el: ".tools-paste"
        , events: {
            "click": "clicked"
        }
        , clicked: function (e) {
            e.preventDefault();
            editor.insert(clipboard);
        }
    });

    var editorGoto = Backbone.View.extend({
        //el: "#goto"
        initialize: function (editor) {
            this.editor = editor;
        }
        , events: {
            "click": "clicked"
        }
        , clicked: function (e) {
            e.preventDefault();
            var self = this;
            validate(self.$el.closest('form'), null, function (validates) {
                if (validates) {
                    var input = self.$el.closest('form').find("input").val();
                    self.editor.gotoLine(input);
                }
            });
        }
    });

    var editorFind = Backbone.View.extend({
        el: "#find"
        , initialize: function (editor) {
            this.editor = editor;
        }
        , events: {
            "click": "clicked"
        }
        , clicked: function (e) {
            e.preventDefault();
            var self = this;
            validate(self.$el.closest('form'), null, function (validates) {
                if (validates) {
                    var needle = self.$el.closest('form').find("input").val();
                    self.editor.findNext({
                        needle: needle
                        , wrap: true
                        , caseSensitive: true
                    , }, true);
                }
            });
        }
    });

    var editorPrev = Backbone.View.extend({
        el: "#prev"
        , initialize: function (editor) {
            this.editor = editor;
        }
        , events: {
            "click": "clicked"
        }
        , clicked: function (e) {
            e.preventDefault();
            var self = this;
            validate(self.$el.closest('form'), null, function (validates) {
                if (validates) {
                    var needle = self.$el.closest('form').find("input").val();
                    self.editor.findPrevious({
                        needle: needle
                        , wrap: true
                        , caseSensitive: true
                    , }, true);
                }
            });
        }
    });

    var editorReplace = Backbone.View.extend({
        el: "#replace"
        , initialize: function (editor) {
            this.editor = editor;
        }
        , events: {
            "click": "clicked"
        }
        , clicked: function (e) {
            e.preventDefault();
            var self = this;
            var prev = self.$el.closest('form').prev();
            validate(self.$el.closest('form'), null, function (validateFind) {
                validate(prev, null, function (validateReplace) {
                    if (validateFind && validateReplace) {
                        var needle = prev.find("input").val();
                        var newText = self.$el.closest('form').find("input").val();
                        self.editor.replace(newText, {
                            needle: needle
                            , wrap: true
                            , caseSensitive: true
                        });
                    }
                });
            });
        }
    });

    var editorReplaceAll = Backbone.View.extend({
        el: "#replaceAll"
        , initialize: function (editor) {
            this.editor = editor;
        }
        , events: {
            "click": "clicked"
        }
        , clicked: function (e) {
            e.preventDefault();
            var self = this;
            var prev = self.$el.closest('form').prev();
            validate(self.$el.closest('form'), null, function (validateFind) {
                validate(prev, null, function (validateReplace) {
                    if (validateFind && validateReplace) {
                        var needle = prev.find("input").val();
                        var newText = self.$el.closest('form').find("input").val();
                        var replaced = self.editor.replaceAll(newText, {
                            needle: needle
                            , wrap: true
                            , caseSensitive: true
                        });
                        alertBox("message", {
                            title: "Replace All Count"
                            , text: replaced + " instances of \"" + needle + "\" replaced with \"" + newText + "\""
                        });
                    }
                });
            });
        }
    });

    var editorBeautify = Backbone.View.extend({
        el: "#beautify"
        , initialize: function (editor) {
            this.editor = editor;
            this.listenTo(currentID, "change", function (data) {
                if (data.get("currentID") !== null) {
                    this.render();
                }
            });
        }
        , events: {
            "click": "clicked"
        }
        , clicked: function (e) {
            e.preventDefault();
            var target = this;
            var tab = currentID.get('currentTab');
            var type = "js";
            if (tab === "#cssEditor") {
                type = "css";
            }
            target.$el.loaderProgIn();
            $.ajax({
                method: "POST"
                , url: "/beautify/" + type
                , data: {
                    text: target.editor.getValue()
                }
                , success: function (result) {
                    target.editor.setValue(result);
                    target.$el.loaderProgOut();
                }
                , error: function (error) {
                    target.$el.loaderProgOut();
                    errorAlert(error);
                }
            });
        }
        , render: function () {
            var tab = currentID.get('currentTab');
            if (
                tab === "#requireScripts" ||
                tab === "#serverScripts" ||
                tab === "#browserScripts" ||
                tab === "#jsonload" ||
                tab === "#reqs" ||
                tab === "#js" ||
                tab === "#cssEditor"
            ) {
                this.$el.prop("disabled", false);
            } else if (
                tab === "#modules" ||
                tab === "#info" ||
                tab === "#body" ||
                tab === "#pug" ||
                tab === "#console" ||
                tab === "#backup" ||
                tab === "#mongodb" ||
                tab === "#selector" ||
                tab === "#history" ||
                tab === "#head"
            ) {
                this.$el.prop("disabled", true);
            }
        }
    });


    var editorReplaceApp = new editorReplace(editor);
    var editorReplaceAllApp = new editorReplaceAll(editor);
    var editorRedoApp = new editorRedo(editor);
    var editorToolsUpApp = new editorToolsUp(editor);
    var editorToolsDownApp = new editorToolsDown(editor);
    var editorToolsLeftApp = new editorToolsLeft(editor);
    var editorToolsRightApp = new editorToolsRight(editor);
    var editorToolsTabApp = new editorToolsTab(editor);
    var editorToolsTabRemoveApp = new editorToolsTabRemove(editor);
    var editorToolsSelectApp = new editorToolsSelect(editor);
    var editorToolsClipboardApp = new editorClipboard(editor);
    var editorToolsCommentApp = new editorToolsComment(editor);
    var editorToolsClearApp = new editorToolsClear(editor);
    var editorGotoAApp = new editorGoto(editor);
    editorGotoAApp.setElement("#gotoA");
    var editorGotoBApp = new editorGoto(editor);
    editorGotoBApp.setElement("#gotoB");
    var editorFindApp = new editorFind(editor);
    var editorPrevApp = new editorPrev(editor);
    var editorUndoApp = new editorUndo(editor);
    var editorBeautifyApp = new editorBeautify(editor);
};