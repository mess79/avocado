console.log('subloaded updateHistory');
var log = require("module/log");
var _ = require('lodash');
var $ = require('jquery');
var ux = require("module/ux_holder");
var alertBox = ux.modal.modal;
var errorAlert = ux.modal.error;
JSON.prune = require("module/webpack_prune");

module.exports = function (Backbone) {

    var trashModel = Backbone.Model.extend();
    var trashCollection = Backbone.Collection.extend({
        model: trashModel
    });
    var trash = new trashCollection();
var next = true;
    var varsModel = Backbone.Model.extend({});
    var vars = new varsModel({
        total: 0
    });

    var totalView = Backbone.View.extend({
        //el: ".count"
        initialize: function () {
            this.listenTo(vars, "change", function (data) {
                this.render(data.get("total"));
            });
        }
        , render: function (data) {
            this.$el.text(data);
        }
    });

    $.ajax({
        url: "/api/count/trash/"
        , success: function (data) {
            vars.set("total", data.total);
        }
        , cache: false
    });

    var range = Backbone.View.extend({
        initialize: function () {
            this.changed();
        }
        , events: {
            "change": "changed"
        }
        , changed: function (e) {
            var target = this;
            var rangeText = this.$el.closest('.tools').find('.selected').text();
            rangeText = rangeText.split("-");
            var start = parseInt(rangeText[0], 10);
            var amount = parseInt(this.$el.val(), 10);
            if (!start) {
                start = 1;
            }
            if (!amount) {
                amount = 10;
            }
            target.$el.loaderProgIn();
            trash.url = "/api/documentlist/trash/" + start + "/" + amount;
            trash.fetch({
                success: function (data) {
                    var end = start + data.length;
                    end--;
                    var text = start + "-" + end;
                    if (start >= end) {
                        text = end + "-" + end;
                    }
                    target.$el.closest('.tools').find('.selected').text(text);
                    target.$el.loaderProgOut();
                }
                , error: function (error) {
                    log(error);
                    target.$el.loaderProgOut();
                }
            });
        }
    });

    var nextRange = Backbone.View.extend({
        events: {
            "click": "clicked"
        }
        , clicked: function (e) {
            var target = this;
            var rangeText = this.$el.closest('.tools').find('.selected').text();
            rangeText = rangeText.split("-");
            var start = parseInt(rangeText[0], 10);
            if (rangeText.length > 1) {
                start = parseInt(rangeText[1], 10);
            }
            var amount = parseInt(this.$el.closest(".tools").find(".range").val(), 10);
            if (!amount) {
                amount = 10;
            }
            next = true;
            if (this.$el.hasClass("previous")) {
                next = false;
                start = rangeText[0] - amount;
                start--;
            }
            if (!start || start < 1) {
                start = 0;
            }
            if (next && start >= vars.get("total")) {
                alertBox("message", {
                    title: "No more data"
                    , text: "You have reached the end, no more data"
                });
            } else {
            start++;
            target.$el.loaderProgIn();
            trash.url = "/api/documentlist/trash/" + start + "/" + amount;
            trash.fetch({
                success: function (data) {
                    var end = start + data.length - 1;
                    target.$el.closest('.tools').find('.selected').text(start + "-" + end);
                    target.$el.loaderProgOut();
                }
                , error: function (error) {
                    log(error);
                    target.$el.loaderProgOut();
                }
            });
        }
        }
    });

    var historyViews = Backbone.View.extend({
        el: "#historyTable"
        , initialize: function () {
            this.listenTo(trash, "update", function (data) {
                this.render(data);
            });
            var rangeApp = new range();
            rangeApp.setElement(this.$el.find(".range"));
            var nextRangeApp = new nextRange();
            nextRangeApp.setElement(this.$el.find(".next"));
            var prevRangeApp = new nextRange();
            prevRangeApp.setElement(this.$el.find(".previous"));
            var totalViewApp = new totalView();
            totalViewApp.setElement(this.$el.find(".count"));
        }
        , template: _.template("<tr><td><%= date %></td><td><%= method %></td><td><%= name %></td><td><%= collection %></td><td><%= id %></td><td><button class='button view v-1 ux-h g-6-6'>view</button></td><td><button class='button restore v-1 ux-h g-6-6'>restore</button></td></tr>")
        , render: function (data) {
            var target = this;
            target.$el.find("tr:gt(0)").remove();
            if (data) {
                data.models.reverse();
                data.each(function (i) {
                    var data = i.toJSON();
                    var dateString = String(data.original.deleteDate);
                    var date = new Date(dateString);
                    date = date.toUTCString();
                    var output = {
                        date: date
                        , method: data.original.details
                        , name: data.name
                        , collection: data.type
                        , id: data.original.id
                            //, json: data
                    };
                    var obj = target.template(output);
                    obj = $.parseHTML(obj);
                    $(obj).find('.view').click(function () {
                        target.$el.loaderProgIn();
                        $.get("/api/trash/" + data._id, function (result) {
                            alertBox("message", {
                                title: "Document View"
                                , text: JSON.prune(result)
                            }, function () {
                                target.$el.loaderProgOut();
                            });
                        });
                    });
                    $(obj).find('.restore').click(function () {
                        target.$el.loaderProgIn();
                        $.get("/api/trash/" + data._id, function (result) {
                            alertBox("message", {
                                title: "Document View"
                                , text: JSON.prune(result)
                            }, function () {
                                target.$el.loaderProgOut();
                            });
                        });
                    });
                    target.$el.find('tr:first-child').after(obj);
                });
            }
        }
    });

    var app = new historyViews();

};