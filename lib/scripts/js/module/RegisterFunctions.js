require.ensure(["jquery", "module/webpack_validation"], function (require) {
    var $ = require('jquery');
    var validate = require('module/webpack_validation');
    $('#create').on('click', function (event) {
        event.preventDefault();
        var self = this;
        $(self).loaderProgIn();
        validate($(self).closest("form"), null, function (validates) {
            if (validates) {
                $.ajax({
                    type: "POST"
                    , url: "/auth/register"
                    , data: $(self).closest("form").serialize()
                    , success: function(data){
                        //console.log(data);
                        //console.log(response);
                        if (data[0] === 0 || data[0] === 4) {
                    $("#user").parent().addClass("hd");
                    $("#pass").parent().addClass("hd");
                    $("#captchaimg").parent().parent().addClass("hd");
                    $("#captcha").parent().addClass("hd");
                    $("#login").parent().addClass("hd");
                    $("#register").parent().parent().addClass("hd");
                }
                if (data[0] === 4) {
                    $('.login, .loggedIn').removeClass("login").addClass('loggedIn').text(data[2]);
                    //console.log('dfdfsf');
                    $("#logout").parent().removeClass("hd");
                }
                if (data[0] === 3) {
                    //$("#captchaimg").attr("src", "#");
                    $("#captchaimg").attr("src", "/captcha/"+ new Date().getTime());
                    $("#captchaimg").parent().parent().removeClass("hd");
                    $("#captcha").parent().removeClass("hd");
                }
                //console.log(data);
                $("#loginmsg").text(data[1]);
                $("#loginmsg").parent().removeClass("hd");
                        $(self).loaderProgOut();
                    }
                    , error: function (err) {
                        console.log(err);
                        $(self).loaderProgOut();
                    }
                 //   , dataType: dataType
                });
                
            } else {
                $(self).loaderProgOut();
            }
        });
    });
});