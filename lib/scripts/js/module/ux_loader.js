require ('cssModule/ux_loader.css');

//console.log(__dirname);

//require.ensure(["jquery", "lodash"], function (require) {

var $ = require("jquery");
var _ = require("lodash");

$.fn.slickOut = function (callback, customEnd) {
        $(this).animate({
            opacity: 0
        }, 100, function () {
            $(this).animate({
                height: 0
                , width: 0
            }, 300, function () {
                if (!customEnd) {
                    $(this).remove();
                }
                if (_.isFunction(callback)) {
                    callback();
                }
            });
        });
    };

$.fn.loaderProgIn = function (callback) {
        var classes = ($(this).attr("class"));
        if (!classes) {
            classes = "";
        } else {
            classes = classes.split(" ");
            classes = _.filter(classes, function (i) {
                return /^g-*/.exec(i);
            });
            classes = classes.join(" ");
        }
        $(this).wrap("<div class=\"l-h\" style=\"position:relative;\">");
        $(this).parent().addClass(classes);
        $(this).removeClass(classes);
        $(this).addClass("l-max");
        $(this).before("<div class=\"l\"><div class=\"l-box\"><div class=\"l-obj1\"></div><div class=\"l-obj2\"></div><div class=\"l-obj3\"></div></div></div>");
        if ($(this).hasClass("b")) {
            $(this).prev().addClass('b');
        }
        $(this).closest('.l-h').find(".l").css("display", "block").animate({
            opacity: 1
        }, 100, function () {
            if (_.isFunction(callback)) {
                callback();
            }
        });
    };

    $.fn.loaderProgOut = function (callback) {
        var target = this;
        $(this).closest('.l-h').children(".l").animate({
            opacity: 0
        }, 300, function () {
            var classes = ($(this).closest('.l-h').attr("class"));
            if (!classes) {
                classes = "";
            } else {
                classes = classes.split(" ");
                classes = _.filter(classes, function (i) {
                    return /^g-*/.exec(i);
                });
                classes = classes.join(" ");
            }
            $(target).siblings().remove();
            $(target).unwrap();
            $(target).removeClass("l-max");
            $(target).addClass(classes);
            if (_.isFunction(callback)) {
                callback();
            }
        });
    };
    console.log('loader loaded');
//});
