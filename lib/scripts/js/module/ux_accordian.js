require.ensure(['jquery'], function (require) {
var $ = require('jquery');

$(".ux-accordianLabel").click(function (event) {
            event.preventDefault();
            if ($(this).hasClass('ux-accordianSelected')) {
                $(this).closest(".accordianHolder").find(".ux-accordianSelected").next().animate({
                    opacity: 0
                }, 300, function () {
                    $(this).css({
                        "display": "none"
                    });
                    $(this).closest(".accordianHolder").find(".ux-accordianSelected").removeClass("ux-accordianSelected");
                });
            } else {
                $(this).closest(".accordianHolder").find(".ux-accordianSelected").removeClass("ux-accordianSelected").next().css({
                    "opacity": 0
                    , "display": "none"
                });
                $(this).addClass("ux-accordianSelected").next().css({
                    "display": "block"
                }).animate({
                    opacity: 1
                }, 300, function () {});

            }
        });
});