//require.ensure(['jquery', "lodash"], function (require) {
    
    require('cssModule/webpack_validation.css');
    var $ = require('jquery');
    var _ = require('lodash');
    
    
/*module.exports = function( value ){
    console.log(value);
    
};*/

    
    var validate = function (form, message, cb) {

        // validation highlighting

        var validatehighlight = function (obj, validationText) {

            // parse the validation array
            $(obj).off("focusout");
            var text = "<ul>";

            _.each(validationText, function (i) {
                text += "<li>" + i + "</li>";
            });

            text += "</ul>";

            $(obj).addClass('ux-form-validate');
            $(obj).closest('label').children().first().addClass('ux-form-validate-info');
            if ($(obj).parent().parent().find(".form-validate-rules-container").length === 0) {
                $(obj).parent().after("<div class=\"form-validate-rules-container g-6-6\"><div class=\"ux-form-validate-rules g-6-6\"><div class=\"ux-form-validate-rules-text g-6-6\">" + text + "</div></div></div>");
            } else {
                $(obj).parent().parent().find(".ux-form-validate-rules-text").html(text);
            }
            $(obj).on("focusout", function () {
                validate($(obj).parent());
            });
        };

        var validateunhighlight = function (obj) {
            $(obj).removeClass('ux-form-validate');
            $(obj).closest('label').children().first().removeClass('i info ux-form-validate-info');
            $(obj).parent().parent().find(".form-validate-rules-container").slickOut();

        };

        // validation rules

        var validates = true;
        var validationRules = function (data, cb) {
            var output = data.split(" ");
            cb(output);
        };

        _.each($(form).find('select, input:text, input:password'), function (i) {

            if ($(i).data("validate")) {
                validationRules($(i).data("validate"), function (validationRules) {

                    var rules = [];

                    // rules

                    _.each(validationRules, function (v) {
                        switch (v) {
                        case "compulsary":
                            if (!$(i).val()) {
                                if ($(i).prop("tagName") === "SELECT") {
                                    rules.push("Please make a selection");
                                } else {
                                    rules.push("Cannot be left blank");
                                }
                            }
                            break;
                        case "lettersOnly":
                            if (!/^[a-zA-Z]+$/.test($(i).val())) {
                                rules.push("Can only contain letters a-Z");
                            }
                            break;
                        case "numbersOnly":
                            if (!/^[0-9]+$/.test($(i).val())) {
                                rules.push("Can only contain only numbers 0-9");
                            }
                            break;
                        case "email":
                            if (!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test($(i).val())) {
                                rules.push("must be a valid email");
                            }
                            break;
                        case "leadWithLetter":
                            if (!/^[a-zA-Z]/.test($(i).val())) {
                                rules.push("Must start with a Letter");
                            }
                            break;
                        case "classIdentifier":
                            if (!/^[_a-zA-Z]+[_a-zA-Z0-9-]{1,}$/.test($(i).val())) {
                                rules.push("Must start with a Letter or \"_\" and then be followed by letters, numbers \"_\" or \"-\"");
                            }
                            break;
                        case "password":
                            if ($(i).val().length < 6) {
                                 rules.push("Must be at least 6 charachters long");
                            } if ($(i).val().length > 50) {
                                 rules.push("Cannot be no longer than 50 charachters");
                            } if (!/\d/.test($(i).val())) {
                                 rules.push("Must contain at least one number");
                            } if (!/[a-zA-Z]/.test($(i).val())) {
                                 rules.push("Must contain at least one letter");
                            //} else if (str.search(/[^a-zA-Z0-9\!\@\#\$\%\^\&\*\(\)\_\+]/) != -1) {
                              //   rules.push("bad_char");
                            }
                            break;
                        case "unique":
                            var test = false;
                            var val = $(i).val();
                            var arrayToCheck = $(i).closest(".wordSelector").find(".wordSelectorList .ux-details");
                            _.each(arrayToCheck, function (a) {
                                if ($(a).text() === val) {
                                    test = true;
                                }
                            });
                            if (test) {
                                rules.push("This has already been used, please choose a unique word/pattern");
                            }
                            break;
                        }

                    });

                    // test to see if any rules were broken and highlight or unhightlight

                    if (rules.length > 0) {
                        validatehighlight(i, rules);
                        validates = false;
                    } else {
                        validateunhighlight(i);
                    }
                });
            }
        });


        if (!validates && message) {
            alertBox("message", {
                title: "Validation"
                , text: "Please complete highlighted fields"
            });

        }
        if (_.isFunction(cb) === true) {
            cb(validates);
        }

    };
    module.exports = validate;
    console.log('Validation Loaded');
//});