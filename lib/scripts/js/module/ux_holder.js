/*require.ensure([
    'module/ux_loader'
    , 'module/ux_block'
    , 'module/ux_dropdown'
    , 'module/ux_accordian'
    , 'module/ux_addRemove'
    , 'module/ux_modal'
], function (require) {*/
    var output = {};
    require('module/ux_loader');
    require('module/ux_block');
    require('module/ux_addRemove');
    require('module/ux_dropdown');
    require('module/ux_accordian');
    output.table = require('module/ux_table');
    output.modal = require('module/ux_modal');
    module.exports = output;
    console.log('UX Holder loaded');
//});