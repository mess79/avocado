
var $ = require("jquery");
var _ = require("lodash");

$.fn.slickOut = function (callback, customEnd) {
        $(this).animate({
            opacity: 0
        }, 100, function () {
            $(this).animate({
                height: 0
                , width: 0
            }, 300, function () {
                if (!customEnd) {
                    $(this).remove();
                }
                if (_.isFunction(callback)) {
                    callback();
                }
            });
        });
    };

    $.fn.slickIn = function (callback) {};