var $ = require('jquery');
JSON.prune = require("module/webpack_prune");
// For todays date;
Date.prototype.today = function () {
    return ((this.getDate() < 10) ? "0" : "") + this.getDate() + "/" + (((this.getMonth() + 1) < 10) ? "0" : "") + (this.getMonth() + 1) + "/" + this.getFullYear();
};

// For the time now
Date.prototype.timeNow = function () {
    return ((this.getHours() < 10) ? "0" : "") + this.getHours() + ":" + ((this.getMinutes() < 10) ? "0" : "") + this.getMinutes() + ":" + ((this.getSeconds() < 10) ? "0" : "") + this.getSeconds() + ":" + ((this.getMilliseconds() < 10) ? "0" : "") + this.getMilliseconds();
};

var log = function (input, trace) {
    console.log(input);
    if(trace){
        console.trace();
    }
    var type = "[" + typeof input + "] ";
    var target = $("#console").find('.scrollBox');
    var timeStamp = new Date().timeNow() + " : ";
    if (!input) {
        input = "Empty log request?";
    }
    if (typeof input === "object") {
        var json = JSON.prune(input, {
            prunedString: undefined
        });
        input = "\n" + JSON.stringify(JSON.parse(json), null, 4);
    }
    input = timeStamp + type + input;
    if (target.text().length) {
        input = target.text() + "\n" + input;
    }
    var maxLength = 10000;
    if (input.length > maxLength) {
        var start = input.length - maxLength;
        input = input.substring(start);
    }
    target.text(input);
    var height = target[0].scrollHeight;
    target.scrollTop(height);
};

log("Console started: " + new Date().today() + "\n");
module.exports = log;