//require.ensure(["jquery", "lodash", "../module/ux_block", "../module/webpack_validation"], function (require) {

var $ = require('jquery');
var _ = require('lodash');
var validate = require("module/webpack_validation");
var block = require("module/ux_block");

var alertBox = function (type, message, next) {
    var response = false;
    var validates = true;
    var html = "";
    //var tag = "div";
    //if (type===form)
    html += "<form style=\"opacity:0; top:100px\" class=\"alertHolder\"><div class=\"alertMiddle\"><div style=\"float:none;\" class=\"alert g-5-6\">";
    html += "<div class=\"alertTitle g-6-6 ux-no-h v-1\">" + message.title + "</div>";
    html += "<div class=\"ux alertBox g-6-6 bf-t\">";
    html += "<div class=\"g-6-6 b flex\"><div class=\"b g-6-6\"><div class=\"ux alertText\">" + message.text + "</div></div>";
    switch (type) {
    case "message":
        html += "<div class=\"b g-6-6\"><button action=\"#\" class=\"alertButton alertClose g-6-6 v-1 ux-h\">Close</button></div>";
        break;
    case "continue":
        html += "<div class=\"b g-3-6\"><button action=\"#\" class=\"alertButton alertContinue g-6-6 v-1 ux-h\">Continue</button></div>";
        html += "<div class=\"b g-3-6\"><button action=\"#\" class=\"alertButton alertClose g-6-6 v-1 ux-h\">Cancel</button></div>";
        break;
    case "boolean":
        html += "<div class=\"b g-3-6\"><button action=\"#\" class=\"alertButton alertContinue g-6-6 v-1 ux-h\">Yes</button></div>";
        html += "<div class=\"b g-3-6\"><button action=\"#\" class=\"alertButton alertClose g-6-6 v-1 ux-h\">No</button></div>";
        break;
    case "form":
        html += "<div class=\"b g-3-6\"><button action=\"#\" class=\"alertButton alertContinue g-6-6 v-1 ux-h\">Submit</button></div>";
        html += "<div class=\"b g-3-6\"><button action=\"#\" class=\"alertButton alertClose g-6-6 v-1 ux-h\">Cancel</button></div>";
        break;
    }
    html += "</div></div></div></div></form>";
    html = $(html);
    $("body").prepend(html);
    block.show();
    $(html).closest(".alertHolder").animate({
        opacity: 1
        , top: "0"
    }, 300, function () {
        $(html).find("select, textarea, input").first().focus();
    });
    $(".alertContinue").click(function (event) {
        event.preventDefault();
        response = true;
        if (type === "form") {
            //var form = 
            validate($(html), null, function (validated) {
                if (validated) {

                    response = _.fromPairs(_.map($(html).find("select, textarea, input").serializeArray(), _.values));
                    //console.log(response);


                    //response = _object(_.map($(html).find("select, textarea, input").serializeArray(), _.values));



                    validates = true;
                } else {
                    validates = false;
                }
            });

        }
    });
    $(".alertButton").click(function (event) {
        event.preventDefault();
        if (validates || $(this).hasClass('alertClose')) {
            if ($(this).hasClass('alertClose')) {
                response = false;
            }
            var target = $(this).closest(".alertHolder");
            target.animate({
                opacity: 0
                , top: "-100"
            }, 300, function () {
                block.hide();
                target.remove();
                if (_.isFunction(next)) {
                    next(response);
                }
            });
        }
    });
};
exports.modal = alertBox;

var errorFunction = function(error, cb){
    if(!_.isFunction(cb)){
        cb = function(){};
    }
    if(!error){
        error={
            status:0
            ,statusText : "Unknown error"
            , responseJSON:{
                error: "Check connection or server logs"
            }
        };
    }
    if(!error.status || error.status!==0){
        error.status=500;
    }
    if(!error.statusText){
        error.statusText= "Unknown error";
    }
    if(!error.responseJSON){
        error.responseJSON = {};
    }
    if(!error.responseJSON.error){
        error.responseJSON.error ="Check server logs";
    }
    
    
    
alertBox("message", {
    title: "Error: " + error.status + " " + error.statusText
    , text: error.responseJSON.error
}, cb());
};

exports.error = errorFunction;
//});


































