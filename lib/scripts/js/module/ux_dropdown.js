require.ensure(['jquery', "module/ux_block"], function (require) {

    var $ = require("jquery");
    var block = require("module/ux_block");
    $(".dropdownLabel").click(function (event) {
        event.preventDefault();
        //log(this.caller);
        var target = $(this).closest(".dropdown");
        $(".blocker").off('click.dropDown');
        if ($(this).hasClass('dropdownDown')) {
            target.css({
                "z-index": "auto"
            });
            target.find(".dropdownContent").css({
                "display": "none"
            });
            if (!target.hasClass("dropdown-NoBlock")) {
                block.hide();
            }
            $(this).removeClass("dropdownDown");
        } else {
            $(this).addClass("dropdownDown");
            target.css({
                "z-index": "9998"
            });
            target.find(".dropdownContent").css({
                "display": "block"
            });
            if (!target.hasClass("dropdown-NoBlock")) {
                block.show();
            }
            $(".blocker").on("click.dropDown", function () {
                target.find(".dropdownLabel").trigger('click');
            });
        }
    });
});