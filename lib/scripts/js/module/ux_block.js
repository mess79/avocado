
//require.ensure(['jquery', "../module/ux_block"], function (require) {

var $ = require("jquery");
$('body').prepend("<div style=\"opacity:0;display:none\" class=\"blocker\"></div>");
$(window).resize(function () {
        //log('here');
        $(".blocker").css({
            "height": $(document).height()
        });
    });
module.exports = {
            hide: function () {
                $(".blocker").css({
                    "opacity": "0"
                    , "display": "none"
                });
            }
            , show: function () {
                $(".blocker").css({
                    "opacity": "0.8"
                    , "display": "block"
                    , "height": $(document).height()
                });
                //log($(document).height()+" "+$("body").height());
            }
        };