define(["jquery", "lodash", "ace/ace", "Loader", "validation", "prune"], function ($, _, ace, loadr, validate, prune) {
    
/*require.ensure(["jquery", "../module/loader", "brace", "lodash", "../module/validation", "../module/prune", "backbone"], function (require) {

var $ = require("jquery");
var _ = require("lodash");
var ace = require("brace");
    require('../module/Loader');
    //require('../module/login');

var prune = require("../module/prune");
var Backbone = require("backbone");
var validate = require("../module/validation");
console.log(_.isFunction(validate));
require('brace/mode/javascript');
require('brace/mode/css');
require('brace/mode/jade');
require('brace/mode/json');
require('brace/mode/html');
require('brace/theme/merbivore');*/

//console.log(validate);

    //extend Jquery with some useful functions...
    $.fn.scrollView = function () {
        return this.each(function () {
            $('html, body').animate({
                scrollTop: $(this).offset().top
            }, 300);
        });
    };

    // For todays date;
    Date.prototype.today = function () {
        return ((this.getDate() < 10) ? "0" : "") + this.getDate() + "/" + (((this.getMonth() + 1) < 10) ? "0" : "") + (this.getMonth() + 1) + "/" + this.getFullYear();
    };

    // For the time now
    Date.prototype.timeNow = function () {
        return ((this.getHours() < 10) ? "0" : "") + this.getHours() + ":" + ((this.getMinutes() < 10) ? "0" : "") + this.getMinutes() + ":" + ((this.getSeconds() < 10) ? "0" : "") + this.getSeconds() + ":" + ((this.getMilliseconds() < 10) ? "0" : "") + this.getMilliseconds();
    };

/*
    $.fn.loaderProgIn = function (callback) {
        var classes = ($(this).attr("class"));
        if (!classes) {
            classes = "";
        } else {
            classes = classes.split(" ");
            classes = _.filter(classes, function (i) {
                return /^g-*//*.exec(i);
            });
            classes = classes.join(" ");
        }
        $(this).wrap("<div class=\"l-h\" style=\"position:relative;\">");
        $(this).parent().addClass(classes);
        $(this).removeClass(classes);
        $(this).addClass("l-max");
        $(this).before("<div class=\"l\"><div class=\"l-box\"><div class=\"l-obj1\"></div><div class=\"l-obj2\"></div><div class=\"l-obj3\"></div></div></div>");
        if ($(this).hasClass("b")) {
            $(this).prev().addClass('b');
        }
        $(this).closest('.l-h').find(".l").css("display", "block").animate({
            opacity: 1
        }, 100, function () {
            if (_.isFunction(callback)) {
                callback();
            }
        });
    };

    $.fn.loaderProgOut = function (callback) {
        var target = this;
        $(this).closest('.l-h').children(".l").animate({
            opacity: 0
        }, 300, function () {
            var classes = ($(this).closest('.l-h').attr("class"));
            if (!classes) {
                classes = "";
            } else {
                classes = classes.split(" ");
                classes = _.filter(classes, function (i) {
                    return /^g-*//*.exec(i);
                });
                classes = classes.join(" ");
            }
            $(target).siblings().remove();
            $(target).unwrap();
            $(target).removeClass("l-max");
            $(target).addClass(classes);
            if (_.isFunction(callback)) {
                callback();
            }
        });
    };
*/
    $.fn.slickOut = function (callback, customEnd) {
        $(this).animate({
            opacity: 0
        }, 100, function () {
            $(this).animate({
                height: 0
                , width: 0
            }, 300, function () {
                if (!customEnd) {
                    $(this).remove();
                }
                if (_.isFunction(callback)) {
                    callback();
                }
            });
        });
    };

    $.fn.slickIn = function (callback) {};

    $(window).resize(function () {
        //log('here');
        $(".blocker").css({
            "height": $(document).height()
        });
    });

    $(document).ready(function () {

        // ad hoc functions
        $(window).on('beforeunload', function () {
            return 'Are you sure you want to leave?';
        });

        $("#console>.scrollBox").css("height", $(window.top).height() - 140);

        var log = function (input) {
            console.log(input);
            var type = "[" + typeof input + "] ";
            var target = $("#console").find('.scrollBox');
            var timeStamp = new Date().timeNow() + " : ";
            //var caller = arguments.callee.caller;
            //console.log(caller);
            if (!input) {
                input = "Empty log request?";
            }
            if (typeof input === "object") {
                var json = JSON.prune(input, {
                    prunedString: undefined
                });
                input = "\n" + JSON.stringify(JSON.parse(json), null, 4);
            }
            input = timeStamp + type + input;
            if (target.text().length) {
                input = target.text() + "\n" + input;
            }
            var maxLength = 10000;
            if (input.length > maxLength) {
                var start = input.length - maxLength;
                input = input.substring(start);
            }
            target.text(input);
            var height = target[0].scrollHeight;
            target.scrollTop(height);
        };

        log("Console started: " + new Date().today() + "\n");

        var block = {
            hide: function () {
                $(".blocker").css({
                    "opacity": "0"
                    , "display": "none"
                });
            }
            , show: function () {
                $(".blocker").css({
                    "opacity": "0.8"
                    , "display": "block"
                    , "height": $(document).height()
                });
                //log($(document).height()+" "+$("body").height());

            }
        };
        /* accordian */
        $(".ux-accordianLabel").click(function (event) {
            event.preventDefault();
            if ($(this).hasClass('ux-accordianSelected')) {
                $(this).closest(".accordianHolder").find(".ux-accordianSelected").next().animate({
                    opacity: 0
                }, 300, function () {
                    $(this).css({
                        "display": "none"
                    });
                    $(this).closest(".accordianHolder").find(".ux-accordianSelected").removeClass("ux-accordianSelected");
                });
            } else {
                $(this).closest(".accordianHolder").find(".ux-accordianSelected").removeClass("ux-accordianSelected").next().css({
                    "opacity": 0
                    , "display": "none"
                });
                $(this).addClass("ux-accordianSelected").next().css({
                    "display": "block"
                }).animate({
                    opacity: 1
                }, 300, function () {});

            }
        });

        /* revealbox */

        var dynamicID = {
            start: 0
            , letter: function () {
                this.start++;
                return "dynamicId" + this.start;
            }
            , number: function () {
                this.start++;
                return this.start;
            }
        };

        var isJSON = function (jsonString) {
            try {
                var o = JSON.parse(jsonString);
                if (o && typeof o === "object") {
                    return o;
                }
            } catch (e) {}
            return false;
        };

        $(".dropdownLabel").click(function (event) {
            event.preventDefault();
            //log(this.caller);
            var target = $(this).closest(".dropdown");
            $(".blocker").off('click.dropDown');
            if ($(this).hasClass('dropdownDown')) {
                target.css({
                    "z-index": "auto"
                });
                target.find(".dropdownContent").css({
                    "display": "none"
                });
                if (!target.hasClass("dropdown-NoBlock")) {
                    block.hide();
                }
                $(this).removeClass("dropdownDown");
            } else {
                $(this).addClass("dropdownDown");
                target.css({
                    "z-index": "9998"
                });
                target.find(".dropdownContent").css({
                    "display": "block"
                });
                if (!target.hasClass("dropdown-NoBlock")) {
                    block.show();
                }
                $(".blocker").on("click.dropDown", function () {
                    target.find(".dropdownLabel").trigger('click');
                });
            }
        });

        $(".wordSelectorButton").click(function (event) {
            event.preventDefault();
            var classNameHolder = $(this).closest(".wordSelector").find(".wordSelectorInput");
            var className = classNameHolder.val();
            var target = $(this);
            validate($(this).closest(".wordSelector"), null, function (validates) {
                if (validates) {
                    target.closest(".wordSelector").find(".wordSelectorList").prepend("<div class=\"b g-6-6 g-6-18 flex\"><div class=\"g-4-6 ux-details bf-r v-1\">" + className + "</div><button class=\"g-2-6 i button remove v-1 ux-h wordSelectorRemove\">Remove</button></div>");
                    var wordlisttext = [];
                    _.each(target.closest(".wordSelector").find(".wordSelectorList .ux-details"), function (i) {
                        wordlisttext.push($(i).text());
                    });
                    wordlisttext.sort();
                    wordlisttext = wordlisttext.join(", ");
                    target.closest(".wordSelectorGroup").find(".wordSelectorDetails").text(wordlisttext);
                    $('.wordSelectorRemove').off("click");
                    $('.wordSelectorRemove').on("click", function (event) {
                        event.preventDefault();
                        target = $(this).parent();
                        target.animate({
                            opacity: 0
                        }, 300, function () {
                            target.remove();
                        });
                    });
                }
            });
        });

        var defined = function (obj, arr) {
            var testobj = _.clone(obj);
            arr = arr.split(".");
            var testfind = _.find(arr, function (a) {
                if (testobj[a]) {
                    testobj = testobj[a];
                    return false;
                } else {
                    return true;
                }
            });
            return _.isUndefined(testfind);
        };

        // add a blocker for use by any module, but primarily for messages and modals
        $('body').prepend("<div style=\"opacity:0;display:none\" class=\"blocker\"></div>");

        var alertBox = function (type, message, next) {
            var response = false;
            var validates = true;
            var html = "";
            //var tag = "div";
            //if (type===form)
            html += "<form style=\"opacity:0; top:100px\" class=\"alertHolder\"><div class=\"alertMiddle\"><div style=\"float:none;\" class=\"alert g-5-6\">";
            html += "<div class=\"alertTitle g-6-6 ux-no-h v-1\">" + message.title + "</div>";
            html += "<div class=\"ux alertBox g-6-6 bf-t\">";
            html += "<div class=\"g-6-6 b flex\"><div class=\"b g-6-6\"><div class=\"ux alertText\">" + message.text + "</div></div>";
            switch (type) {
            case "message":
                html += "<div class=\"b g-6-6\"><button action=\"#\" class=\"alertButton alertClose g-6-6 v-1 ux-h\">Close</button></div>";
                break;
            case "continue":
                html += "<div class=\"b g-3-6\"><button action=\"#\" class=\"alertButton alertContinue g-6-6 v-1 ux-h\">Continue</button></div>";
                html += "<div class=\"b g-3-6\"><button action=\"#\" class=\"alertButton alertClose g-6-6 v-1 ux-h\">Cancel</button></div>";
                break;
            case "boolean":
                html += "<div class=\"b g-3-6\"><button action=\"#\" class=\"alertButton alertContinue g-6-6 v-1 ux-h\">Yes</button></div>";
                html += "<div class=\"b g-3-6\"><button action=\"#\" class=\"alertButton alertClose g-6-6 v-1 ux-h\">No</button></div>";
                break;
            case "form":
                html += "<div class=\"b g-3-6\"><button action=\"#\" class=\"alertButton alertContinue g-6-6 v-1 ux-h\">Submit</button></div>";
                html += "<div class=\"b g-3-6\"><button action=\"#\" class=\"alertButton alertClose g-6-6 v-1 ux-h\">Cancel</button></div>";
                break;
            }
            html += "</div></div></div></div></form>";
            html = $(html);
            $("body").prepend(html);
            block.show();
            $(html).closest(".alertHolder").animate({
                opacity: 1
                , top: "0"
            }, 300, function () {
                $(html).find("select, textarea, input").first().focus();
            });
            $(".alertContinue").click(function (event) {
                event.preventDefault();
                response = true;
                if (type === "form") {
                    //var form = 
                    validate($(html), null, function (validated) {
                        if (validated) {
                            response = _.object(_.map($(html).find("select, textarea, input").serializeArray(), _.values));
                            validates = true;
                        } else {
                            validates = false;
                        }
                    });

                }
            });
            $(".alertButton").click(function (event) {
                event.preventDefault();
                if (validates || $(this).hasClass('alertClose')) {
                    if ($(this).hasClass('alertClose')) {
                        response = false;
                    }
                    var target = $(this).closest(".alertHolder");
                    target.animate({
                        opacity: 0
                        , top: "-100"
                    }, 300, function () {
                        block.hide();
                        target.remove();
                        if (_.isFunction(next)) {
                            next(response);
                        }
                    });
                }
            });
        };

        var bodywizardType = function () {
            $(".bodyWizardType").off('change');
            $(".bodyWizardType").change(function () {
                $(this).closest(".bodyWizardGroup").find("[class*='hd-'], .hd").css({
                    "display": "none"
                    , "opacity": 0
                });
                switch ($(this).val()) {
                case "Module":
                    $(this).closest(".bodyWizardGroup").find(".hd-1").css({
                        "display": "flex"
                        , "opacity": 1
                    });
                    break;
                case "Content":
                    $(this).closest(".bodyWizardGroup").find(".hd-2").css({
                        "display": "flex"
                        , "opacity": 1
                    });
                    break;
                case "Nest":
                    $(this).closest(".bodyWizardGroup").find(".hd-3").css({
                        "display": "flex"
                        , "opacity": 1
                    });
                    break;
                }
            });
        };
        bodywizardType();

        $("#testButton").click(function () {
            alertBox("message", {
                title: "Test"
                , text: "some test text <br><br><br><br><br><br>dfsfsdf"
            }, function (response) {
                alertBox("boolean", {
                    title: "Confirm Action"
                    , text: "Are you sure?"
                }, function (response) {
                    if (response) {
                        alert(response);
                    }
                });
            });
        });
        // css set ups
        // add div for Checkbox hack
        $('input[type=checkbox]').after('<div class=\"checkbg\"></div><div class=\"checkimg\"></div>');

        var editor = ace.edit("editor");
        editor.getSession().setTabSize(4);
        editor.setTheme("ace/theme/merbivore");
        editor.setShowPrintMargin(false);
        editor.$blockScrolling = Infinity;

        //globals for backbone

        /*var dataModel = Backbone.Model.extend({
            initialize : function(){
                log('efdf');
            }
        }, {nestedSave : function(){
                log('dfdfd');
            }
        });*/
        //collection to hold all the different documents
        var dataCollection = Backbone.Collection.extend({
            comparator: function (item) {
                    return item.get('name').toLowerCase();
                }
                //, model : dataModel
        });
        var currentModel = Backbone.Model.extend({});
        // model to hold the current data
        var currentID = new currentModel({
            currentID: null
            , currentTab: "#selector"
            , currentDocId: null
        });

        var mongoModel = Backbone.Model.extend({
            idAttribute: "_id"
        });

        var sessionData = Backbone.Model.extend({
            defaults: {
                "query": null
                , "tab": null
                , "cursorPosition": null
                , "undoManager": null
            }
            , idAttribute: "_id"
        });

        var sessionCollection = Backbone.Collection.extend({
            model: sessionData
        });

        var sessionColl = new sessionCollection();

        var mongoCollection = Backbone.Collection.extend({
            model: mongoModel
            , comparator: function (item) {
                return item.get('name').toLowerCase();
            }
        });

        $("#document").loaderProgIn();
        var selectorCount = {
            module: false
            , page: false
            , template: false
        };

        var selectorCheck = function (selectorCount) {
            if (selectorCount.module && selectorCount.page && selectorCount.template && selectorCount.css) {
                $("#document").loaderProgOut();
            }
        };

        var moduleMap = new mongoCollection();
        moduleMap.url = "/api/documentlist/module";
        moduleMap.fetch({
            success: function (data) {
                selectorCount.module = true;
                selectorCheck(selectorCount);
            }
            , error: function (error) {
                log(error);
            }
        });

        var pageMap = new mongoCollection();
        pageMap.url = "/api/documentlist/page";
        pageMap.fetch({
            success: function (data) {
                selectorCount.page = true;
                selectorCheck(selectorCount);
            }
            , error: function (error) {
                log(error);
            }
        });

        var templateMap = new mongoCollection();
        templateMap.url = "/api/documentlist/template";
        templateMap.fetch({
            success: function (data) {
                selectorCount.template = true;
                selectorCheck(selectorCount);
            }
            , error: function (error) {
                log(error);
            }
        });

        var cssMap = new mongoCollection();
        cssMap.url = "/api/documentlist/media";
        cssMap.fetch({
            success: function (data) {
                selectorCount.css = true;
                selectorCheck(selectorCount);
            }
            , error: function (error) {
                log(error);
            }
        });

        var selectors = Backbone.View.extend({
            initialize: function () {
                var tempList = new selectorsTemp();
                this.listenTo(moduleMap, "update", function (data) {
                    this.changeType();
                });
                this.listenTo(pageMap, "update", function (data) {
                    this.changeType();
                });
                this.listenTo(templateMap, "update", function (data) {
                    this.changeType();
                    tempList.render();
                });
            }
            , el: "#collection"
            , events: {
                'change': 'changeType'
            }
            , changeType: function () {
                var documents = new selectorsDocument();
                var data;
                switch (this.$el.val()) {
                case "page":
                    data = pageMap;
                    break;
                case "module":
                    data = moduleMap;
                    break;
                case "template":
                    data = templateMap;
                    break;
                default:
                    data = false;
                    break;
                }
                documents.render(data);
            }
        });

        var selectorsTemp = Backbone.View.extend({
            el: ".templateList"
            , initialize: function () {
                this.listenTo(templateMap, "update", function (data) {
                    this.render();
                });
            }
            , render: function () {
                var list = new selectList();
                list.setElement(this.el);
                list.render(templateMap);
            }
        });

        var selectorsDocument = Backbone.View.extend({
            el: "#document"
            , render: function (data) {
                var list = new selectList();
                list.setElement(this.el);
                list.render(data);
            }
        });

        var selectList = Backbone.View.extend({
            template: _.template("<option value=\"<%= _id %>\"><%= name %></option>")
            , render: function (data) {
                var target = this;
                target.$el.html("<option value=\"\" disabled=\"disabled\" selected=\"selected\">-- no selection --</option>");
                if (data) {
                    data.each(function (i) {
                        target.$el.append(target.template(i.attributes));
                    });
                }
                return this;
            }
        });

        var moduleView = Backbone.View.extend({
            el: "#modulelist"
            , initialize: function () {
                this.listenToOnce(moduleMap, "sync", function (data) {
                    this.render();
                    this.listenTo(moduleMap, "update", function (data) {
                        this.render();
                    });
                });
            }
            , template: _.template("<label class=\"h g-6-18 g-3-6 b\"><div class=\"g-3-6 ux-no-h v-1\"><%= name %>:</div><div class=\"g-3-6 v-1 h bf-l\"><input class=\"bf-l module\" type=\"checkbox\" name=\"<%= name %>\" value=\"<%= _id %>\"><div class=\"checkbg\"></div><div class=\"checkimg\"></div></div></label>")
            , render: function () {
                var target = this;
                target.$el.html("");
                moduleMap.each(function (i) {
                    target.$el.append(target.template(i.attributes));
                });
                return this;
            }
        });

        // collection to hold all the different open models
        var coll = new dataCollection();

        var editorDisable = Backbone.View.extend({
            initialize: function () {

            }
            , render: function (tab) {
                if (
                    tab === "#js" ||
                    tab === "#jsonload" ||
                    tab === "#reqs" ||
                    tab === "#cssEditor"
                ) {
                    this.$el.prop("disabled", false);
                } else if (
                    tab === "#modules" ||
                    tab === "#info" ||
                    tab === "#body" ||
                    tab === "#pug" ||
                    tab === "#console" ||
                    tab === "#backup" ||
                    tab === "#mongodb" ||
                    tab === "#selector" ||
                    tab === "#selected" ||
                    tab === "#history" ||
                    tab === "#head"
                ) {
                    this.$el.prop("disabled", true);
                }
            }
        });

        var scriptsTabs = Backbone.View.extend({
            //el: "#browser"
            initialize: function (options) {
                this.element = options.substring(1);
                //log(this.element);
                this.setElement(options);
                this.listenTo(currentID, "change:currentID", function (dataBackbone) {
                    if (dataBackbone.get("currentID") !== null) {
                        dataBackbone = coll.get(dataBackbone.get("currentID"));
                        this.render(dataBackbone);
                    } else {
                        this.$el.find('.t-a').parent().remove();
                    }
                });
            }
            , render: function (data) {
                var self = this;
                self.$el.find('.t-a').parent().remove();
                var dataJSON = data.toJSON();
                try {
                    dataJSON = dataJSON.data.resources.scripts[this.element];
                    if (dataJSON && dataJSON.length > 0) {
                        dataJSON.sort(function (a, b) {
                            var nameA = a.name.toLowerCase()
                                , nameB = b.name.toLowerCase();
                            if (nameA > nameB)
                                return -1;
                            if (nameA < nameB)
                                return 1;
                            return 0;
                        });
                    }
                } catch (err) {
                    log(err);
                }
                _.each(dataJSON, function (i) {
                    self.$el.find(".t-h").prepend("<div class=\"b\"><li class=\"t-autoWidth t-a v-1\" data-docId=\"" + i._id + "\" href=\"#" + self.element + "Scripts\">" + i.name + "</li></div>");
                });
                _.each(self.$el.find(".t-a"), function (i) {
                    var tab = new tabView();
                    tab.setElement(i);
                    /*var scriptTab = new browserScripts();
                    scriptTab.setElement(i);*/
                });
                this.$el.find(".t-a").first().addClass('t-s');
                if (currentID.get("currentTab") === "#" + this.element || currentID.get("currentTab") === "#" + this.element + "Scripts") {
                    //log('here');

                    if (this.$el.find(".t-a").length > 0) {
                        //log('here length < 0');
                        this.$el.find(".t-a").first().trigger('click');
                    } else {
                        //log('here === 0');
                        $("[href='#"+self.element+"']").trigger('click');
                    }
                }
            }
        });

        var scriptsApp = new scriptsTabs("#browser");
        var requireApp = new scriptsTabs("#require");
        var serverApp = new scriptsTabs("#server");

        var addScript = Backbone.View.extend({
            initialize: function (option) {
                    this.element = option;
                    this.setElement("#" + this.element + "New");
                }
                //, el: "#browserNew"
            , events: {
                "click": "clicked"
            }
            , clicked: function () {
                var self = this;
                if (currentID.get("currentID") !== null) {
                    alertBox("form", {
                        title: "Choose Name"
                        , text: "<label class=\"h gl-6-18 gl-1-6 g-6-18 g-4-6 b\"><div class=\"g-2-6 ux-no-h v-1\">Name: </div><div class=\"g-4-6\"><input class=\"ux-text g-6-6 bf-l v-1\" id=\"name\" placeholder=\"Name\" name=\"name\" type=\"text\" data-validate=\"compulsary leadWithLetter\"></div></label>"
                    }, function (response) {
                        if (response) {
                            self.render(response.name);
                        }
                    });

                } else {
                    log("no document loaded");
                }
            }
            , render: function (name) {
                var self = this;

                var id = currentID.get("currentID");
                if (id) {
                    var data = coll.get(id);
                    var jsonModel = data.toJSON();
                    if (!jsonModel.data.resources.scripts[self.element]) {
                        jsonModel.data.resources.scripts[self.element] = [];
                    }
                    jsonModel = jsonModel.data.resources.scripts[self.element];
                    jsonModel.push({
                        "name": name
                        , "_id": undefined
                    });
                    data.sync("update", data, {
                        success: function (model, response, options) {
                            _.extend(_.find(jsonModel, {
                                "_id": undefined
                            }), model);
                            self.$el.closest(".t-h").find(".t-s").removeClass("t-s");
                            //log(self.element);
                            self.$el.closest(".t-h").prepend("<div class=\"b\"><li class=\"t-autoWidth t-a t-s v-1\" data-docId=\"" + model._id + "\" href=\"#" + self.element + "Scripts\">" + model.name + "</li></div>");
                            var tab = new tabView();
                            tab.setElement(self.$el.closest(".t-h").find(".t-s"));
                            self.$el.closest(".t-h").find(".t-s").trigger('click');

                            /*var scriptTab = new browserScripts();
                            scriptTab.setElement(self.$el.closest(".t-h").find(".t-s"));*/

                            target.loaderProgOut();
                        }
                        , error: function (error) {
                            alertBox("message", {
                                title: "Error"
                                , text: error.status + " " + error.statusText
                            });
                            target.loaderProgOut();
                        }
                        , section: "section"
                        , tab: "#" + self.element + "Scripts"
                        , query: undefined
                    });
                }
            }
        });

        /*var browserScripts = Backbone.View.extend({
            initialize : function(){
                var id = currentID.get("currentID");
                var model = coll.get(id);
                log(model);
                this.listenTo(model, "change", function (data) {
                        log('line787');
                        this.render(data);
                })
            },
            render : function(data){
                log("line 788");
                log(data.attributes);
            }
        })*/

        var addScriptApp = new addScript("browser");
        var addRequireApp = new addScript("require");
        var addServerApp = new addScript("server");


        var editorUndo = Backbone.View.extend({
            initialize: function (editor) {
                this.editor = editor;
            }
            , el: "#undo"
            , events: {
                "click": "clicked"
            }
            , clicked: function (e) {
                e.preventDefault();
                this.editor.undo(true);
            }
        });

        var editorRedo = Backbone.View.extend({
            initialize: function (editor) {
                this.editor = editor;
            }
            , el: "#redo"
            , events: {
                "click": "clicked"
            }
            , clicked: function (e) {
                e.preventDefault();
                this.editor.redo(true);
            }
        });

        var editorToolsUp = Backbone.View.extend({
            initialize: function (editor) {
                this.editor = editor;
            }
            , el: ".tools-up"
            , events: {
                "click": "clicked"
            }
            , clicked: function (e) {
                e.preventDefault();
                var pos = this.editor.getCursorPosition();
                pos.row--;
                if (this.$el.closest(".dropdownInnerContent").find(".tools-select").parent().css("display") !== "none") {
                    this.editor.navigateTo(pos.row, pos.column);
                } else if (this.$el.closest(".dropdownInnerContent").find(".tools-copy").parent().css("display") !== "none") {
                    //log('fddsf');
                    this.editor.moveCursorToPosition(pos);
                }
            }
        });

        var editorToolsDown = Backbone.View.extend({
            initialize: function (editor) {
                this.editor = editor;
            }
            , el: ".tools-down"
            , events: {
                "click": "clicked"
            }
            , clicked: function (e) {
                e.preventDefault();
                var pos = this.editor.getCursorPosition();
                pos.row++;
                if (this.$el.closest(".dropdownInnerContent").find(".tools-select").parent().css("display") !== "none") {
                    this.editor.navigateTo(pos.row, pos.column);
                } else if (this.$el.closest(".dropdownInnerContent").find(".tools-copy").parent().css("display") !== "none") {
                    this.editor.moveCursorToPosition(pos);
                }
            }
        });

        var editorToolsLeft = Backbone.View.extend({
            initialize: function (editor) {
                this.editor = editor;
            }
            , el: ".tools-left"
            , events: {
                "click": "clicked"
            }
            , clicked: function (e) {
                e.preventDefault();
                var pos = this.editor.getCursorPosition();
                pos.column--;
                if (this.$el.closest(".dropdownInnerContent").find(".tools-select").parent().css("display") !== "none") {
                    this.editor.navigateTo(pos.row, pos.column);
                } else if (this.$el.closest(".dropdownInnerContent").find(".tools-copy").parent().css("display") !== "none") {
                    this.editor.moveCursorToPosition(pos);
                }
            }
        });

        var editorToolsRight = Backbone.View.extend({
            initialize: function (editor) {
                this.editor = editor;
            }
            , el: ".tools-right"
            , events: {
                "click": "clicked"
            }
            , clicked: function (e) {
                e.preventDefault();
                var pos = this.editor.getCursorPosition();
                pos.column++;
                if (this.$el.closest(".dropdownInnerContent").find(".tools-select").parent().css("display") !== "none") {
                    this.editor.navigateTo(pos.row, pos.column);
                } else if (this.$el.closest(".dropdownInnerContent").find(".tools-copy").parent().css("display") !== "none") {
                    this.editor.moveCursorToPosition(pos);
                }
            }
        });

        var editorToolsTab = Backbone.View.extend({
            initialize: function (editor) {
                this.editor = editor;
            }
            , el: ".tools-tab"
            , events: {
                "click": "clicked"
            }
            , clicked: function (e) {
                e.preventDefault();
                editor.indent();
            }
        });

        var editorToolsTabRemove = Backbone.View.extend({
            initialize: function (editor) {
                this.editor = editor;
            }
            , el: ".tools-tabRemove"
            , events: {
                "click": "clicked"
            }
            , clicked: function (e) {
                e.preventDefault();
                editor.blockOutdent();
            }
        });


        var editorToolsComment = Backbone.View.extend({
            initialize: function (editor) {
                this.editor = editor;
            }
            , el: ".tools-comment"
            , events: {
                "click": "clicked"
            }
            , clicked: function (e) {
                e.preventDefault();
                var text = this.editor.getCopyText();
                this.editor.insert("/* " + text + " */");
            }
        });

        var editorToolsClear = Backbone.View.extend({
            initialize: function (editor) {
                this.editor = editor;
            }
            , el: ".tools-clear"
            , events: {
                "click": "clicked"
            }
            , clicked: function (e) {
                e.preventDefault();
                this.editor.insert("");
            }
        });

        var editorToolsSelect = Backbone.View.extend({
            initialize: function (editor) {
                this.editor = editor;
            }
            , el: ".tools-select"
            , events: {
                "click": "clicked"
            }
            , clicked: function (e) {
                e.preventDefault();
                this.$el.parent().addClass("hd");
                this.$el.closest(".dropdownInnerContent").find(".tools-copy").parent().removeClass("hd");
            }
        });

        //var clipboard = "";

        var editorClipboard = Backbone.View.extend({
            initialize: function (editor) {
                this.editor = editor;
                this.clipboard = "";
                var editorToolsCopyApp = new editorToolsCopy(this.editor, this.clipboard);
                var editorToolsPasteApp = new editorToolsPaste(this.editor, this.clipboard);


            }
        });

        var editorToolsCopy = Backbone.View.extend({
            initialize: function (editor, clipboard) {
                this.editor = editor;
                this.clipboard = clipboard;
            }
            , el: ".tools-copy"
            , events: {
                "click": "clicked"
            }
            , clicked: function (e) {
                e.preventDefault();
                clipboard = editor.getCopyText();
                this.$el.parent().addClass("hd");
                this.$el.closest(".dropdownInnerContent").find(".tools-select").parent().removeClass("hd");
            }
        });

        var editorToolsPaste = Backbone.View.extend({
            initialize: function (editor, clipboard) {
                this.editor = editor;
                this.clipboard = clipboard;
            }
            , el: ".tools-paste"
            , events: {
                "click": "clicked"
            }
            , clicked: function (e) {
                e.preventDefault();
                editor.insert(clipboard);
            }
        });

        var editorGoto = Backbone.View.extend({
            //el: "#goto"
            initialize: function (editor) {
                this.editor = editor;
            }
            , events: {
                "click": "clicked"
            }
            , clicked: function (e) {
                e.preventDefault();
                var self = this;
                validate(self.$el.closest('form'), null, function (validates) {
                    if (validates) {
                        var input = self.$el.closest('form').find("input").val();
                        self.editor.gotoLine(input);
                    }
                });
            }
        });

        var editorFind = Backbone.View.extend({
            el: "#find"
            , initialize: function (editor) {
                this.editor = editor;
            }
            , events: {
                "click": "clicked"
            }
            , clicked: function (e) {
                e.preventDefault();
                var self = this;
                validate(self.$el.closest('form'), null, function (validates) {
                    if (validates) {
                        var needle = self.$el.closest('form').find("input").val();
                        self.editor.findNext({
                            needle: needle
                            , wrap: true
                            , caseSensitive: true
                        , }, true);
                    }
                });
            }
        });

        var editorPrev = Backbone.View.extend({
            el: "#prev"
            , initialize: function (editor) {
                this.editor = editor;
            }
            , events: {
                "click": "clicked"
            }
            , clicked: function (e) {
                e.preventDefault();
                var self = this;
                validate(self.$el.closest('form'), null, function (validates) {
                    if (validates) {
                        var needle = self.$el.closest('form').find("input").val();
                        self.editor.findPrevious({
                            needle: needle
                            , wrap: true
                            , caseSensitive: true
                        , }, true);
                    }
                });
            }
        });

        var editorReplace = Backbone.View.extend({
            el: "#replace"
            , initialize: function (editor) {
                this.editor = editor;
            }
            , events: {
                "click": "clicked"
            }
            , clicked: function (e) {
                e.preventDefault();
                var self = this;
                var prev = self.$el.closest('form').prev();
                validate(self.$el.closest('form'), null, function (validateFind) {
                    validate(prev, null, function (validateReplace) {
                        if (validateFind && validateReplace) {
                            var needle = prev.find("input").val();
                            var newText = self.$el.closest('form').find("input").val();
                            self.editor.replace(newText, {
                                needle: needle
                                , wrap: true
                                , caseSensitive: true
                            });
                        }
                    });
                });
            }
        });

        var editorReplaceAll = Backbone.View.extend({
            el: "#replaceAll"
            , initialize: function (editor) {
                this.editor = editor;
            }
            , events: {
                "click": "clicked"
            }
            , clicked: function (e) {
                e.preventDefault();
                var self = this;
                var prev = self.$el.closest('form').prev();
                validate(self.$el.closest('form'), null, function (validateFind) {
                    validate(prev, null, function (validateReplace) {
                        if (validateFind && validateReplace) {
                            var needle = prev.find("input").val();
                            var newText = self.$el.closest('form').find("input").val();
                            var replaced = self.editor.replaceAll(newText, {
                                needle: needle
                                , wrap: true
                                , caseSensitive: true
                            });
                            alertBox("message", {
                                title: "Replace All Count"
                                , text: replaced + " instances of \"" + needle + "\" replaced with \"" + newText + "\""
                            });
                        }
                    });
                });
            }
        });

        var editorAditionalDelete = Backbone.View.extend({
            el: "#deleteSub"
            , initialize: function () {
                this.listenTo(currentID, "change", function (data) {
                    if (data.get("currentID") !== null) {
                        this.render();
                    }
                });
            }
            , events: {
                "click": "clicked"
            }
            , clicked: function (e) {
                var self = this;
                var doc = currentID.get("currentID");
                var id = currentID.get("currentDocId");
                e.preventDefault();
                self.$el.loaderProgIn();
                alertBox("boolean", {
                    title: "Delete"
                    , text: "Delete sub document?<br/>Doc ID: " + id
                }, function (response) {
                    if (response) {
                        var doc = currentID.get("currentID");
                        var id = currentID.get("currentDocId");
                        var tab = currentID.get("currentTab");
                        var sub;
                        switch (tab.substr(1)) {
                        case "browserScripts":
                            sub = "browser";
                            break;
                        case "requireScripts":
                            sub = "require";
                            break;
                        case "serverScripts":
                            sub = "server";
                            break;
                        }
                        var url = "/api/" + $('#selectedCollection').text() + "/" + doc + "/sub/" + tab.substr(1);
                        var subModelToDeleteModel = Backbone.Model.extend({
                            urlRoot: url
                        });
                        var subModelToDelete = new subModelToDeleteModel({
                            id: id
                        });
                        subModelToDelete.sync("delete", subModelToDelete, {
                            success: function (result) {
                                model = coll.get(result.id);
                                model = model.toJSON();
                                cloned = _.clone(model.data.resources.scripts[sub]);
                                var match = _.find(cloned, {
                                    _id: result.subID
                                });
                                if (match) {
                                    model.data.resources.scripts[sub] = _.without(cloned, match);
                                }
                                var target = $("[href='"+tab+"'][data-docid='" + result.subID + "']").parent();
                                var newTarget = $("[href='#"+sub+"']")
                                if (target.prev().find("li").hasClass('t-a')) {
                                    if (target.find('li').hasClass('t-s')) {
                                        newTarget = target.prev().find("li");
                                    }
                                } else if (target.next().find("li").hasClass('t-a')) {
                                    if (target.find('li').hasClass('t-s')) {
                                        newTarget = target.next().find("li");
                                    }
                                } 
                                
                                target.slickOut(function(){
                                    newTarget.trigger('click');
                                });
                                
                                self.$el.loaderProgOut();
                            }
                            , error: function (error) {
                                self.$el.loaderProgOut();
                                alertBox("message", {
                                    title: "Error"
                                    , text: error.status + " " + error.statusText
                                }, function () {
                                    self.$el.loaderProgOut();
                                });
                            }
                        });
                    } else {
                        self.$el.loaderProgOut();
                    }
                });
            }
            , render: function () {
                var tab = currentID.get('currentTab');
                if (
                    tab === "#browserScripts" || tab === "#requireScripts" || tab === "#serverScripts"
                ) {
                    this.$el.prop("disabled", false);
                } else {
                    this.$el.prop("disabled", true);
                }
            }
        });

        var editorBeautify = Backbone.View.extend({
            el: "#beautify"
            , initialize: function (editor) {
                this.editor = editor;
                this.listenTo(currentID, "change", function (data) {
                    if (data.get("currentID") !== null) {
                        this.render();
                    }
                });
            }
            , events: {
                "click": "clicked"
            }
            , clicked: function (e) {
                e.preventDefault();
                var target = this;
                var tab = currentID.get('currentTab');
                var type = "js";
                if (tab === "#cssEditor") {
                    type = "css";
                }
                target.$el.loaderProgIn();
                $.ajax({
                    method: "POST"
                    , url: "/beautify/" + type
                    , data: {
                        text: target.editor.getValue()
                    }
                    , success: function (result) {
                        target.editor.setValue(result);
                        target.$el.loaderProgOut();
                    }
                    , error: function (error) {
                        target.$el.loaderProgOut();
                        alertBox("message", {
                            title: "Error"
                            , text: error.status + " " + error.statusText
                        });
                    }
                });
            }
            , render: function () {
                var tab = currentID.get('currentTab');
                //log(tab);
                if (
                    tab === "#requireScripts" ||
                    tab === "#serverScripts" ||
                    tab === "#browserScripts" ||
                    tab === "#jsonload" ||
                    tab === "#reqs" ||
                    tab === "#js" ||
                    tab === "#cssEditor"
                ) {
                    this.$el.prop("disabled", false);
                } else if (
                    tab === "#modules" ||
                    tab === "#info" ||
                    tab === "#body" ||
                    tab === "#pug" ||
                    tab === "#console" ||
                    tab === "#backup" ||
                    tab === "#mongodb" ||
                    tab === "#selector" ||
                    tab === "#history" ||
                    tab === "#head"
                ) {
                    this.$el.prop("disabled", true);
                }
            }
        });

        var typeChange = Backbone.View.extend({
            el: "#type"
            , initialize: function () {

            }
            , events: {
                "change": "changed"
            }
            , changed: function () {
                this.$el.closest("#infolist").find("[class*='hd-'], .hd").css({
                    "display": "none"
                    , "opacity": 0
                });
                switch (this.$el.val()) {
                case "page":
                    this.$el.closest("#infolist").find(".hd-1").css({
                        "display": "flex"
                        , "opacity": 1
                    });
                    break;
                case "template":
                    this.$el.closest("#infolist").find(".hd-2").css({
                        "display": "flex"
                        , "opacity": 1
                    });
                    break;
                case "module":
                    this.$el.closest("#infolist").find(".hd-3").css({
                        "display": "flex"
                        , "opacity": 1
                    });
                    break;
                }

            }
        });

        var typeChangeApp = new typeChange();

        var update = function (tab, id, query) {
            //log('fff');
            if (
                tab === "#js" ||
                tab === "#pug" ||
                tab === "#reqs" ||
                tab === "#jsonload" ||
                tab === "#mongodb" ||
                tab === "#body" ||
                tab === "#head" ||
                tab === "#cssEditor" ||
                tab === "#requireScripts" ||
                tab === "#serverScripts" ||
                tab === "#browserScripts"
            ) {

                var backboneID = id + tab;
                //log(backboneID);
                if (query && tab === "#cssEditor") {
                    backboneID += query;
                } else if (!query && tab === "#cssEditor") {
                    backboneID = null;
                }
                if (query && tab === "#browserScripts") {
                    backboneID += query;
                } else if (!query && tab === "#browserScripts") {
                    backboneID = null;
                }
                if (query && tab === "#serverScripts") {
                    backboneID += query;
                } else if (!query && tab === "#serverScripts") {
                    backboneID = null;
                }
                if (query && tab === "#requireScripts") {
                    backboneID += query;
                } else if (!query && tab === "#requireScripts") {
                    backboneID = null;
                }
                //log(backboneID);
                if (backboneID) {
                    var session = sessionColl.get(backboneID);
                    if (session) {
                        //log(session.attributes);
                    }
                    var row = editor.getCursorPosition().row;
                    var col = editor.getCursorPosition().column;
                    var pos = {
                        "row": row
                        , "column": col
                    };
                    if (session) {
                        session.set("cursorPosition", pos);
                        session.set("undoManager", editor.session.getUndoManager());
                        session.set("tab", tab);
                        session.set("query", query);
                        session.set("_id", backboneID);
                        session.set("document", id);

                    } else {
                        session = new sessionData();
                        session.set("cursorPosition", pos);
                        session.set("undoManager", editor.session.getUndoManager());
                        session.set("tab", tab);
                        session.set("query", query);
                        session.set("body", null);
                        session.set("_id", backboneID);
                        session.set("document", id);
                        session = sessionColl.add(session);
                    }
                    var data = coll.get(id);

                    if (data) {
                        data = data.toJSON();
                        var self;
                        var matched;
                        switch (tab) {
                        case "#js":
                            data.data.resources.scripts.text = editor.getValue();
                            break;
                        case "#pug":
                            data.data.html[0].data[0].text = editor.getValue();
                            break;
                        case "#reqs":
                            data.data.resources.scripts.requires.text = editor.getValue();
                            break;
                        case "#jsonload":
                            data.data.resources.scripts.onload = editor.getValue();
                            break;
                        case "#mongodb":
                            data = editor.getValue();
                            break;
                        case "#body":
                            var valid = true;
                            var json = editor.getValue();
                            try {
                                // switch out for parse json test function
                                if (typeof json === "string") {
                                    json = JSON.parse(json);
                                }
                            } catch (err) {
                                valid = false;
                            }
                            if (valid && json && json[0] && json[0].data && json[0].data[0]) {
                                data.data.html = editor.getValue();
                                session.set("body", null);
                                //log('update');
                            } else {
                                try {
                                    json = editor.getValue();
                                    session.set("body", json);
                                    alertBox("message", {
                                        title: "JSON parse error"
                                        , text: "Invalid structure has not persisted to model, saved as a temporary model only."
                                    });
                                } catch (err) {
                                    alertBox("message", {
                                        title: "JSON parse error"
                                        , text: "error saving temporary copy of json."
                                    });
                                }
                            }
                            break;
                        case "#head":
                            data.data.head = editor.getValue();
                            break;
                        case "#cssEditor":
                            if (query) {
                                self = editor;
                                matched = false;
                                _.each(data.data.resources.css, function (i) {
                                    if (i.query === query) {
                                        i.text = self.getValue();
                                        matched = true;
                                    }
                                });
                                if (!matched) {
                                    data.data.resources.css.push({
                                        "query": query
                                        , "text": self.getValue()
                                    });
                                }
                            }
                            break;
                        case "#browserScripts":
                            if (query) {
                                self = editor;
                                matched = false;
                                _.each(data.data.resources.scripts.browser, function (i) {
                                    if (i._id === query) {
                                        i.text = self.getValue();
                                        matched = true;
                                        return;
                                    } else {
                                    }
                                });
                                if (!matched) {
                                }
                            }
                            break;
                        case "#serverScripts":
                            if (query) {
                                self = editor;
                                matched = false;
                                _.each(data.data.resources.scripts.server, function (i) {
                                    if (i._id === query) {
                                        i.text = self.getValue();
                                        matched = true;
                                        return;
                                    } else {
                                    }
                                });
                                if (!matched) {
                                }
                            }
                            break;
                        case "#requireScripts":
                            if (query) {
                                self = editor;
                                matched = false;
                                //log("\n"+query + " " + _.find(data.data.resources.scripts.browser, {"_id":query}));

                                _.each(data.data.resources.scripts.require, function (i) {
                                    //log(i._id);

                                    if (i._id === query) {
                                        i.text = self.getValue();
                                        matched = true;
                                        return;
                                    } else {
                                        //log(i._id +" "+query);
                                    }
                                });
                                if (!matched) {
                                    /* data.data.resources.scripts.browser.push({
                                         "_id": query
                                         , "text": self.getValue()
                                     });*/
                                }
                            }
                            break;
                        }
                    }
                }
            }
        };

        var tabChange = Backbone.View.extend({
            el: editor
            , initialize: function () {
                this.listenTo(currentID, "change", function (dataBackbone) {
                    if (dataBackbone.get("currentID") !== null) {
                        dataBackbone = coll.get(dataBackbone.get("currentID"));
                        this.render(dataBackbone);
                    }
                });
                var editorReplaceApp = new editorReplace(this.el);
                var editorReplaceAllApp = new editorReplaceAll(this.el);
                var editorUndoApp = new editorUndo(this.el);
                var editorRedoApp = new editorRedo(this.el);
                var editorToolsUpApp = new editorToolsUp(this.el);
                var editorToolsDownApp = new editorToolsDown(this.el);
                var editorToolsLeftApp = new editorToolsLeft(this.el);
                var editorToolsRightApp = new editorToolsRight(this.el);
                var editorToolsTabApp = new editorToolsTab(this.el);
                var editorToolsTabRemoveApp = new editorToolsTabRemove(this.el);
                var editorToolsSelectApp = new editorToolsSelect(this.el);
                var editorToolsClipboardApp = new editorClipboard(this.el);
                var editorToolsCommentApp = new editorToolsComment(this.el);
                var editorToolsClearApp = new editorToolsClear(this.el);
                var editorGotoAApp = new editorGoto(this.el);
                editorGotoAApp.setElement("#gotoA");
                var editorGotoBApp = new editorGoto(this.el);
                editorGotoBApp.setElement("#gotoB");
                var editorFindApp = new editorFind(this.el);
                var editorPrevApp = new editorPrev(this.el);
                var editorBeautifyApp = new editorBeautify(this.el);
                var editorAditionalDeleteApp = new editorAditionalDelete();
            }
            , render: function (dataBackbone) {
                update(currentID.previousAttributes().currentTab, currentID.previousAttributes().currentID, currentID.previousAttributes().currentDocId);
                data = dataBackbone.toJSON();
                this.el.setValue("");
                if (!data.data) {
                    data.data = {};
                }
                if (!data.data.html) {
                    data.data.html = [];
                }
                if (typeof data.data.html === "string") {
                    data.data.html = isJSON(data.data.html);
                }
                if (!data.data.html[0]) {
                    data.data.html[0] = {};
                }
                if (!data.data.html[0].data) {
                    data.data.html[0].data = [];
                }
                if (!data.data.html[0].data[0]) {
                    data.data.html[0].data[0] = {};
                }
                if (!data.data.html[0].data[0].text) {
                    data.data.html[0].data[0].text = "";
                }
                if (!data.data.head) {
                    data.data.head = {};
                }
                if (typeof data.data.head === "string") {
                    data.data.head = isJSON(data.data.head);
                }
                if (!data.data.head.html) {
                    data.data.head.html = [];
                }
                if (!data.data.head.html[0]) {
                    data.data.head.html[0] = {};
                }
                if (!data.data.head.html[0].data) {
                    data.data.head.html[0].data = [];
                }
                if (!data.data.head.html[0].data[0]) {
                    data.data.head.html[0].data[0] = {};
                }
                if (!data.data.head.html[0].data[0].text) {
                    data.data.head.html[0].data[0].text = "";
                }
                if (!data.data.resources) {
                    data.data.resources = {};
                }
                if (!data.data.resources.css) {
                    data.data.resources.css = [];
                }
                if (!data.data.resources.scripts) {
                    data.data.resources.scripts = {};
                }
                if (!data.data.resources.scripts.text) {
                    data.data.resources.scripts.text = "";
                }
                if (!data.data.resources.scripts.requires) {
                    data.data.resources.scripts.requires = {
                        text: ""
                    };
                }
                if (!data.data.resources.scripts.onload) {
                    data.data.resources.scripts.onload = "";
                }
                var sessionID = currentID.get("currentID") + "#body";
                var sessionValid = sessionColl.get(sessionID);
                var self;
                var id;
                switch (currentID.get("currentTab")) {
                case "#browserScripts":
                    self = this;
                    id = currentID.get("currentDocId");
                    _.each(data.data.resources.scripts.browser, function (i) {
                        if (i._id === id) {
                            if (!i.text) {
                                i.text = "";
                            }
                            self.el.setValue(i.text);
                            self.el.getSession().setMode("ace/mode/javascript");
                        }
                    });
                    break;
                 case "#serverScripts":
                    self = this;
                    id = currentID.get("currentDocId");
                    _.each(data.data.resources.scripts.server, function (i) {
                        if (i._id === id) {
                            if (!i.text) {
                                i.text = "";
                            }
                            self.el.setValue(i.text);
                            self.el.getSession().setMode("ace/mode/javascript");
                        }
                    });
                    break;
                case "#requireScripts":
                    self = this;
                    id = currentID.get("currentDocId");
                    _.each(data.data.resources.scripts.require, function (i) {
                        if (i._id === id) {
                            if (!i.text) {
                                i.text = "";
                            }
                            self.el.setValue(i.text);
                            self.el.getSession().setMode("ace/mode/javascript");
                        }
                    });
                    break;
                case "#cssEditor":
                    self = this;
                    id = currentID.get("currentDocId");
                    _.each(data.data.resources.css, function (i) {
                        if (i.query === id) {
                            if (!i.text) {
                                i.text = "";
                            }
                            self.el.setValue(i.text);
                            self.el.getSession().setMode("ace/mode/css");
                        }
                    });
                    break;
                case "#js":
                    this.el.setValue(data.data.resources.scripts.text);
                    this.el.getSession().setMode("ace/mode/javascript");
                    break;
                case "#pug":
                    this.el.setValue(data.data.html[0].data[0].text);
                    //log(data.data.html[0].data[0].mime);
                    this.el.getSession().setMode("ace/mode/jade");
                    break;
                case "#reqs":
                    this.el.setValue(data.data.resources.scripts.requires.text);
                    this.el.getSession().setMode("ace/mode/javascript");
                    break;
                case "#jsonload":
                    this.el.setValue(data.data.resources.scripts.onload);
                    this.el.getSession().setMode("ace/mode/javascript");
                    break;
                case "#mongodb":
                    if (typeof data === "object") {
                        this.el.setValue(JSON.stringify(data, null, 4));
                    } else {
                        this.el.setValue(data);
                    }
                    this.el.getSession().setMode("ace/mode/json");
                    break;
                case "#body":
                    var output = data.data.html;
                    if (sessionValid) {
                        output = sessionValid.get("body");
                        if (output) {
                            alertBox("message", {
                                title: "JSON parse error"
                                , text: "Invalid structure, correct before JSON will be saved."
                            });
                        } else {
                            output = data.data.html;
                        }
                    }
                    if (typeof output === "object") {
                        output = JSON.stringify(output, null, 4);
                    }
                    this.el.setValue(output);
                    this.el.getSession().setMode("ace/mode/json");
                    break;
                case "#head":
                    if (typeof data.data.head === "object") {
                        data.data.head = JSON.stringify(data.data.head, null, 4);
                    }
                    this.el.setValue(data.data.head);
                    this.el.getSession().setMode("ace/mode/json");
                    break;
                }
                editor.getSession().setUndoManager(new ace.UndoManager());
                var tab = currentID.get("currentTab");
                var undoStack = currentID.get("currentID") + tab;
                var query = currentID.get("currentDocId");
                if (query) {
                    undoStack += query;
                }
                if (
                    tab === "#js" ||
                    tab === "#pug" ||
                    tab === "#reqs" ||
                    tab === "#jsonload" ||
                    tab === "#mongodb" ||
                    tab === "#body" ||
                    tab === "#head" ||
                    tab === "#cssEditor" ||
                    tab === "#requireScripts" ||
                    tab === "#serverScripts" ||
                    tab === "#browserScripts"
                ) {
                    var undoData = sessionColl.get(undoStack);
                    if (undoData) {
                        var undo = undoData.get("undoManager");
                        var pos = undoData.get("cursorPosition");
                        this.el.session.setUndoManager(undo);
                        pos.row++;
                        this.el.gotoLine(pos.row, pos.column, false);
                        this.el.focus();
                    } else {
                        this.el.gotoLine(0, 0, false);
                        this.el.focus();
                    }
                }
            }
        });

        var modulesView = Backbone.View.extend({
            el: "#modules"
            , initialize: function () {
                this.listenTo(currentID, "change", function (data) {
                    if (data.get("currentID") !== null) {
                        data = coll.get(data.get("currentID"));
                        this.render(data);
                    }
                });
            }
            , render: function (data) {
                var target = this.$el;
                this.$el.find(".module").prop("checked", false);
                if (data.get("modules")) {
                    _.each(data.get("modules"), function (i) {
                        target.find("input:checkbox[value=\"" + i + "\"]").prop("checked", true);
                    });
                }
            }
            , events: {
                'change input': 'change'
            }
            , change: function () {
                var model = coll.get(currentID.get('currentID'));
                if (model) {
                    var modOutput = [];
                    this.$el.find(".module").each(function () {
                        if ($(this).is(":checked")) {
                            modOutput.push($(this).val());
                        }
                    });

                    model.set({
                        modules: modOutput
                    });
                }
            }
        });

        var documentInfoView = Backbone.View.extend({
            initialize: function () {
                var documentInfoNameViewRun = new documentInfoViews("name");
                var documentInfoNotesViewRun = new documentInfoViews("notes");
                var documentInfoRobotsViewRun = new documentInfoViews("robots");
                var documentInfoActiveViewRun = new documentInfoViews("active");
                var documentInfoSecureViewRun = new documentInfoViews("secure");
                var documentInfoTemplateViewRun = new documentInfoViews("template");
                var documentInfoAccesslevelViewRun = new documentInfoViews("accesslevel");
                var documentInfoFreqViewRun = new documentInfoViews("freq");
                var documentInfoPrioritylateViewRun = new documentInfoViews("priority");
                var documentInfoTypeViewRun = new documentInfoViews("type");
            }
        });

        var documentInfoViews = Backbone.View.extend({
            initialize: function (attr) {
                this.attr = attr;
                this.setElement("#" + this.attr);
                this.listenTo(currentID, "change", function (data) {
                    if (data.get("currentID") !== null) {
                        data = coll.get(data.get("currentID"));
                        this.render(data);
                    }
                });
            }
            , events: {
                "keyup": "keyup"
                , "change": "change"
            }
            , render: function (data) {
                var inputType = null;
                if (this.$el.is("input:text") || this.$el.is('textarea')) {
                    inputType = "text";
                }
                if (this.$el.is("input:checkbox")) {
                    inputType = "checkbox";
                }
                if (this.$el.is("select")) {
                    inputType = "select";
                }
                if (data.get(this.attr)) {
                    switch (inputType) {
                    case "text":
                    case "select":
                        this.$el.val(data.get(this.attr));
                        break;
                    case "checkbox":
                        this.$el.prop("checked", data.get(this.attr));
                        break;
                    }
                } else {
                    switch (inputType) {
                    case "text":
                    case "select":
                        this.$el.val("");
                        break;
                    case "checkbox":
                        this.$el.prop("checked", false);
                        break;
                    }
                }
                if (this.attr === "type") {
                    this.$el.trigger("change");
                }
            }
            , keyup: function () {
                var id = currentID.get("currentID");
                if (id) {
                    data = coll.get(id);
                    data.set(this.attr, this.$el.val());
                }
            }
            , change: function () {
                var id = currentID.get("currentID");
                if (id) {
                    data = coll.get(id);
                    var inputType = null;
                    if (this.$el.is("input:text") || this.$el.is('textarea')) {
                        inputType = "text";
                    }
                    if (this.$el.is("input:checkbox")) {
                        inputType = "checkbox";
                    }
                    if (this.$el.is("select")) {
                        inputType = "select";
                    }
                    switch (inputType) {
                    case "checkbox":
                        data.set(this.attr, this.$el.is(":checked"));
                        break;
                    case "select":
                        data.set(this.attr, this.$el.val());
                        break;
                    }
                }
            }
        });


        $.fn.tabSlideIn = function (callback) {
            var self = this;
            $(this).closest(".dropdown").siblings($(self).attr("href")).addClass("t-c-s");
            $(".blocker").css({
                "height": $(document).height()
            });
            $(this).closest(".dropdown").siblings($(self).attr("href")).css("right", "500px").animate({
                opacity: 1
                , right: 0
            }, 300, function () {
                callback();
            });

        };

        $.fn.tabSlideOut = function (callback) {
            var self = this;
            $(this).closest(".dropdown").siblings(".t-c-s").animate({
                opacity: 0
                , right: -500
            }, 300, function () {
                $(self).closest(".dropdown").siblings(".t-c").removeClass("t-c-s");
                callback();
            });
        };

        var menuView = Backbone.View.extend({
            events: {
                'click': 'clicked'
            }
            , clicked: function () {
                var self = this;
                if (self.$el.closest(".dropdownContent").css('display') != "none") {
                    self.$el.closest(".dropdown").find(".dropdownLabel>.button").trigger("click");
                }
                if (!self.$el.hasClass("m-s")) {
                    self.$el.closest(".m-h").find("li").removeClass("m-s");
                    self.$el.addClass("m-s");
                    self.$el.tabSlideOut(function () {
                        var tab = self.$el.attr("href");
                        self.$el.tabSlideIn(function () {
                            $(".subtitle").text(self.$el.text());
                            switch (tab) {
                            case "#editDocument":
                                $(tab).find("#selectedOptions").find(".t-s").first().trigger("click");
                                break;
                            case "#settings":
                                $(tab).find(".t-s").first().trigger("click");
                                break;
                            }
                        });
                    });
                }
            }
        });

        var tabView = Backbone.View.extend({
            initialize: function () {}
            , events: {
                'click': 'clicked'
            }
            , clicked: function () {
                var self = this;
                this.$el.closest(".t-h").find("li").removeClass("t-s");
                this.$el.addClass("t-s");
                this.$el.closest(".t-h").siblings(".t-c").removeClass("t-c-s");
                this.$el.closest(".t-h").siblings(self.$el.attr("href")).addClass("t-c-s").focus();
                var tab = this.$el.attr("href");
                var id = this.$el.data("docid");
                var doc = this.$el.data("doc");
                /*if(doc){
                    $("#selectedOptions").find(".t-s").first().css("color", "red");
                    $("#selectedOptions").find(".t-s").first().trigger("click");
                }*/
                if (tab) {
                    //log(tab);
                    switch (tab) {

                    case "#browser":
                    case "#require":
                    case "#server":
                        //log($(tab).find(".t-a").length);
                        if ($(tab).find(".t-a").length === 0) {
                            //log('here');
                            currentID.set('currentTab', tab);
                            //$(tab).trigger("click");

                        } else {
                            $(tab).find(".t-s").first().trigger("click");
                        }
                        break;
                    case "#css":
                    case "#scripts":
                        //$(tab).find(".t-s").first().css("color", "red");
                        $(tab).find(".t-s").first().trigger("click");
                        break;
                    default:
                        //log(tab);
                        currentID.set('currentTab', tab);
                        currentID.set('currentDocId', id);
                        break;
                    }
                }
            }
        });

        var editorHolder = Backbone.View.extend({
            el: "#editorHolder"
            , initialize: function () {
                this.listenTo(currentID, "change:currentTab", function (data) {
                    if (data.get("currentID") !== null) {
                        this.render();
                    } else {
                        this.$el.addClass("hd");
                    }
                });
            }
            , render: function () {
                target = this.$el;
                var tab = currentID.get('currentTab');
                //log(tab);
                if (tab) {
                    switch (tab) {
                    case "#settings":
                    case "#selector":
                    case "#selected":
                    case "#info":
                    case "#modules":
                    case "#history":
                    case "#backup":
                    case "#console":
                    case "#browser":
                    case "#server":
                    case "#require":
                        this.$el.addClass("hd");
                        break;
                    default:
                        this.$el.removeClass("hd");
                        break;
                    }
                }
            }
        });

        var moduleHolder = Backbone.View.extend({
            el: "#moduleHolder"
            , initialize: function () {
                this.listenTo(currentID, "change", function (data) {
                    if (data.get("currentID") !== null) {
                        this.$el.removeClass("hd");
                    } else {
                        this.$el.addClass("hd");
                    }
                });
            }
        });


        var documentSelectedView = Backbone.View.extend({
            el: "#selected"
            , initialize: function () {
                this.listenTo(currentID, "change", function (data) {
                    if (data.get("currentID") !== null) {
                        data = coll.get(data.get("currentID"));
                        this.render(data);
                    } else {
                        log("no document loaded");
                        this.render(null);
                    }
                });
            }
            , render: function (data) {
                if (data) {
                    this.$el.find("#selectedCollection").html(data.get("type"));
                    this.$el.find("#selectedDocument").html(data.get("name"));
                    this.$el.find("#selectedId").html(data.get("_id"));
                } else {
                    this.$el.find("#selectedCollection").html("-- No selection --");
                    this.$el.find("#selectedDocument").html("-- No selection --");
                    this.$el.find("#selectedId").html("-- No selection --");
                }
            }
        });

        var cssTabs = Backbone.View.extend({
            el: "#css"
            , initialize: function () {
                this.listenTo(cssMap, "update", function (data) {
                    this.render(data);
                });
            }
            , render: function (data) {
                var tabline = new newTabViewHolder();
                this.$el.html(tabline.render(data));
            }
        });

        var newTabViewHolder = Backbone.View.extend({
            tagName: "ul"
            , className: "t-h"
            , render: function (data) {
                var target = this;
                target.$el.html("");
                var tab = new newTabView(data);
                data.each(function (i) {
                    target.$el.append(tab.render(i).html());
                });

                var tabs = new tabViews();
                tabs.setElement(target.$el.find(".t-a"));
                tabs.render();

                target.$el.append("<div class=\"t-b g-6-6 b b-t\"></div>");
                target.$el.find("li:first").addClass("t-s");
                return target.el;
            }
        });

        var newTabView = Backbone.View.extend({
            initialize: function () {}
            , template: _.template("<div class=\"b-b f\"><li class=\"t-autoWidth t-a v-1\" href=\"#cssEditor\" data-docid=\"<%= _id %>\"><%= name %></li></div>")
            , render: function (data) {
                this.$el.html(this.template(data.attributes));
                return this.$el;
            }
        });

        var tabViews = Backbone.View.extend({
            el: ".t-a"
            , initialize: function () {}
            , render: function () {
                this.$el.each(function () {
                    var tab = new tabView();
                    tab.setElement(this);
                });
            }
        });

        var menuViews = Backbone.View.extend({
            el: ".m-a"
            , initialize: function () {}
            , render: function () {
                this.$el.each(function () {
                    var menu = new menuView();
                    menu.setElement(this);
                });
            }
        });

        var tabModelChange = Backbone.View.extend({
            initialize: function () {
                var tabChangeApp = new tabChange();
                var documentSelectedViewApp = new documentSelectedView();
                var modulesViewApp = new modulesView();
                var documentInfoViewApp = new documentInfoView();
                var editorHolderApp = new editorHolder();
                var moduleHolderApp = new moduleHolder();
                var infoHolderApp = new moduleHolder();
                infoHolderApp.setElement("#infoHolder");
            }
        });

        var tabViewStart = Backbone.View.extend({
            initialize: function () {
                var tabViewsApp = new tabViews();
                tabViewsApp.render();
                var menuViewsApp = new menuViews();
                menuViewsApp.render();
            }
        });

        //view for each loaded model
        var dataViews = Backbone.View.extend({
            el: '#dataModels'
            , initialize: function () {
                this.listenTo(coll, "add", function (result) {
                    this.add(result);
                });
                this.listenTo(coll, "remove", function (result) {
                    this.remove(result);
                });
            }
            , remove: function (result) {
                var idToDelete = result.get("id");
                var sessionsToDelete = sessionColl.where({
                    "document": idToDelete
                });
                sessionColl.remove(sessionsToDelete);
                var target = this.$el.find("[data-doc='" + result.get("id") + "']").parent();
                target.slickOut(function () {
                    if (target.prev().find("li").hasClass('t-a')) {
                        if (target.find('li').hasClass('t-s')) {
                            target.prev().find("li").trigger('click');
                        }
                    } else if (target.next().find("li").hasClass('t-a')) {
                        if (target.find('li').hasClass('t-s')) {
                            target.next().find("li").trigger('click');
                        }
                    } else if (target.next().hasClass('t-b')) {
                        target.next().slickOut(function () {
                            $("[href='#selector']").trigger('click');
                        });
                        currentID.set("currentID", null);
                    }
                    target.remove();
                }, true);
            }
            , add: function (result) {
                var data = new dataView({
                    model: result
                });
                currentID.set("currentID", result.get("id"));
                if (!this.$('div').last().hasClass('t-b')) {
                    this.$el.append("<div class=\"t-b g-6-6 b b-t\"></div>");
                }
                this.$('.t-s').removeClass('t-s');
                this.$('div').last().before(data.render().el);
                return this;
            }
        });

        var dataView = Backbone.View.extend({
            tagName: 'div'
            , className: 'b flex'
            , attributes: {
                "style": "position:relative;border-color:transparent;"
            }
            , events: {
                'click li': 'clicked'
                , 'click button.close': 'close'
            }
            , clicked: function () {
                currentID.set("currentID", this.model.get("id"));
            }
            , close: function () {
                coll.remove(this.model);
            }
            , template: _.template("<li data-doc=\"<%= id %>\"class=\"t-a t-s t-autoWidth g-6-6 v-1 f1\"><%= name %></li><div class=\"icon\"><button class=\"i button close v-1 ux-no-h\"></button></div>")
            , render: function (result) {
                this.$el.html(this.template(this.model.attributes));
                var tab = new tabView();
                tab.setElement(this.$el.find("li"));
                return this;
            }
        });

        //empty model for retireiving documents to be listened to by views
        var dataModel = Backbone.Model.extend({
            sync: function (method, model, options) {
                switch (method) {
                case "update":
                    model = _.clone(model);
                    var tempUrl = model.url() + "/" + options.section;
                    var text = "";
                    if (options.section === "section") {
                        tempUrl += "/" + options.tab.substr(1);
                    }
                    if (options.query) {
                        tempUrl += "/" + options.query;
                    }
                    model.url = tempUrl;
                    model.attributes = model.toJSON();
                    switch (options.section) {
                    case "modules":
                        model.attributes = {
                            patch: model.attributes.modules
                        };
                        break;
                    case "info":
                        model.attributes = {
                            patch: {
                                name: model.attributes.name
                                , type: model.attributes.type
                                , template: model.attributes.template
                                , freq: model.attributes.freq
                                , priority: model.attributes.priority
                                , robots: model.attributes.robots
                                , active: model.attributes.active
                                , secure: model.attributes.secure
                                , access: model.attributes.access
                                , notes: model.attributes.notes
                            }
                        };
                        break;
                    case "section":
                        var output;
                        var patch;
                        switch (options.tab) {
                        case "#cssEditor":
                            try {
                                _.each(model.attributes.data.resources.css, function (i) {
                                    if (i.query === options.query) {
                                        text = i;
                                    }
                                });
                                model.attributes = {
                                    patch: text
                                };
                            } catch (err) {
                                log(err);
                            }
                            break;
                        case "#browserScripts":
                            try {
                                patch = _.find(model.attributes.data.resources.scripts.browser, {
                                    "_id": options.query
                                });
                                model.attributes = {
                                    patch: patch
                                };
                            } catch (err) {
                                log(err);
                            }
                            break;
                        case "#serverScripts":
                            try {
                                patch = _.find(model.attributes.data.resources.scripts.server, {
                                    "_id": options.query
                                });
                                model.attributes = {
                                    patch: patch
                                };
                            } catch (err) {
                                log(err);
                            }
                            break;
                        case "#requireScripts":
                            try {

                                //log(options.query);
                                //log(model.attributes.data.resources.scripts.browser);
                                patch = _.find(model.attributes.data.resources.scripts.require, {
                                    "_id": options.query
                                });
                                //log(patch);
                                model.attributes = {
                                    patch: patch
                                };
                            } catch (err) {
                                log(err);
                            }
                            break;
                        case "#js":
                            try {
                                model.attributes = {
                                    patch: model.attributes.data.resources.scripts.text
                                };
                            } catch (err) {
                                log(err);
                            }
                            break;
                        case "#reqs":
                            try {
                                model.attributes = {
                                    patch: model.attributes.data.resources.scripts.requires.text
                                };
                            } catch (err) {
                                log(err);
                            }
                            break;
                        case "#jsonload":
                            try {
                                model.attributes = {
                                    patch: model.attributes.data.resources.scripts.onload
                                };
                            } catch (err) {
                                log(err);
                            }
                            break;
                        case "#pug":
                            try {
                                model.attributes = {
                                    patch: model.attributes.data.html[0].data[0]
                                };
                            } catch (err) {
                                log(err);
                            }
                            break;
                        case "#body":
                            try {
                                output = model.attributes.data.html;
                                if (typeof output === "string") {
                                    output = JSON.parse(output);
                                }
                                model.attributes = {
                                    patch: output
                                };
                            } catch (err) {
                                log(err);
                            }
                            break;
                        case "#head":
                            try {
                                output = model.attributes.data.head;
                                if (typeof output === "string") {
                                    output = JSON.parse(output);
                                }
                                model.attributes = {
                                    patch: output
                                };
                            } catch (err) {
                                log(err);
                            }
                            break;
                        }
                        break;
                    }
                    break;
                case "create":
                    break;
                case "delete":
                    break;
                case "read":
                    break;
                }
                //log(model);
                return Backbone.sync(method, model, options);
            }
        });



        // watch all the sections for changes and update the relevant models, probably change to a view...
        /*var vaildStructure = function (output, attribute) {
            if (attribute === "body") {
                if (output) {
                    $('#editorBody').next().find(".save").text("Save").prop('disabled', false);
                } else {
                    $('#editorBody').next().find(".save").text("Invalid Stucture").prop('disabled', true);
                }
            } else if (attribute === "head") {
                if (output) {
                    $('#editorHead').next().find(".save").text("Save").prop('disabled', false);
                } else {
                    $('#editorHead').next().find(".save").text("Invalid Stucture").prop('disabled', true);
                }
            }
        };
        */

        var loadin = function (collection, document, target, documentname, callback) {
            var loadinURL = "/api/" + collection + "/" + document;
            var mod = new dataModel({
                id: document
            });
            mod.urlRoot = "/api/" + collection;
            var modtest = coll.get(document);
            if (modtest) {
                mod = modtest;
                alertBox("boolean", {
                    title: "Already loaded content"
                    , text: "This document is already loaded. \n\nSwitch to tab?"
                }, function (response) {
                    if (response) {
                        $("#dataModels").find('li').filter(function () {
                            return $(this).text() == mod.get("name");
                        }).trigger('click');
                        $(target).loaderProgOut(function () {
                            $("#collection").val("").trigger('change');

                        });
                    } else {
                        $(target).loaderProgOut(function () {
                            $("#collection").val("").trigger('change');
                        });
                    }
                    callback();
                });

            } else {
                mod.fetch({
                    success: function (data) {
                        coll.add(data);
                        if (_.isFunction(callback)) {
                            callback();
                        }
                        $("#collection").val("").trigger('change');
                    }
                    , error: function (response, error) {
                        log(error);
                        alertBox("message", {
                            title: "Error"
                            , text: error.status + " " + error.statusText
                        });
                        $(target).loaderProgOut(function () {
                            $("#collection").val("").trigger('change');
                        });
                    }
                });
            }
        };

        var create = Backbone.View.extend({
            el: "#create"
            , events: {
                'click': 'clicked'
            }
            , clicked: function (event) {
                event.preventDefault();
                var self = this;
                self.$el.loaderProgIn();
                validate(self.$el.closest('form'), null, function (validates) {
                    if (validates) {
                        $.ajax({
                            method: "POST"
                            , url: "/api/create/" + $('#newName').val() + "/" + $('#newCollection').val()
                            , success: function (result) {
                                var newModel = new mongoModel({
                                    "name": result.name
                                    , "_id": result._id
                                });
                                switch ($('#newCollection').val()) {
                                case "page":
                                    pageMap.add(newModel);
                                    break;
                                case "module":
                                    moduleMap.add(newModel);
                                    break;
                                case "template":
                                    templateMap.add(newModel);
                                }
                                self.$el.loaderProgOut();
                                if ($('#newCollection').val() === "module") {}
                                alertBox("message", {
                                    title: "Document Created"
                                    , text: "New document created</br></br>Document: \"" + result.name + "\"</br></br>Collection: \"" + result.type + "\""
                                }, function () {
                                    alertBox("boolean", {
                                        title: "Load New document"
                                        , text: "Load new document?</br></br>Document: \"" + result.name + "\"</br></br>Collection: \"" + result.type + "\""
                                    }, function (response) {
                                        if (response) {
                                            loadin(result.type, result._id, self.el, result.name, function () {
                                                $("li[href='#editDocument'").trigger('click');
                                                self.$el.loaderProgOut();
                                            });
                                        } else {
                                            self.$el.loaderProgOut();
                                        }
                                    });
                                });
                            }
                            , error: function (error) {
                                self.$el.loaderProgOut();
                                log(error);
                                alertBox("message", {
                                    title: "Error"
                                    , text: error.status + " " + error.statusText + "</br></br>" + error.responseText
                                });
                            }
                        });
                    } else {
                        self.$el.loaderProgOut();
                    }
                });
            }
        });

        var duplicate = Backbone.View.extend({
            el: "#duplicate"
            , events: {
                'click': 'clicked'
            }
            , clicked: function (event) {
                event.preventDefault();
                var self = this;
                self.$el.loaderProgIn();
                $.ajax({
                    method: "POST"
                    , url: "/api/duplicate/" + $('#selectedId').text() + "/" + $('#selectedCollection').text()
                    , success: function (result) {
                        var newModel = new mongoModel({
                            "name": result.name
                            , "_id": result._id
                        });
                        switch ($('#selectedCollection').text()) {
                        case "page":
                            pageMap.add(newModel);
                            break;
                        case "module":
                            moduleMap.add(newModel);
                            break;
                        case "template":
                            templateMap.add(newModel);
                        }
                        if ($('#selectedCollection').text() === "module") {}
                        alertBox("message", {
                            title: "Document Duplicated"
                            , text: "Document duplicated</br></br>Document: \"" + result.name + "\"</br></br>Collection: \"" + result.type + "\""
                        }, function () {
                            alertBox("boolean", {
                                title: "Load New document"
                                , text: "Load new document?</br></br>Document: \"" + result.name + "\"</br></br>Collection: \"" + result.type + "\""
                            }, function (response) {
                                if (response) {
                                    loadin(result.type, result._id, self.el, result.name, function () {
                                        self.$el.loaderProgOut();
                                    });
                                } else {
                                    self.$el.loaderProgOut();
                                }
                            });
                        });
                    }
                    , error: function (error) {
                        self.$el.loaderProgOut();
                        log(error);
                        alertBox("message", {
                            title: "Error"
                            , text: error.status + " " + error.statusText + "</br></br>" + error.responseText
                        });
                    }
                });
            }
        });

        var open = Backbone.View.extend({
            el: "#open"
            , events: {
                "click": "clicked"
            }
            , clicked: function (event) {
                event.preventDefault();
                var self = this;
                self.$el.loaderProgIn();
                validate(self.$el.closest('form'), null, function (validates) {
                    if (validates) {
                        loadin($('#collection').val(), $('#document').val(), self.el, $('#document option:selected').text(), function () {
                            self.$el.loaderProgOut();
                            if (self.$el.is("#openAndGoto")) {
                                $("li[href='#editDocument']").trigger('click');
                            }
                        });
                    } else {
                        self.$el.loaderProgOut();
                    }
                });
            }
        });

        var appViews = Backbone.View.extend({
            initialize: function () {
                var cssTabsApp = new cssTabs();
                var selectorApp = new selectors();
                var moduleApp = new moduleView();
                var app = new dataViews();
                var selectorsTempApp = new selectorsTemp();
                var tabViewsStartApp = new tabViewStart();
                var tabModelChangeApp = new tabModelChange();
                var duplicateApp = new duplicate();
                var createApp = new create();
                var openApp = new open();
                var openAndGotoApp = new open();
                openAndGotoApp.setElement("#openAndGoto");

            }
        });
        var appRun = new appViews();

        var modulePatch = Backbone.View.extend({
            el: "#moduleSave"
            , initialize: function () {
                var patchView = new patch({
                    "section": "modules"
                });
                patchView.setElement(this.el);
            }
        });

        var infoPatch = Backbone.View.extend({
            el: "#siteinfoSave"
            , initialize: function () {
                var patchView = new patch({
                    "section": "info"
                });
                patchView.setElement(this.el);
            }
        });

        var sectionPatch = Backbone.View.extend({
            el: "#sectionSave"
            , initialize: function () {
                var patchView = new patch({
                    "section": "section"
                });
                patchView.setElement(this.el);
            }
        });

        var patch = Backbone.View.extend({
            initialize: function (options) {
                this.section = options.section;
            }
            , events: {
                "click": "clicked"
            }
            , clicked: function (event) {
                event.preventDefault();
                if (!currentID.get("currentID")) {
                    alertBox("message", {
                        title: "Warning"
                        , text: "No document selected"
                    });
                } else {
                    var doc = currentID.get("currentID");
                    var tab = currentID.get("currentTab");
                    var query = currentID.get("currentDocId");
                    model = coll.get(doc);
                    var target = this.$el;
                    target.loaderProgIn();
                    if (this.section === "section") {
                        update(currentID.get('currentTab'), currentID.get('currentID'), currentID.get('currentDocId'));
                    }
                    model.sync("update", model, {
                        success: function (model, response, options) {
                            //log(model);
                            if (!model.save) {
                                log('need to update the model with the below');
                                //log(model);
                            }
                            target.loaderProgOut();
                        }
                        , error: function (error) {
                            alertBox("message", {
                                title: "Error"
                                , text: error.status + " " + error.statusText
                            });
                            target.loaderProgOut();
                        }
                        , section: this.section
                        , tab: tab
                        , query: query
                    });
                }
            }
        });



        var deleteButton = Backbone.View.extend({
            el: "#delete"
            , events: {
                "click": "clicked"
            }
            , clicked: function (event) {
                event.preventDefault();
                if (!currentID.get("currentID")) {
                    alertBox("message", {
                        title: "Warning"
                        , text: "No document selected"
                    });
                } else {
                    var self = this;
                    alertBox("continue", {
                        title: "Confirm delete"
                        , text: "You are about to delete:</br></br>Name: \"" + $("#selectedDocument").text() + "\"</br></br>Document ID: \"" + $("#selectedId").text() + "\"</br></br>Collection: \"" + $("#selectedCollection").text() + "\"</br>"
                    }, function (response) {
                        if (response) {
                            self.$el.loaderProgIn();
                            var doc = currentID.get("currentID");
                            model = coll.get(doc);
                            model.sync("delete", model, {
                                success: function (result) {
                                    var modelToRemove;
                                    switch (result.collection) {
                                    case "page":
                                        pageMap.remove(result.id);
                                        break;
                                    case "module":
                                        moduleMap.remove(result.id);
                                        break;
                                    case "template":
                                        templateMap.remove(result.id);
                                        break;
                                    }
                                    alertBox("message", {
                                        title: "Deleted"
                                        , text: "Sucessfully deleted:</br></br>Name: \"" + $("#selectedDocument").text() + "\"</br></br>Document ID: \"" + $("#selectedId").text() + "\"</br></br>Collection: \"" + $("#selectedCollection").text() + "\"</br>"
                                    }, function () {
                                        coll.remove(model);
                                    });
                                    self.$el.loaderProgOut();
                                }
                                , error: function (error) {
                                    self.$el.loaderProgOut();
                                    alertBox("message", {
                                        title: "Error"
                                        , text: error.status + " " + error.statusText
                                    });
                                }
                            });
                        }
                    });
                }
            }
        });

        var modulePatchApp = new modulePatch();
        var infoPatchApp = new infoPatch();
        var sectionPatchApp = new sectionPatch();
        var deleteButtonnApp = new deleteButton();


        $("#backUpButton").click(function (event) {
            event.preventDefault();
            $(this).loaderProgIn();
            var target = this;
            $.post("/api/backup/", function (data) {
                log(data);
                $(target).loaderProgOut();
            });
        });

        $(".editorclass").css("height", $(window.top).height() - 140);
        $(".editorclass").on("click keypress", function () {
            $(".editorclass").css("height", $(window.top).height() - 140);
            $(this).scrollView();
        });
    });
    $("#ide").loaderProgOut();
    $('#collection').prop("disabled", false);
    console.log("Backbone and Ace powered editors loaded and ready");
// testing
});