/**
 * Module dependencies.
 */

var express = require('express');
var app = express();
var config = require('./config');
//var requires = require('./requires');
// connect/express requires
//var reload = require('reload');
var http = require('http');
//var reload = require('reload');
//var https = require('https');
var path = require('path');
//var fs = require('fs');
var compress = require('compression');
//var mongodb = require('./models/dbconnect');
//var mongoconnection = mongodb.mongoose.connections[0];
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
//var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
app.use(bodyParser.json({limit: "1mb"}));
app.use(bodyParser.urlencoded({limit:"1mb", extended : true}));
var favicon = require('serve-favicon');
//var errorhandler = require('errorhandler');
var morgan = require('morgan');
var methodOverride = require('method-override');
var fn = require('./lib/fn');
//var environment = app.get('env');

console.log("Node environment: "+app.get('env'));

/*var authenticate = require('./routes/login');
//console.log(authenticate);
var main = require('./routes/crud');*/

// create new DB connection for sessions use
var dbuser = "";
if (config.mongo.users.admin.user === "") {dbuser = "";} else {   dbuser = config.mongo.users.admin.user + ':' + config.mongo.users.admin.pass;}
var dburl = 'mongodb://' + dbuser + config.mongo.url + "/" + config.mongo.db;

var passport = require('passport');
require('./lib/auth')(passport); 
//require('./lib/js/bundle');
//sessions

app.use(session({
    resave: false,
    saveUninitialized: false,
    name: "sessionID",
    store: new MongoStore({
        //mongooseConnection: mongoconnection
        ttl: 14 * 24 * 60 * 60, // session expiry
        url : dburl
    }),
    secret: config.sessionSecret
}));

// passport



// security certificates for ssl
/*
var privateKey = fs.readFileSync('./ssl/server-key.pem', 'utf8');
var certificate = fs.readFileSync('./ssl/server-cert.pem', 'utf8');
var credentials = {
    key: privateKey,
    cert: certificate
};*/

// all environments
app.set('port', process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || config.port);
app.set('ip',  process.env.OPENSHIFT_NODEJS_IP || process.env.IP || "127.0.0.1");
//self.port      = process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT;

//app.set('secureport', process.env.SSLPORT || config.secureport);
app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'jade');
app.set('etag', false); // change when ready to deal with Etags
app.use(compress());

// cookies 
//app.use(cookieParser());
//set up url for sessions on mongodb


// Initialize Passport!  Also use passport.session() middleware, to support persistent login sessions (recommended).
app.use(passport.initialize());
app.use(passport.session());

// Configure passport


// other uses
app.use(favicon(__dirname + '/public/images/favicon.ico'));
if (config.chatty === true) {
    app.use(morgan('dev'));
}
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(methodOverride());
//app.use(express.static(path.join(__dirname, '')));
app.use(express.static(path.join(__dirname, 'public')));

// development only
/*
if ('development' == app.get('env')) {
    app.use(errorhandler());
    app.locals.pretty = true; // also unpretty on the page route jade.render command index.js ;)
    var memwatch = require('memwatch');
    memwatch.on('leak', function(info) {
        console.log(info);
    });
}*/

// routes
//routes(app);

//CRUD API

require('./routes/routes')(app, passport);

/*app.post("/api/*", main.api.create);
app.get("/api/*", main.api.read);
app.put("/api/*", main.api.update);
app.delete("/api/*", main.api.delete);
app.get('/test', main.test);
app.post('/beautify/:method', main.beautify);
app.get('/images/icons/*', main.icon);
//app.get('/auth/:method', main.login);
app.get('/auth/:method', authenticate);
app.post('/auth/:method', authenticate);
//app.post('/auth/:method', main.login);
app.get('/captcha', main.captcha);
app.get('/captcha/*', main.captcha);
app.get('/sitemap.xml', main.sitemap);
app.get('/robots.txt', main.robots);
app.get('/*', main.file);*/

var server = http.createServer(app);
//server.timeout = 120000;
server.setTimeout(120000, function(){
    console.log("server timed out");
});

server.on("error", err=>console.log(err));
//reload(server, app);

// start servers
fn.cache.startup(
    function() {
        fn.log("cache proceeses complete, starting servers");
        server.listen(app.get('port'), app.get('ip'), function() {
            console.log('Express server listening on port ' + app.get('port'));
        });

        // only kicks in SSL if a secure port is allocated
/*
        if (app.get('secureport')) {
            https.createServer(credentials, app).listen(app.get('secureport'), function() {
                console.log('Express server listening on secure port ' + app.get('secureport'));
            });
        }*/
    });